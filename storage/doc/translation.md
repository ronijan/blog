### Translation

[Symfony Translation Doc](https://symfony.com/doc/current/translation.html)

Extracting Translation Contents and Updating Catalogs Automatically

* $ `php bin/console translation:extract --force en --format=yaml --sort=desc`
* $ `php bin/console translation:extract --force de --format=yaml --sort=desc`