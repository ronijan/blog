### PHP-Unit Tests

* run all tests of the applications `$ php bin/phpunit`

* run all tests in a folder `$ php bin/phpunit tests/Unit`

* run tests only for a class `$ php bin/phpunit tests/Unit/Service/ConfigServiceTest.php`