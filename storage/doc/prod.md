### Production

---

- move images in `public/assets/img/ref`
- move images in `public/assets/img/testimonials`
- move images in `public/assets/img/team`
- move images in `public/assets/uploads/article`
- move images in `public/assets/uploads/avatar`

---

read [How to Deploy a Symfony Application](https://symfony.com/doc/current/deployment.html)

* $ `php bin/console config:dump-reference security` *default*
* $ `php bin/console debug:config security` *actual*
* $ `composer require symfony/requirements-checker`

* $ `php bin/phpunit`
* $ `php bin/console faker:load`
* $ `composer dump-env prod`
* $ `composer install --no-dev --optimize-autoloader`
* $ `APP_ENV=prod APP_DEBUG=0 php bin/console cache:clear`

---

### Sanitize output

add the following code at `vendor/autoload_runtime.php` Line: 28

````php
ob_start("sanitizeOutput");

function sanitizeOutput($buffer): array|string|null
{
$search = ['/\>[^\S ]+/s', '/[^\S ]+\</s', '/(\s)+/s', '/<!--(.|\s)*?-->/'];
$replace = ['>', '<', '\\1', ''];
return preg_replace($search, $replace, $buffer);
}
````
