### TODOs

* generate migrations !
* in repo::add(...) plz change all to save method

---

### DONE

* Added Infos Modal for Article, Tag, Category .. *__[11032023]__*
* Fixed Pagination for Articles
* faker package is not working on prod !!! (fixed)

* Auther can see his own published Articles but can't edit them.. *__[09032023]__*
* Fix permission on social links.
* Pagination for articles in home.
* Fixed contact-form route.
* Added dummy About-us page

* remove models and use repos. directly.. *__[08032023]__* 
    + ProfileModel
    + ResetPasswordModel 
    + TwoFactorAuthDevicesModel 
    + TwoFactorAuthModel 
    + ArticleModel 
    + AutherModel 
    + CategoryModel 
    + CommentModel 
    + ReplyModel 
    + TagModel 
    + ContactFormModel 
    + FAQModel 
    + LogActivitiesModel 
    + NewsletterModel 
    + NotificationModel 
    + SystemLogsModel 
    + UserModel 
    + UserSettingModel 