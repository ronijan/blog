### Bin/Console

- $ php bin/console doctrine:migrations:migrate
- $ php bin/console doctrine:schema:drop --force

---

### DataFixtures

to genereate Dummy data

- $ php bin/console app:seeds 5 (see CHANGE_ME in Helper/FakerHelper::class)
- $ php bin/console admin:add
- $ php bin/console faker:load

```SQL
$ mysql -u blogger -p (SEE .env)
$ update user set roles = '["ROLE_SUPER_ADMIN"]' where email = 'your@email.dev';
```

---

### PHP installation

- $ sudo apt update
- $ sudo apt install lsb-release ca-certificates apt-transport-https software-properties-common -y
- $ sudo add-apt-repository ppa:ondrej/php
- $ sudo apt install php8.1
- $ php -v
- $ sudo apt install php8.0-<extension>
- $ sudo apt install php8.1-cli php8.1-common php8.1-imap php8.1-redis php8.1-xml php8.1-zip php8.1-mbstring php8.1-ext-xml
- $ php -m
- $ systemctl status php\*-fpm.service

---

### Mysql

- sudo apt-get install mysql-server
- sudo mysql_secure_installation
- sudo systemctl status mysql
- sudo systemctl enable mysql
- sudo mysql -u root -p
- mysql> create user admin@localhost identified by 'NEW*password*!1';
- mysql> select user from mysql.user;

- mysql> GRANT ALL PRIVILEGES ON DB_Name . \* TO 'admin'@'localhost';

- mysql> UPDATE user SET roles = '["ROLE_SUPER_ADMIN"]' WHERE email = 'admin@hu.com';
- mysql> UPDATE user SET updated_at = '2000-01-01 00:00:00' WHERE email = 'xxx@xxxx.com';
- mysql> UPDATE contact_form SET updated_at = '2000-01-01 00:00:00' WHERE name IN('name1', 'name2');

```SQL
    --
    SET FOREIGN_KEY_CHECKS = 0;
    SET @tables = NULL;
    SELECT GROUP_CONCAT('`', table_schema, '`.`', table_name, '`') INTO @tables
        FROM information_schema.tables
        WHERE table_schema = '!!!_SEPECIFY_DB_NAME_HERE_!!!';

    SET @tables = CONCAT('DROP TABLE ', @tables);
    PREPARE stmt FROM @tables;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
    SET FOREIGN_KEY_CHECKS = 1;
    --
```

---

### XDebug

- $ `sudo apt install php-xdebug`
- $ `/etc/php/8.2/mods-available`
- $ `sudo vim xdebug.ini`

```BASH
# xdebug.ini
zend_extension=xdebug.so
xdebug.mode=develop,debug
xdebug.start_with_request=yes
```

---

### Git

- git rm -r cached .
- git add .

---

### Ubuntu

- service cron start
