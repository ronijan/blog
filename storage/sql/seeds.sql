--
--
  SET FOREIGN_KEY_CHECKS = 0; 
  SET @tables = NULL;
  SELECT GROUP_CONCAT('`', table_schema, '`.`', table_name, '`') INTO @tables
    FROM information_schema.tables 
    WHERE table_schema = '!!!_SEPECIFY_DB_NAME_HERE_!!!';

  SET @tables = CONCAT('DROP TABLE ', @tables);
  PREPARE stmt FROM @tables;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;
  SET FOREIGN_KEY_CHECKS = 1;
--
--

DROP TABLE IF EXISTS article;

CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auther_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_time` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `is_head` tinyint(1) NOT NULL,
  `is_comments_allowed` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `tags` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `head_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_23A0E66BCC5EBBA` (`auther_id`),
  KEY `IDX_23A0E6612469DE2` (`category_id`),
  CONSTRAINT `FK_23A0E6612469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  CONSTRAINT `FK_23A0E66BCC5EBBA` FOREIGN KEY (`auther_id`) REFERENCES `auther` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO article VALUES("1","2","2","dolore magna aliquam erat volutpat","<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.&nbsp;</p><h4><strong style=\"color: rgb(0, 102, 204);\">Ut wisi enim ad minim veniam</strong></h4><p>quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.&nbsp;</p><p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum</p><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper</p><h4><strong style=\"color: rgb(0, 102, 204);\">Suscipit lobortis nisl ut aliquip ex ea commodo consequat.&nbsp;</strong></h4><p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis</p><ul><li>At vero eos et accusam et justo duo dolores et ea rebum.</li><li>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</li><li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</li><li>invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</li></ul><p>At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat</p><p>et nonumy sed tempor <u style=\"background-color: rgb(255, 255, 204);\">et et invidunt justo labore Stet clita ea et gubergren, kasd magna no rebum</u>. sanctus sea sed takimata ut vero voluptua. est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.&nbsp;</p>","5 Min.","1","0","1","0","Adipisci, Delectus","08_20230216141447.png","2023-02-16 14:25:28","2023-02-16 14:14:47"),
("2","1","1","autem vel eum iriure dolor in hendrerit in","<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.&nbsp;</p><h4><span style=\"color: rgb(0, 102, 204);\">Ut wisi enim ad minim veniam</span></h4><p>quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.&nbsp;</p><p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum</p><h4>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper <span style=\"color: rgb(0, 102, 204);\">suscipit lobortis nisl ut aliquip ex ea commodo consequat.&nbsp;</span></h4><p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis</p><p>At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no <span style=\"background-color: rgb(255, 235, 204);\">sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat</span></p><p><span style=\"background-color: rgb(255, 235, 204);\">et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no </span>rebum. sanctus sea sed takimata ut vero voluptua. est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.</p>","3 Min.","1","1","1","0","Optio, Id","06_20230216143900.png","2023-02-16 14:39:11","2023-02-16 14:39:00");



DROP TABLE IF EXISTS auther;

CREATE TABLE `auther` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_profile_public` tinyint(1) NOT NULL,
  `articles_count` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_47403042A76ED395` (`user_id`),
  CONSTRAINT `FK_47403042A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO auther VALUES("1","5","Ronijan","Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.","1","1","2023-02-16 14:52:34","2023-02-16 14:04:22"),
("2","1","Susie","Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.","1","1","2023-02-16 14:18:11","2023-02-16 14:09:44");



DROP TABLE IF EXISTS category;

CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO category VALUES("1","voluptatem","2023-02-16 13:47:09","2023-02-16 13:47:09"),
("2","reprehenderit","2023-02-16 13:47:10","2023-02-16 13:47:10"),
("3","qui","2023-02-16 13:47:12","2023-02-16 13:47:12"),
("4","eligendi","2023-02-16 13:47:13","2023-02-16 13:47:13");



DROP TABLE IF EXISTS comment;

CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `parent_comment_id` int(11) NOT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9474526C7294869C` (`article_id`),
  KEY `IDX_9474526CA76ED395` (`user_id`),
  CONSTRAINT `FK_9474526C7294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`),
  CONSTRAINT `FK_9474526CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO comment VALUES("1","1","5","0","Consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.","2023-02-16 14:18:44","2023-02-16 14:18:44"),
("2","1","5","0","duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat","2023-02-16 14:23:38","2023-02-16 14:23:38"),
("3","2","1","0","Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.","2023-02-16 14:51:25","2023-02-16 14:51:25");



DROP TABLE IF EXISTS contact_form;

CREATE TABLE `contact_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_copy_sent` tinyint(1) NOT NULL,
  `is_archived` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO contact_form VALUES("1","Mckenna Leuschke","coy.wolf@cassin.net","Totam eos quia delectus corrupti quas. Est magni dignissimos corporis magnam quis ab id dolorem.","Nihil eos consectetur rerum molestiae corrupti. Distinctio eum repellat culpa ullam placeat. Non officiis aut incidunt qui. Est sed rem vel velit.","","0","0","0","2023-02-16 14:01:48","2023-02-16 13:47:09"),
("2","Francis Hauck I","kbecker@gmail.com","Eum similique voluptatem beatae repellat. Accusamus enim eos corrupti magni.","Ut fugiat dicta quisquam. Aspernatur molestiae laboriosam explicabo ipsam rerum. Rem qui sit itaque omnis. Dolore earum dolor placeat ut rerum.","","0","0","0","2023-02-16 14:01:48","2023-02-16 13:47:10"),
("3","Carmen O\'Hara","phyllis.ohara@hyatt.com","Aut veniam et adipisci corporis. Omnis saepe consectetur quia omnis.","Iusto voluptas voluptates aut nostrum et et reiciendis. Fuga corporis molestiae rerum ex consequatur. Incidunt accusantium ipsa natus eaque ipsam. Repellat et ea numquam non rerum et porro qui. Nesciunt saepe quis corrupti amet voluptatum sed saepe.","","0","0","0","2023-02-16 14:01:48","2023-02-16 13:47:11"),
("4","Audreanne Kihn","justyn33@hauck.org","Mollitia est inventore qui quia. Nesciunt incidunt esse asperiores. Enim quas ut vel itaque.","Molestiae impedit eos adipisci nulla. Quidem beatae iste aut. Laboriosam suscipit quo perferendis ratione eum vero similique voluptatem. Odio expedita quidem nobis aspernatur quia natus. Omnis corrupti suscipit officia. Dicta aliquid id maxime non voluptas iste dolores voluptatem. Minima beatae et accusantium recusandae sunt nemo et.","","0","1","0","2023-02-16 14:02:30","2023-02-16 13:47:13");



DROP TABLE IF EXISTS doctrine_migration_versions;

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO doctrine_migration_versions VALUES("DoctrineMigrations\\Version20230216132102","2023-02-16 13:21:12","20402");



DROP TABLE IF EXISTS frequently_asked_questions;

CREATE TABLE `frequently_asked_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS log_activities;

CREATE TABLE `log_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `action` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BFE7035EA76ED395` (`user_id`),
  CONSTRAINT `FK_BFE7035EA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



DROP TABLE IF EXISTS newsletter;

CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_subscribed` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO newsletter VALUES("1","crooks wanda","crooks.wanda@hotmail.com","f44484b1ce5728283b0aad32032716af7b3ec0dd3e938d0496458de62585688c","0","2023-02-16 13:47:09","2023-02-16 13:47:09"),
("2","heidi87","heidi87@armstrong.biz","710e81fba33f6adc5774079b291612ccd2cd8fd75bcc681560d1cfd724140e0e","0","2023-02-16 13:47:10","2023-02-16 13:47:10"),
("3","ashtyn75","ashtyn75@gmail.com","aec5de567af054b9e7fcf033db61548af06ba519492e5aa3e52caa43ef5f152e","0","2023-02-16 13:47:11","2023-02-16 13:47:11"),
("4","reynolds alfonso","reynolds.alfonso@hotmail.com","9a4352e2fbadd2bfdc36fb693a33aa841b77707d7223e3409fcd0ddd708433c9","1","2023-02-16 14:03:18","2023-02-16 13:47:13");



DROP TABLE IF EXISTS notification;

CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `subject` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_seen` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BF5476CAA76ED395` (`user_id`),
  CONSTRAINT `FK_BF5476CAA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO notification VALUES("1","1","Aut et culpa et temporibus. Harum provident in voluptate.","Aliquid rerum quibusdam voluptas vel consequatur dolor rem. Aut dolor doloribus et facere a.","1","2023-02-16 14:03:22","2023-02-16 13:47:08"),
("2","2","Aut sit autem rerum. Est eveniet cupiditate tenetur voluptatem. Rem illo in fuga.","Sunt dicta id possimus voluptate. Aut inventore molestiae aliquid reiciendis. Ut inventore et nam. Aliquam voluptatem sapiente eos.","1","2023-02-16 14:03:22","2023-02-16 13:47:10"),
("3","3","Et quas consequatur rerum. Quo dolorum a ea numquam cupiditate non eius.","Veniam consequatur sed a porro debitis. Eos alias doloremque necessitatibus molestias nesciunt consequatur. Non corrupti quas ullam eligendi eos. Quas sint quia ut est cum quo.","1","2023-02-16 14:03:22","2023-02-16 13:47:11"),
("4","4","Ut ratione qui tempore. Ullam ut modi minus dolores fuga quos nihil est.","Sapiente animi quisquam aperiam qui quod eligendi. Commodi doloribus sint recusandae velit. Perferendis dolores quos saepe repellat. Possimus ullam ut sint aperiam nam harum.","1","2023-02-16 14:03:22","2023-02-16 13:47:13"),
("5","5","New User","A new User has just registered via form","1","2023-02-16 14:03:22","2023-02-16 13:50:59"),
("7","5","Full-backup","Full backup script is executed successfully.","0","2023-02-16 15:01:43","2023-02-16 15:01:43");



DROP TABLE IF EXISTS profile;

CREATE TABLE `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_uploaded` tinyint(1) NOT NULL,
  `size` int(11) NOT NULL,
  `extension` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8157AA0FA76ED395` (`user_id`),
  CONSTRAINT `FK_8157AA0FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO profile VALUES("1","1","maria_20230216145042.png","1","110564","png","2023-02-16 14:50:42","2023-02-16 13:47:08"),
("2","2","avatar.png","0","0","","2023-02-16 13:47:10","2023-02-16 13:47:10"),
("3","3","avatar.png","0","0","","2023-02-16 13:47:11","2023-02-16 13:47:11"),
("4","4","avatar.png","0","0","","2023-02-16 13:47:12","2023-02-16 13:47:12"),
("5","5","me_20230216143553.png","1","23765","png","2023-02-16 14:35:53","2023-02-16 13:50:59");



DROP TABLE IF EXISTS reset_password;

CREATE TABLE `reset_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requested_at` datetime NOT NULL,
  `expires_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B9983CE5A76ED395` (`user_id`),
  CONSTRAINT `FK_B9983CE5A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS system_logs;

CREATE TABLE `system_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_archived` tinyint(1) NOT NULL,
  `ip` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS tag;

CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO tag VALUES("1","delectus","2023-02-16 13:47:09","2023-02-16 13:47:09"),
("2","id","2023-02-16 13:47:10","2023-02-16 13:47:10"),
("3","optio","2023-02-16 13:47:12","2023-02-16 13:47:12"),
("4","adipisci","2023-02-16 13:47:13","2023-02-16 13:47:13");



DROP TABLE IF EXISTS two_factor_auth;

CREATE TABLE `two_factor_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `alternative_email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_enabled` tinyint(1) NOT NULL,
  `otp` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2040D91CA76ED395` (`user_id`),
  CONSTRAINT `FK_2040D91CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO two_factor_auth VALUES("1","1","","0","","2023-02-16 13:47:08","2023-02-16 13:47:08"),
("2","2","","0","","2023-02-16 13:47:09","2023-02-16 13:47:09"),
("3","3","","0","","2023-02-16 13:47:11","2023-02-16 13:47:11"),
("4","4","","0","","2023-02-16 13:47:12","2023-02-16 13:47:12"),
("5","5","","0","","2023-02-16 13:50:59","2023-02-16 13:50:59");



DROP TABLE IF EXISTS two_factor_auth_devices;

CREATE TABLE `two_factor_auth_devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `browser` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operating_system` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BE99E87DA76ED395` (`user_id`),
  CONSTRAINT `FK_BE99E87DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS user;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_writer` tinyint(1) NOT NULL,
  `is_verified` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `is_disabled` tinyint(1) NOT NULL,
  `login_counts` int(11) NOT NULL,
  `locale` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `temp_email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_logged_in` datetime DEFAULT NULL,
  `last_password_updated` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO user VALUES("1","Susie_Edit","ro@hu.com","[\"ROLE_EDITOR\"]","$2y$13$5Rk/9grWUiOEsf7TD.uK4OAj00EFniXeL3bbp7jx1PVYWnqZLWuKa","1","1","0","0","2","en","","","2023-02-16 14:39:53","2023-02-16 14:11:08","2023-02-16 14:39:53","2023-02-16 13:47:07"),
("2","Dr. Kraig Dickinson V","rlittle@witting.org","[\"ROLE_USER\"]","$2y$13$edaenW/DCWaP8kE8Fw3tmOT0C7m2l6rI9nSPOhRadyUIMhj.vsXOW","0","0","0","0","0","en","","","","2023-02-16 13:47:09","2023-02-16 13:47:09","2023-02-16 13:47:09"),
("3","Electa Jenkins","rturcotte@berge.net","[\"ROLE_USER\"]","$2y$13$QEZ0cfKEVAFKZjfq7nghf.kmRsxeZOTxJFtlLdHjzb3DQXLN09IZS","0","0","0","0","0","en","","","","2023-02-16 13:47:11","2023-02-16 13:47:11","2023-02-16 13:47:10"),
("4","Eldred Kessler","grimes.tatum@dickinson.com","[\"ROLE_USER\"]","$2y$13$zvi7N8s9.Ara/CM3XwlKWOibodmYRzk4VtKfEl2lUmGCGGswsrMZW","0","0","0","0","0","en","","","","2023-02-16 13:47:12","2023-02-16 13:47:12","2023-02-16 13:47:12"),
("5","Ronijan","hu@hu.com","[\"ROLE_SUPER_ADMIN\"]","$2y$13$9QwosWWB2apcrEYEVJwLYOG5.XRTM9EliW/Vu3qcOoBANeCAgwl7e","1","1","0","0","10","en","","","2023-02-16 14:52:13","2023-02-16 13:50:59","2023-02-16 14:52:13","2023-02-16 13:50:58");



DROP TABLE IF EXISTS user_setting;

CREATE TABLE `user_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `pagination_limit` int(11) NOT NULL,
  `auto_delete_notifications` tinyint(1) NOT NULL,
  `notification_sending` tinyint(1) NOT NULL,
  `logs_limit` int(11) NOT NULL,
  `log_sending` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C779A692A76ED395` (`user_id`),
  CONSTRAINT `FK_C779A692A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO user_setting VALUES("1","1","8","1","0","6","0","2023-02-16 13:47:08","2023-02-16 13:47:08"),
("2","2","8","1","0","6","0","2023-02-16 13:47:10","2023-02-16 13:47:10"),
("3","3","8","1","0","6","0","2023-02-16 13:47:11","2023-02-16 13:47:11"),
("4","4","8","1","0","6","0","2023-02-16 13:47:13","2023-02-16 13:47:13"),
("5","5","8","1","1","6","1","2023-02-16 14:01:27","2023-02-16 13:50:59");



