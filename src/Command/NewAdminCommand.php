<?php

declare(strict_types=1);

namespace App\Command;

use App\Helper\UserHelper;
use App\Service\Account\RegisterNewUserService;
use App\Service\ConfigService;
use App\Service\FakerService;
use App\Service\UserService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

// $ php bin/console admin:add

#[AsCommand(
    name: 'admin:add',
    description: 'Add new user with role admin',
    hidden: false,
    aliases: ['admin:new']
)]
class NewAdminCommand extends Command
{
    public function __construct(
        private readonly UserService $userService,
        private readonly RegisterNewUserService $registerNewUserService,
        private readonly FakerService $fakerService,
        private readonly ConfigService $config
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('running ...');

        if ($this->config->getParameter('ALLOW_COMMAND') !== 'on') {
            $output->writeln("Please check the config [ALLOW_COMMAND]. It must be on.");
            return Command::FAILURE;
        }

        $username = $this->fakerService->getOne()->getName();
        $email = $this->fakerService->getOne()->getEmail();
        $pw = 'Demo_123';

        $user = $this->registerNewUserService->setUp($username, $email, $pw);

        $user = $this->userService->updateTokenAndIsVerified($user);

        $this->userService->updateRole($user, UserHelper::ROLE_ADMIN);

        $output->writeln('done!');
        $output->writeln(
            sprintf(
                'Demo Account - u:: %s, e:: %s, Pw:: %s',
                $username,
                $email,
                $pw
            )
        );

        return Command::SUCCESS;
    }

    protected function configure(): void
    {
        $this->setHelp("This command allows you to create an admin ..");
    }
}