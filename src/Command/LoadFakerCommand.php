<?php

declare(strict_types=1);

namespace App\Command;

use App\Helper\DataTransformer;
use App\Service\ConfigService;
use App\Service\FakerService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

// $ php bin/console faker:load

#[AsCommand(
    name: 'faker:load',
    description: 'Seed Database with fake Data.',
    hidden: false,
    aliases: ['faker:l']
)]
class LoadFakerCommand extends Command
{
    public function __construct(
        private readonly FakerService $fakerService,
        private readonly ConfigService $config
    ) { 
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('running ...');

        if ($this->config->getParameter('ALLOW_COMMAND') !== 'on') {
            $output->writeln("Please check the config [ALLOW_COMMAND]. It must be on.");
            return Command::FAILURE;
        }

        $i = 1;

        while($i <= DataTransformer::FAKER_TABLE_ENTRIES_COUNT) {
            $this->fakerService->create();
            $i++;
        }

        $output->writeln('done! check your faker table.');

        return Command::SUCCESS;
    }

    protected function configure(): void
    {
        $this->setHelp("This command allows you to seed faker table with fake Data ..");
    }
}