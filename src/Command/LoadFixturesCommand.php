<?php

declare(strict_types=1);

namespace App\Command;

use App\DataFixtures\AppFixtures;
use App\Service\ConfigService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

// $ php bin/console app:seeds 5 (see CHANGE_ME in Helper/FakerHelper::class)

#[AsCommand(
    name: 'app:seeds',
    description: 'Seed Database with fake Data.',
    hidden: false,
    aliases: ['app:s']
)]
class LoadFixturesCommand extends Command
{
    public function __construct(private readonly AppFixtures $appFixtures, private readonly ConfigService $config)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('running ...');

        if ($this->config->getParameter('ALLOW_COMMAND') !== 'on') {
            $output->writeln("Please check the config [ALLOW_COMMAND]. It must be on.");
            return Command::FAILURE;
        }

        $this->appFixtures->load((int) $input->getArgument('count') ?? 1);

        $output->writeln('done! check your database.');

        return Command::SUCCESS;
    }

    protected function configure(): void
    {
        $this->addArgument('count', InputArgument::OPTIONAL, 'Count of users to be created.');

        $this->setHelp("This command allows you to seed database with fake Data for testing purposes ... \nIt will not reset Database. \nThis means, the Data will be just added to Database.\n");
    }
}