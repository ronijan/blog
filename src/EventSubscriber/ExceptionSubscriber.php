<?php

namespace App\EventSubscriber;

use App\Helper\Logger;
use App\Service\SystemLogsService;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly SystemLogsService $systemLogsService) {}

    #[ArrayShape([KernelEvents::EXCEPTION => "array[]"])]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [['logException', 0]],
        ];
    }

    public function logException(ExceptionEvent $event): void
    {
        $message = $event->getThrowable()->getMessage();

        Logger::addLog($message, 'critical');
        
        $this->systemLogsService->add('Exception', $message);
    }
}