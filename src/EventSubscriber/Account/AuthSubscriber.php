<?php

namespace App\EventSubscriber\Account;

use App\Helper\SessionName;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AuthSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly UrlGeneratorInterface $urlGenerator) {}

    #[ArrayShape([KernelEvents::REQUEST => "array[]"])]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [
                ['processTwoStepVerification', 30],
            ],
        ];
    }

    public function processTwoStepVerification(RequestEvent $event): ?RedirectResponse
    {
        $session = $event->getRequest()->getSession()->get(SessionName::TWO_F_A);

        if (isset($session) && $session === sha1('TRUE')) {
            return new RedirectResponse($this->urlGenerator->generate('app_profile_security_2fa_index'));
        }

        return null;
    }
}