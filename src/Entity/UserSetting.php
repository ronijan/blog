<?php

namespace App\Entity;

use App\Helper\UserSettingConfig;
use App\Repository\UserSettingRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UserSettingRepository::class)]
class UserSetting
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(inversedBy: 'userSetting', cascade: ['persist', 'remove'])]
    private ?User $user = null;

    #[ORM\Column]
    private int $paginationLimit = UserSettingConfig::DEFAULT_PAGINATION_LIMIT;

    #[ORM\Column]
    private bool $autoDeleteNotifications = UserSettingConfig::AUTO_DELETE_OLD_NOTIFICATIONS;

    #[ORM\Column]
    private bool $notificationSending = UserSettingConfig::INFORM_USER_NEW_NOTIFICATION_ARRIVED;

    #[ORM\Column]
    private int $logsLimit = UserSettingConfig::DEFAULT_LOGS_LIMIT;

    #[ORM\Column]
    private bool $logSending = UserSettingConfig::SENDING_LOG_VIA_EMAIL;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?DateTimeInterface $updatedAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?DateTimeInterface $createdAt = null;

    public function __construct()
    {
        $this->setCreatedAt(new DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPaginationLimit(): ?int
    {
        return $this->paginationLimit;
    }

    public function setPaginationLimit(int $paginationLimit): self
    {
        $this->paginationLimit = $paginationLimit;

        return $this;
    }

    public function shouldAutoDeleteNotifications(): bool
    {
        return $this->autoDeleteNotifications;
    }

    public function setAutoDeleteNotifications(bool $autoDeleteNotifications): self
    {
        $this->autoDeleteNotifications = $autoDeleteNotifications;

        return $this;
    }


    public function isNotificationSending(): bool
    {
        return $this->notificationSending;
    }

    public function setNotificationSending(bool $notificationSending): self
    {
        $this->notificationSending = $notificationSending;

        return $this;
    }

    public function getLogsLimit(): ?int
    {
        return $this->logsLimit;
    }

    public function setLogsLimit(int $logsLimit): self
    {
        $this->logsLimit = $logsLimit;

        return $this;
    }

    public function isLogSending(): bool
    {
        return $this->logSending;
    }

    public function setLogSending(bool $logSending): self
    {
        $this->logSending = $logSending;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
