<?php

namespace App\Entity;

use App\Entity\Account\Profile;
use App\Entity\Account\ResetPassword;
use App\Entity\Account\TwoFactorAuth;
use App\Entity\Account\TwoFactorAuthDevices;
use App\Entity\Blog\Comment;
use App\Entity\Blog\ReplyComment;
use App\Repository\UserRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: ['email'], message: 'There is already an Account with this email')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\Column(type: 'string', length: 100)]
    private string $username;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private string $email;

    #[ORM\Column(type: 'json')]
    private array $roles = ['ROLE_USER'];

    #[ORM\Column(type: 'string')]
    private string $password;

    #[ORM\Column]
    private bool $isWriter = false;

    #[ORM\Column(type: 'boolean')]
    private bool $isVerified = false;

    #[ORM\Column(type: 'boolean')]
    private bool $isDeleted = false;

    #[ORM\Column(type: 'boolean')]
    private bool $isDisabled = false;

    #[ORM\Column(type: 'integer')]
    private int $loginCounts = 0;

    #[ORM\Column(length: 50)]
    private string $locale = 'en';

    #[ORM\Column(type: 'string', length: 200, nullable: true)]
    private ?string $tempEmail;

    #[ORM\Column(type: 'string', length: 200, nullable: true)]
    private ?string $token;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?DateTimeInterface $lastLoggedIn;

    #[ORM\Column(type: 'datetime')]
    private DateTimeInterface $lastPasswordUpdated;

    #[ORM\Column(type: 'datetime')]
    private DateTimeInterface $updatedAt;

    #[ORM\Column(type: 'datetime')]
    private DateTimeInterface $createdAt;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: TwoFactorAuth::class)]
    private $twoFactorAuths;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: TwoFactorAuthDevices::class)]
    private $twoFactorAuthDevices;

    #[ORM\OneToOne(mappedBy: 'user', targetEntity: Profile::class, cascade: ['persist', 'remove'])]
    private $profile;

    #[ORM\OneToOne(mappedBy: 'user', targetEntity: ResetPassword::class, cascade: ['persist', 'remove'])]
    private $resetPassword;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Notification::class)]
    private Collection $notifications;

    #[ORM\OneToOne(mappedBy: 'user', cascade: ['persist', 'remove'])]
    private ?UserSetting $userSetting = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: LogActivities::class)]
    private Collection $logActivities;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Comment::class)]
    private Collection $comments;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: ReplyComment::class)]
    private Collection $replyComments;

    public function __construct()
    {
        $this->setCreatedAt(new DateTime());
        $this->twoFactorAuths = new ArrayCollection();
        $this->twoFactorAuthDevices = new ArrayCollection();
        $this->notifications = new ArrayCollection();
        $this->logActivities = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->replyComments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getLoginCounts(): ?int
    {
        return $this->loginCounts;
    }

    public function setLoginCounts(int $loginCounts): self
    {
        $this->loginCounts = $loginCounts;

        return $this;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function isWriter(): bool
    {
        return $this->isWriter;
    }

    public function setIsWriter(bool $isWriter): self
    {
        $this->isWriter = $isWriter;

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function isDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function isDisabled(): ?bool
    {
        return $this->isDisabled;
    }

    public function setIsDisabled(bool $isDisabled): self
    {
        $this->isDisabled = $isDisabled;

        return $this;
    }

    public function getTempEmail(): ?string
    {
        return $this->tempEmail;
    }

    public function setTempEmail(?string $tempEmail): self
    {
        $this->tempEmail = $tempEmail;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getLastLoggedIn(): ?DateTimeInterface
    {
        return $this->lastLoggedIn;
    }

    public function setLastLoggedIn(?DateTimeInterface $lastLoggedIn): self
    {
        $this->lastLoggedIn = $lastLoggedIn;

        return $this;
    }

    public function getLastPasswordUpdated(): ?DateTimeInterface
    {
        return $this->lastPasswordUpdated;
    }

    public function setLastPasswordUpdated(DateTimeInterface $lastPasswordUpdated): self
    {
        $this->lastPasswordUpdated = $lastPasswordUpdated;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection<int, TwoFactorAuth>
     */
    public function getTwoFactorAuths(): Collection
    {
        return $this->twoFactorAuths;
    }

    public function addTwoFactorAuth(TwoFactorAuth $twoFactorAuth): self
    {
        if (!$this->twoFactorAuths->contains($twoFactorAuth)) {
            $this->twoFactorAuths[] = $twoFactorAuth;
            $twoFactorAuth->setUser($this);
        }

        return $this;
    }

    public function removeTwoFactorAuth(TwoFactorAuth $twoFactorAuth): self
    {
        if ($this->twoFactorAuths->removeElement($twoFactorAuth)) {
            // set the owning side to null (unless already changed)
            if ($twoFactorAuth->getUser() === $this) {
                $twoFactorAuth->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, TwoFactorAuthDevices>
     */
    public function getTwoFactorAuthDevices(): Collection
    {
        return $this->twoFactorAuthDevices;
    }

    public function addTwoFactorAuthDevice(TwoFactorAuthDevices $twoFactorAuthDevice): self
    {
        if (!$this->twoFactorAuthDevices->contains($twoFactorAuthDevice)) {
            $this->twoFactorAuthDevices[] = $twoFactorAuthDevice;
            $twoFactorAuthDevice->setUser($this);
        }

        return $this;
    }

    public function removeTwoFactorAuthDevice(TwoFactorAuthDevices $twoFactorAuthDevice): self
    {
        if ($this->twoFactorAuthDevices->removeElement($twoFactorAuthDevice)) {
            // set the owning side to null (unless already changed)
            if ($twoFactorAuthDevice->getUser() === $this) {
                $twoFactorAuthDevice->setUser(null);
            }
        }

        return $this;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(?Profile $profile): self
    {
        // unset the owning side of the relation if necessary
        if ($profile === null && $this->profile !== null) {
            $this->profile->setUser(null);
        }

        // set the owning side of the relation if necessary
        if ($profile !== null && $profile->getUser() !== $this) {
            $profile->setUser($this);
        }

        $this->profile = $profile;

        return $this;
    }

    public function getResetPassword(): ?ResetPassword
    {
        return $this->resetPassword;
    }

    public function setResetPassword(?ResetPassword $resetPassword): self
    {
        // unset the owning side of the relation if necessary
        if ($resetPassword === null && $this->resetPassword !== null) {
            $this->resetPassword->setUser(null);
        }

        // set the owning side of the relation if necessary
        if ($resetPassword !== null && $resetPassword->getUser() !== $this) {
            $resetPassword->setUser($this);
        }

        $this->resetPassword = $resetPassword;

        return $this;
    }

    /**
     * @return Collection<int, Notification>
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications->add($notification);
            $notification->setUser($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->removeElement($notification)) {
            // set the owning side to null (unless already changed)
            if ($notification->getUser() === $this) {
                $notification->setUser(null);
            }
        }

        return $this;
    }

    public function getUserSetting(): ?UserSetting
    {
        return $this->userSetting;
    }

    public function setUserSetting(?UserSetting $userSetting): self
    {
        // unset the owning side of the relation if necessary
        if ($userSetting === null && $this->userSetting !== null) {
            $this->userSetting->setUser(null);
        }

        // set the owning side of the relation if necessary
        if ($userSetting !== null && $userSetting->getUser() !== $this) {
            $userSetting->setUser($this);
        }

        $this->userSetting = $userSetting;

        return $this;
    }

    /**
     * @return Collection<int, LogActivities>
     */
    public function getLogActivities(): Collection
    {
        return $this->logActivities;
    }

    public function addLogActivity(LogActivities $logActivity): self
    {
        if (!$this->logActivities->contains($logActivity)) {
            $this->logActivities->add($logActivity);
            $logActivity->setUser($this);
        }

        return $this;
    }

    public function removeLogActivity(LogActivities $logActivity): self
    {
        if ($this->logActivities->removeElement($logActivity)) {
            // set the owning side to null (unless already changed)
            if ($logActivity->getUser() === $this) {
                $logActivity->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
            $comment->setUser($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getUser() === $this) {
                $comment->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ReplyComment>
     */
    public function getReplyComments(): Collection
    {
        return $this->replyComments;
    }

    public function addReplyComment(ReplyComment $replyComment): self
    {
        if (!$this->replyComments->contains($replyComment)) {
            $this->replyComments->add($replyComment);
            $replyComment->setUser($this);
        }

        return $this;
    }

    public function removeReplyComment(ReplyComment $replyComment): self
    {
        if ($this->replyComments->removeElement($replyComment)) {
            // set the owning side to null (unless already changed)
            if ($replyComment->getUser() === $this) {
                $replyComment->setUser(null);
            }
        }

        return $this;
    }
}
