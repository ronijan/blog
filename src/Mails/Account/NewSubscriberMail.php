<?php

declare(strict_types=1);

namespace App\Mails\Account;

use App\Helper\Mailer;
use App\Mails\AbstractMail;
use App\Mails\MailInterface;
use App\Service\ConfigService;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

final class NewSubscriberMail extends AbstractMail implements MailInterface
{
    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly ConfigService $configService
    ) {
    }

    public function send(...$context): void
    {
        [$username, $userEmail, $token] = $context;

        $email = (new TemplatedEmail())
            ->from(
                new Address(
                    $this->configService->getParameter('no_reply'),
                    $this->configService->getParameter('app_name')
                )
            )
            ->to(new address($userEmail, $username))
            ->subject('Confirm to complete the Subscription')
            ->htmlTemplate('mails/account/new_subscriber.html.twig')
            ->context([
                'username' => $username,
                'token' => $token,
                'showUnSubscriptionLink' => true,
            ]);

        Mailer::catch(sprintf('/newsletter/confirm/email/%s', $token));

        $this->mailer->send($email);
    }
}