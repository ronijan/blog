<?php

declare(strict_types=1);

namespace App\Mails\Account;

use App\Helper\Mailer;
use App\Mails\AbstractMail;
use App\Mails\MailInterface;
use App\Service\ConfigService;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

final class ContactFormSendCopyToUserMail extends AbstractMail implements MailInterface
{
    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly ConfigService $configService
    ) {
    }

    public function send(...$context): void
    {
        [$username, $userEmail, $subject, $message] = $context;

        $email = (new TemplatedEmail())
            ->from(
                new Address(
                    $this->configService->getParameter('no_reply'),
                    $this->configService->getParameter('app_name')
                )
            )
            ->to(new address($userEmail, $username))
            ->subject('A copy from your message on contact form')
            ->htmlTemplate('mails/account/contact_form_send_copy_to_user.html.twig')
            ->context([
                'username' => $username,
                'subject' => $subject,
                'message' => $message,
            ]);

        Mailer::catch($message);

        $this->mailer->send($email);
    }
}