<?php

declare(strict_types=1);

namespace App\Mails\Account;

use App\Helper\Mailer;
use App\Mails\AbstractMail;
use App\Mails\MailInterface;
use App\Service\ConfigService;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

final class PasswordChangedMail extends AbstractMail implements MailInterface
{
    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly ConfigService $configService
    ) {
    }

    public function send(...$context): void
    {
        [$username, $email] = $context;

        $template = (new TemplatedEmail())
            ->from(
                new Address(
                    $this->configService->getParameter('no_reply'),
                    $this->configService->getParameter('app_name')
                )
            )
            ->to(new address($email, $username))
            ->subject('Your password changed')
            ->htmlTemplate('mails/account/profile_change_password.html.twig')
            ->context([
                'username' => $username,
            ]);

        Mailer::catch(sprintf('PW for user [%s] has been changed.', $email));

        $this->mailer->send($template);
    }
}