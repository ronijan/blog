<?php

declare(strict_types=1);

namespace App\Mails\Blog;

use App\Helper\Mailer;
use App\Mails\AbstractMail;
use App\Mails\MailInterface;
use App\Service\ConfigService;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

final class WelcomeWriterMail extends AbstractMail implements MailInterface
{
    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly ConfigService $configService
    ) {
    }

    public function send(...$context): void
    {
        [$username, $userEmail] = $context;

        $email = (new TemplatedEmail())
            ->from(
                new Address(
                    $this->configService->getParameter('no_reply'),
                    $this->configService->getParameter('app_name')
                )
            )
            ->to(new address($userEmail, $username))
            ->subject('Welcome to our Blog')
            ->htmlTemplate('mails/blog/welcome_msg_to_writer.html.twig')
            ->context([
                'username' => $username,
            ]);

        Mailer::catch(sprintf('Message has been sent to %s right now.', $userEmail));

        $this->mailer->send($email);
    }
}