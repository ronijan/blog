<?php

declare(strict_types=1);

namespace App\Mails\Admin;

use App\Helper\Mailer;
use App\Mails\AbstractMail;
use App\Mails\MailInterface;
use App\Service\ConfigService;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

final class NewUserViaAdminMail extends AbstractMail implements MailInterface
{
    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly ConfigService $configService
    ) {
    }

    public function send(...$context): void
    {
        [$username, $userEmail, $otp] = $context;

        $email = (new TemplatedEmail())
            ->from(
                new Address(
                    $this->configService->getParameter('no_reply'),
                    $this->configService->getParameter('app_name')
                )
            )
            ->to(new address($userEmail, $username))
            ->subject('Account created for you')
            ->htmlTemplate('mails/admin/new_user_via_admin.html.twig')
            ->context([
                'username' => $username,
                'otp' => $otp,
            ]);

        Mailer::catch($otp);

        $this->mailer->send($email);
    }
}