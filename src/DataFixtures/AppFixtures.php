<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Helper\FakerHelper;
use App\Helper\TestimonialHelper;
use App\Service\Account\RegisterNewUserService;
use App\Service\Blog\CategoryService;
use App\Service\Blog\SocialShareService;
use App\Service\Blog\TagService;
use App\Service\ConfigService;
use App\Service\ContactFormService;
use App\Service\FAQService;
use App\Service\NewsletterService;
use App\Service\NotificationService;
use App\Service\TeamService;
use App\Service\TestimonialService;
use App\Service\UserService;
use Faker\Factory;

final class AppFixtures
{
    // run this command $ php bin/console app:seeds 5 (see CHANGE_ME in Helper/FakerHelper::class)

    public function __construct(
        private readonly RegisterNewUserService $registerNewUserService,
        private readonly ContactFormService $contactFormService,
        private readonly UserService $userService,
        private readonly NewsletterService $newsletterService,
        private readonly NotificationService $notificationService,
        private readonly ConfigService $configService,
        private readonly CategoryService $categoryService,
        private readonly TagService $tagService,
        private readonly TestimonialService $testimonialService,
        private readonly FAQService $frequentlyAskedQuestionsService,
        private readonly SocialShareService $socialShareService,
        private readonly TeamService $teamService,
    ) {
    }

    public function load(int $count = FakerHelper::USERS_LIMIT): void
    {
        if ($this->configService->getParameter('ALLOW_COMMAND') === 'on') {

            $this->addUsers($count);

            $this->addContactFormMessages();

            $this->addSubscribers();

            $this->addTestimonials();

            $this->addTeamMembers();

            $this->addFAQ();

            $this->addBlogCategories();

            $this->addBlogTags();

            $this->addBlogSocialShareLinks();

            $this->notificationService->notifyAdmin(
                'Data Fixtures',
                sprintf(
                    'Data Fixtures (Users, Messages, Subscribers.. has been loaded successfully. [%s]',
                    $count
                )
            );

            return;
        }

        $this->notificationService->notifyAdmin(
            'Data Fixtures',
            'Data Fixtures cannot be loaded. Please check the Configration.'
        );
    }

    private function addUsers(int $count): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < $count; $i++) {
            $user = $this->registerNewUserService->setUp(
                $faker->name(),
                $faker->email(),
                $faker->password(8),
                null
            );

            $this->notificationService->create(
                $user,
                $faker->text(100),
                $faker->paragraph(3)
            );
        }
    }

    private function addContactFormMessages(): void
    {
        if (FakerHelper::ALLOW_CONTACT_FORM_MESSAGES) {
            $faker = Factory::create();

            for ($i = 0; $i < FakerHelper::CONTACT_MESSAGES_LIMIT; $i++) {
                $this->contactFormService->create(
                    $faker->name(),
                    $faker->email(),
                    $faker->text(100),
                    $faker->paragraph(5),
                    false
                );
            }
        }
    }

    private function addSubscribers(): void
    {
        if (FakerHelper::ALLOW_SUBSCRIPERS) {
            $faker = Factory::create();

            for ($i = 0; $i < FakerHelper::SUBSCRIPERS_LIMIT; $i++) {
                $this->newsletterService->addNewSubscriber($faker->email());
            }
        }
    }

    private function addBlogCategories(): void
    {
        if (FakerHelper::ALLOW_BLOG_CATEGORIES) {
            $faker = Factory::create();

            for ($i = 0; $i < FakerHelper::BLOG_CATEGORIES_LIMIT; $i++) {
                $this->categoryService->add($faker->word());
            }
        }
    }

    private function addBlogTags(): void
    {
        if (FakerHelper::ALLOW_BLOG_TAGS) {
            $faker = Factory::create();

            for ($i = 0; $i < FakerHelper::BLOG_TAGS_LIMIT; $i++) {
                $this->tagService->add($faker->word());
            }
        }
    }

    private function addTestimonials(): void
    {
        if (FakerHelper::ALLOW_TESTIMONIALS) {
            $faker = Factory::create();

            for ($i = 0; $i < FakerHelper::TESTIMONIALS_LIMIT; $i++) {
                $this->testimonialService->create(
                    $faker->firstName(),
                    $faker->lastName(),
                    $faker->jobTitle(),
                    $faker->sentence(20),
                    TestimonialHelper::DEFAULT_AVATAR,
                    null,
                    null,
                    true
                );
            }
        }
    }

    private function addFAQ()
    {
        if (FakerHelper::ALLOW_FAQ) {
            $faker = Factory::create();

            for ($i = 0; $i < FakerHelper::FAQ_LIMIT; $i++) {
                $this->frequentlyAskedQuestionsService->create(
                    $faker->sentence(),
                    $faker->sentence(20)
                );
            }
        }
    }

    private function addBlogSocialShareLinks()
    {
        if (FakerHelper::ALLOW_BLOG_SOCIAL_SHARE_LINKS) {
            $this->socialShareService->add(
                'twitter',
                'bi-twitter',
                '#4141f4',
                'https://twitter.com/intent/tweet?url=URL&text=TEXT'
            );

            $this->socialShareService->add(
                'facebook',
                'bi-facebook',
                '#2424d7',
                'https://www.facebook.com/sharer/sharer.php?u=URL'
            );

            $this->socialShareService->add(
                'pinterest',
                'bi-pinterest',
                '#f21212',
                'https://pinterest.com/pin/create/button/?url=URL&description=TEXT'
            );

            $this->socialShareService->add(
                'linkedin',
                'bi-linkedin',
                '#4772f4',
                'https://www.linkedin.com/shareArticle?mini=true&url=URL'
            );

            $this->socialShareService->add(
                'email',
                'bi-envelope',
                '#aaa702',
                'mailto:?&subject=TEXT&body=URL'
            );
        }
    }

    private function addTeamMembers()
    {
        if (FakerHelper::ALLOW_TEAMS_MEMBERS) {
            $faker = Factory::create();

            for ($i = 0; $i < FakerHelper::TEAM_MEMBERS_LIMIT; $i++) {
                $this->teamService->createOrUpdate(
                    null,
                    $faker->firstName(),
                    $faker->lastName(),
                    $faker->jobTitle(),
                    true,
                );
            }
        }
    }
}