<?php

namespace App\Security;

use App\Exception\InvalidConfigArgumentException;
use App\Helper\SessionName;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\LogActivitiesService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use App\Traits\RemoteTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class AppLoginFormAuthenticator extends AbstractLoginFormAuthenticator
{
    use TargetPathTrait;
    use RemoteTrait;
    use FormValidationTrait;

    public const LOGIN_ROUTE = 'app_login';

    public function __construct(
        private readonly UrlGeneratorInterface $urlGenerator,
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly UserService $userService,
        private readonly LogActivitiesService $logActivitiesService
    ) {
    }

    public function authenticate(Request $request): Passport
    {
        $email = $this->validate($request->request->get('email', ''));

        $request->getSession()->set(Security::LAST_USERNAME, $email);

        return new Passport(
            new UserBadge($email),
            new PasswordCredentials($this->validate($request->request->get('password', ''))),
            [
                new CsrfTokenBadge('authenticate', $this->validate($request->request->get('_csrf_token'))),
            ]
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        $user = $token->getUser();

        $this->updateAndSendLogToUser($user);

        $this->checkTwoStepVerificationAndSendOtp($user, $request, $firewallName);

        if ($targetPath = $this->getTargetPath($request->getSession(), $firewallName)) {
            return new RedirectResponse($targetPath);
        }

        return new RedirectResponse($this->urlGenerator->generate('app_profile_index'));
    }

    protected function getLoginUrl(Request $request): string
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }

    private function updateAndSendLogToUser(UserInterface $user): void
    {
        try {
            $this->userService->updateLastLoggedInAndLoginCounts($user);

            $logActivity = $this->getIP(true);
            $this->logActivitiesService->add($user, 'newLoggedIn', $logActivity);

            $this->logActivitiesService->sendLogToUserIfEnabled($user, $logActivity);
        } catch (InvalidConfigArgumentException | TransportExceptionInterface $e) {
            //
        }
    }

    private function checkTwoStepVerificationAndSendOtp(UserInterface $user, Request $request, string $firewallName): ?RedirectResponse
    {
        if ($user && $this->handelTwoFactorAuthService->checkTwoStepVerification($user)) {
            $this->handelTwoFactorAuthService->sendOtp($user);

            $request->getSession()->set(SessionName::TWO_F_A, sha1('TRUE'));
            $request->getSession()->set(SessionName::TWO_F_A_USER_IDENTIFIER, sha1($user->getUserIdentifier()));

            $this->saveTargetPath($request->getSession(), $firewallName, $this->urlGenerator->generate('app_profile_index'));

            return new RedirectResponse($this->urlGenerator->generate('app_profile_security_2fa_index'));
        }

        return null;
    }
}