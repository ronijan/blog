<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\User as AppUser;
use App\Exception\Account\AccountIsDeletedException;
use App\Exception\Account\AccountIsDisabledException;
use App\Exception\Account\AccountIsExpiredException;
use App\Exception\Account\AccountIsNotVerifiedException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user): void
    {
        if (!$user instanceof AppUser) {
            return;
        }

        if ($user->isDeleted() === true) {
            throw new AccountIsDeletedException('Contact support to get your account back.');
        }

        if ($user->isDisabled() === true) {
            throw new AccountIsDisabledException('You account has been disabled. Contact us to enable it again.');
        }

        if (!$user->isVerified()) {
            throw new AccountIsNotVerifiedException('You need to confirm your email.');
        }

        if ($user->getResetPassword()) {
            throw new AccountIsDeletedException('Please reset your password.');
        }
    }

    public function checkPostAuth(UserInterface $user): void
    {
        if (!$user instanceof AppUser) {
            return;
        }

        if ($user->isDeleted()) {
            throw new AccountIsExpiredException('Your user Account no longer exists.');
        }
    }
}