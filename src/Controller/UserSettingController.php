<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\UserSettingService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/setting')]
class UserSettingController extends AbstractBaseController
{
    use FormValidationTrait;

    #[Route('/update-limit', name: 'app_user_setting_store', methods: 'post')]
    public function storeLimit(Request $request, UserSettingService $userSettingService): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $uri = $this->validate($request->request->get('uri'));
        
        $limit = $this->validateNumber($request->request->get('limit'));

        if (!empty($uri) && !empty($limit)) {
            $userSettingService->updatePaginationLimit($this->getUser(), $limit);
            return $this->redirectToRoute($uri);
        }

        return new Response("Failed to update Limit.", Response::HTTP_NOT_FOUND);
    }
}
