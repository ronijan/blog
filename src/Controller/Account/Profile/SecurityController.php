<?php

declare(strict_types=1);

namespace App\Controller\Account\Profile;

use App\Controller\AbstractBaseController;
use App\Helper\UserHelper;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\Account\TwoFactorAuthDevicesService;
use App\Service\Account\TwoFactorAuthService;
use App\Service\LogActivitiesService;
use App\Service\NotificationService;
use App\Service\UserService;
use App\Service\UserSettingService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/profile/security')]
class SecurityController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const PROFILE_ROUTE = 'app_profile_index';
    private const TWO_F_A_ROUTE = 'app_profile_security_2fa_index';
    private const PROFILE_SECURITY_ROUTE = 'app_profile_security_index';
    private const TOO_MANY_REQUEST_ROUTE = 'app_meta_too_many_requests';
    private const LOGS_ACTIVITIES_ROUTE = 'app_profile_security_logs_index';

    public function __construct(
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly UserService $userService,
        private readonly ProfileService $profileService,
        private readonly TwoFactorAuthDevicesService $twoFactorAuthDevicesService,
        private readonly TwoFactorAuthService $twoFactorAuthService,
        private readonly NotificationService $notificationService,
        private readonly TranslatorInterface $translator,
        private readonly UserSettingService $userSettingService,
        private readonly LogActivitiesService $logActivitiesService
    ) {}

    #[Route('/home', name: 'app_profile_security_index')]
    public function index(): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $profile = $this->profileService->getByUser($user);
        $auth = $this->twoFactorAuthService->getByUser($user);
        $notifications = $this->notificationService->getAllUnseenByUser($user);

        return $this->render('account/profile/security.html.twig', [
            'profile' => $profile,
            'auth' => $auth,
            'notifications' => $notifications,
        ]);
    }

    #[Route('/delete-verified-devices', name: 'app_profile_security_delete_devices', methods: 'post')]
    public function deleteVerifiedDevices(Request $request): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $user = $this->getUser();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($user, $currentPassword)) {
            $this->addFlash('warning', $this->translator->trans('Please type your current password'));
            return $this->redirectToRoute(self::PROFILE_SECURITY_ROUTE);
        }

        if (!$this->validateCheckbox($request->request->get('delete'))) {
            $this->addFlash('warning', $this->translator->trans('Please try once again'));
            return $this->redirectToRoute(self::PROFILE_SECURITY_ROUTE);
        }

        if ($this->twoFactorAuthDevicesService->delete($user)) {
            $this->addFlash('success', $this->translator->trans('Done! No device has been left'));
        }

        return $this->redirectToRoute(self::PROFILE_SECURITY_ROUTE);
    }

    #[Route('/log-activities/home', name: 'app_profile_security_logs_index')]
    public function logActivitiesIndex(): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getAllUnseenByUser($user);

        $logActivitiesLimit = $this->userSettingService->getLogActivitiesLimitByUser($user);

        $logCounts = $this->logActivitiesService->cleanUpByUserAndLimit($user, $logActivitiesLimit);

        if ($logCounts > 4) {
            $this->notificationService->create(
                $user,
                $this->translator->trans('Log Activities'),
                $this->translator->trans('Total Logs has been deleted') . ' [' . $logCounts . ']'
            );
        }

        $logs = $this->logActivitiesService->getAllByUserAndLogsLimit($user, $logActivitiesLimit);

        $isSendingLogEnabled = $this->logActivitiesService->isSendingEmailEnabledByUser($user);

        return $this->render('account/profile/log_activities.html.twig', [
            'logs' => $logs,
            'profile' => $profile,
            'notifications' => $notifications,
            'logsLimit' => $logActivitiesLimit,
            'isSendingLogEnabled' => $isSendingLogEnabled,
        ]);
    }

    #[Route('/update-logs-limit', name: 'app_profile_security_logs_store_limit', methods: 'post')]
    public function storeLogLimit(Request $request): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();
 
        $limit = $this->validateNumber($request->request->get('option'));

        $this->userSettingService->updateLogsLimit($this->getUser(), $limit);

        return $this->redirectToRoute(self::LOGS_ACTIVITIES_ROUTE);
    }

    #[Route('/delete-log', name: 'app_profile_security_logs_delete_one', methods: 'post')]
    public function deleteLog(Request $request): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        if ($this->isGranted(UserHelper::ROLE_ADMIN)) {

            $logActivity = $this->logActivitiesService->getById(
                $this->validateNumber($request->request->get('id'))
            );

            if ($logActivity) {
                $this->logActivitiesService->delete($logActivity);
                $this->addFlash('success', $this->translator->trans('Log has been deleted successfully.'));
                return $this->redirectToRoute(self::LOGS_ACTIVITIES_ROUTE);
            }
        }

        $this->addFlash('warning', $this->translator->trans('Hmm. Log could not be delete'));

        return $this->redirectToRoute(self::LOGS_ACTIVITIES_ROUTE);
    }

    #[Route('/send-log-per-email', name: 'app_profile_security_logs_send_log', methods: 'post')]
    public function sendLogViaEmail(Request $request): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $shouldLogBeSent = $this->validateCheckbox($request->request->get('sendLog'));

        $this->userSettingService->updateLogSending($this->getUser(), $shouldLogBeSent);

        return $this->redirectToRoute(self::LOGS_ACTIVITIES_ROUTE);
    }
}
