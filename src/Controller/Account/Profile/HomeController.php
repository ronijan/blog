<?php

declare(strict_types=1);

namespace App\Controller\Account\Profile;

use App\Controller\AbstractBaseController;
use App\Helper\UserHelper;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\Account\TwoFactorAuthService;
use App\Service\FileHandlerService;
use App\Service\FileUploaderService;
use App\Service\NotificationService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/profile')]
class HomeController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const PROFILE_ROUTE = 'app_profile_index';

    public function __construct(
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly ProfileService $profileService,
        private readonly TwoFactorAuthService $twoFactorAuthService,
        private readonly NotificationService $notificationService,
        private readonly TranslatorInterface $translator
    ) {}

    #[Route('/home', name: 'app_profile_index')]
    public function index(): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $profile = $this->profileService->getByUser($user);
        $auth = $this->twoFactorAuthService->getByUser($user);
        $notifications = $this->notificationService->getAllUnseenByUser($user);

        return $this->render('account/profile/index.html.twig', [
            'auth' => $auth,
            'supporterEmail' => $this->getParameter('supporter_email'),
            'profile' => $profile,
            'notifications' => $notifications,
        ]);
    }

    #[Route('/avatar', name: 'app_profile_change_avatar', methods: 'post')]
    public function uploadAvatar(
        Request $request,
        FileUploaderService $uploader,
        FileHandlerService $fileHandler
    ): Response {
        $this->granteUserRoleUserOrRedirectToLogin();
        
        $user = $this->getUser();

        $profile = $this->profileService->getByUser($user);

        $path = $this->getParameter('avatar_dir');

        if ($profile) {
            if ($this->validateCheckbox($request->request->get('removeAvatar'))) {
                // delete the avatar at first
                $fileHandler->unlinkFile($path, $profile->getName());
                // then reset it in profile table
                $this->profileService->update($profile, UserHelper::DEFAULT_AVATAR, 0, '', false);
                $this->addFlash('success', $this->translator->trans('Avatar has been deleted'));

                return $this->redirectToRoute(self::PROFILE_ROUTE);
            }

            /** @var UploadedFile $file */
            $file = $request->files->get('avatar');

            if (!$file) {
                $this->addFlash('warning', $this->translator->trans('Please choose a valid image'));
                return $this->redirectToRoute(self::PROFILE_ROUTE);
            }

            $ext = $file->getClientOriginalExtension();
            $size = (int)$file->getSize();

            if (!$fileHandler->isImageExtensionAllowed($ext)) {
                $this->addFlash('warning', $this->translator->trans('Extension is not allowed'));
                return $this->redirectToRoute(self::PROFILE_ROUTE);
            }

            // update the avatar
            if ($profile->isUploaded()) {
                // delete the old one at first
                $fileHandler->unlinkFile($path, $profile->getName());
                // upload the new one
                $fileName = $uploader->upload($file);
                $this->profileService->update($profile, $fileName, $size, $ext);
                $this->addFlash('success', $this->translator->trans('Avatar has been updated'));

                return $this->redirectToRoute(self::PROFILE_ROUTE);
            }

            $fileName = $uploader->upload($file);
            $this->profileService->update($profile, $fileName, $size, $ext);
            $this->addFlash('success', $this->translator->trans('Avatar has been uploaded'));

            return $this->redirectToRoute(self::PROFILE_ROUTE);
        }

        $this->addFlash('warning', $this->translator->trans('Failed to upload avatar'));

        return $this->redirectToRoute(self::PROFILE_ROUTE);
    }
}
