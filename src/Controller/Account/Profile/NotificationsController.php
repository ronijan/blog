<?php

declare(strict_types=1);

namespace App\Controller\Account\Profile;

use App\Controller\AbstractBaseController;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\NotificationService;
use App\Service\PaginationService;
use App\Service\UserSettingService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/notification')]
class NotificationsController extends AbstractBaseController
{
    use FormValidationTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const NOTIFICATION_ROUTE_INDEX = 'app_profile_notification_index';
    private const PAGE_NOT_FOUND_ROUTE_INDEX = 'app_meta_page_not_found';

    public function __construct(
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly ProfileService $profileService,
        private readonly NotificationService $notificationService,
        private readonly UserSettingService $userSettingService,
        private readonly TranslatorInterface $translator
    ) {}

    #[Route('/home/{page?}', name: 'app_profile_notification_index')]
    public function index(PaginationService $paginationService, ?int $page): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $page = $this->validateNumber($page);

        if ($page < 0) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getAllUnseenByUser($user);

        $this->notificationService->updateIsSeen($notifications);

        $limit = $this->userSettingService->getPaginationLimit($user);

        $offset = $paginationService->getOffset($page, $limit);

        $allNotifications = $this->notificationService->getAllByUserWithOffsetAndLimit($user, $offset, $limit);

        $pagination = $paginationService->paginate(
            $this->notificationService->getAllByUser($user),
            $page,
            $limit
        );

        $autoDeleteNotifications = $this->userSettingService->isAutoDeleteNotificationsEnabled($user);

        if ($autoDeleteNotifications) {
            $this->notificationService->deleteAllOlderThanSixMonthsByUser($user);
        }

        $isActiveSendNotification = $this->userSettingService->isActiveSendNotification($user);

        return $this->render('account/profile/notifications.html.twig', [
            'allNotifications' => $allNotifications,
            'profile' => $profile,
            'notifications' => $notifications,
            'pagination' => $pagination,
            'paginationLimit' => $limit,
            'autoDeleteNotifications' => $autoDeleteNotifications,
            'isActiveSendNotification' => $isActiveSendNotification
        ]);
    }

    #[Route('/view/{id}', name: 'app_profile_notification_show', methods: ['post', 'get'])]
    public function show(string $id): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $notificationId = $this->validateNumber($id);

        if ($notificationId <= 0) {
            $this->addFlash('warning', $this->translator->trans('Notification cannot be viewed'));
            return $this->redirectToRoute(self::NOTIFICATION_ROUTE_INDEX);
        }

        $notificationToView = $this->notificationService->getByUserAndId($user, $notificationId);
        $profile = $this->profileService->getByUser($user);
        $notifications = $this->notificationService->getAllUnseenByUser($user);

        if ($notificationToView === null) {
            $this->addFlash('warning', $this->translator->trans('Notification cannot be found'));
            return $this->redirectToRoute(self::NOTIFICATION_ROUTE_INDEX);
        }

        return $this->render('account/profile/notification/view.html.twig', [
            'notification' => $notificationToView,
            'profile' => $profile,
            'notifications' => $notifications,
        ]);
    }

    #[Route('/delete-old-notifications', name: 'app_profile_notification_delete_old_notifications', methods: 'post')]
    public function deleteOldNotifications(Request $request): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $deleteOldNotifications = $this->validateCheckbox($request->request->get('deleteOldNotifications'));

        $this->userSettingService->updateAutoDeleteNotifications($this->getUser(), $deleteOldNotifications);

        return $this->redirectToRoute(self::NOTIFICATION_ROUTE_INDEX);
    }

    #[Route('/notify-me-via-email', name: 'app_profile_notification_notify_via_email', methods: 'post')]
    public function notifyViaEmail(Request $request): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $notify = $this->validateCheckbox($request->request->get('notifyMe'));

        $this->userSettingService->updateNotificationSending($this->getUser(), $notify);

        return $this->redirectToRoute(self::NOTIFICATION_ROUTE_INDEX);
    }

    #[Route('/remove', name: 'app_profile_notification_delete', methods: 'post')]
    public function delete(Request $request): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $notificationId = $this->validateNumber($request->request->get('nId'));

        if ($notificationId <= 0) {
            $this->addFlash('warning', $this->translator->trans('Notification can not be deleted'));
            return $this->redirectToRoute(self::NOTIFICATION_ROUTE_INDEX);
        }

        $notification = $this->notificationService->getByUserAndId($this->getUser(), $notificationId);

        if ($notification && $this->validateCheckbox($request->request->get('delete'))) {
            $this->notificationService->delete($notification);
            $this->addFlash('success', $this->translator->trans('Notification has been deleted'));
        }

        return $this->redirectToRoute(self::NOTIFICATION_ROUTE_INDEX);
    }
}
