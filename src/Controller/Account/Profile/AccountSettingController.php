<?php

declare(strict_types=1);

namespace App\Controller\Account\Profile;

use App\Controller\AbstractBaseController;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\Account\TwoFactorAuthService;
use App\Service\NotificationService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Helper\Locale;

#[Route('/profile/account-setting')]
class AccountSettingController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const PROFILE_ACCOUNT_SETTING_ROUTE = 'app_profile_account_setting_index';

    public function __construct(
        private readonly UserService $userService,
        private readonly TranslatorInterface $translator,
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly ProfileService $profileService,
        private readonly TwoFactorAuthService $twoFactorAuthService,
        private readonly NotificationService $notificationService
    ) {}

    #[Route('/home', name: 'app_profile_account_setting_index')]
    public function index(): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $profile = $this->profileService->getByUser($user);

        $auth = $this->twoFactorAuthService->getByUser($user);

        $notifications = $this->notificationService->getAllUnseenByUser($user);

        return $this->render('account/profile/account_setting.html.twig', [
            'profile' => $profile,
            'auth' => $auth,
            'notifications' => $notifications,
            'localeOptions' => Locale::AVAILABLE,
        ]);
    }

    #[Route('/change-locale', name: 'app_profile_account_setting_store_locale', methods: 'post')]
    public function changeLocale(Request $request, UserService $userService): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $lang = $this->validate($request->request->get('lang'));

        if ($lang !== '') {
            $userService->updateLocale($this->getUser(), $lang);
        }

        $request->getSession()->set('_locale', $lang);

        $request->setLocale($lang);

        return $this->redirectToRoute(self::PROFILE_ACCOUNT_SETTING_ROUTE);
    }

    #[Route('/disable-account', name: 'app_profile_account_setting_disable_account', methods: 'post')]
    public function disableAccount(Request $request): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $user = $this->getUser();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($user, $currentPassword)) {
            $this->addFlash('warning', $this->translator->trans('Please type your current password'));
            return $this->redirectToRoute(self::PROFILE_ACCOUNT_SETTING_ROUTE);
        }

        if (!$this->validateCheckbox($request->request->get('disableAccount'))) {
            $this->addFlash('warning', $this->translator->trans('Please the checkbox confirmation is required'));
            return $this->redirectToRoute(self::PROFILE_ACCOUNT_SETTING_ROUTE);
        }

        $this->userService->disableAccount($user);

        $this->addFlash(
            'notice',
            $this->translator->trans(
                'Done! You will not be able to login anymore. Contact us when you want your account once again'
            )
        );

        $request->getSession()->invalidate(1);

        return $this->redirectToRoute(self::PROFILE_ACCOUNT_SETTING_ROUTE);
    }

    #[Route('/delete-account', name: 'app_profile_account_setting_delete_account', methods: 'post')]
    public function deleteAccount(Request $request): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();
        
        $user = $this->getUser();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($user, $currentPassword)) {
            $this->addFlash('warning', $this->translator->trans('Please type your current password'));
            return $this->redirectToRoute(self::PROFILE_ACCOUNT_SETTING_ROUTE);
        }

        if (!$this->validateCheckbox($request->request->get('deleteAccount'))) {
            $this->addFlash('warning', $this->translator->trans('All fields are required'));
            return $this->redirectToRoute(self::PROFILE_ACCOUNT_SETTING_ROUTE);
        }

        $this->userService->deleteAccount($user);

        $request->getSession()->invalidate(2);

        $this->addFlash(
            'notice',
            $this->translator->trans(
                'Account will be deleted within 6 months. You will not be able to login once again'
            )
        );

        return $this->redirectToRoute(self::PROFILE_ACCOUNT_SETTING_ROUTE);
    }
}
