<?php

declare(strict_types=1);

namespace App\Controller\Account\Profile;

use App\Controller\AbstractBaseController;
use App\Exception\Account\ExportPersonalDataException;
use App\Helper\Logger;
use App\Mails\Account\ChangeEmailRequestMail;
use App\Mails\Account\PasswordChangedMail;
use App\Service\Account\ExportPersonalDataService;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\Account\TwoFactorAuthService;
use App\Service\NotificationService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use JsonException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/profile/personal-data')]
class PersonalDataController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const PROFILE_PERSONAL_DATA_ROUTE = 'app_profile_personal_data_index';

    public function __construct(
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly UserService $userService,
        private readonly ProfileService $profileService,
        private readonly TwoFactorAuthService $twoFactorAuthService,
        private readonly NotificationService $notificationService,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route('/home', name: 'app_profile_personal_data_index')]
    public function index(): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $profile = $this->profileService->getByUser($user);
        $notifications = $this->notificationService->getAllUnseenByUser($user);

        return $this->render('account/profile/personal_data.html.twig', [
            'profile' => $profile,
            'notifications' => $notifications,
        ]);
    }

    #[Route('/change-username', name: 'app_profile_personal_data_username', methods: 'post')]
    public function updateUsername(Request $request): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $username = $this->validateUsernameAndReplaceSpace($request->request->get('newUsername'));
        $error = $this->usernameError($username);

        if ($error !== null) {
            $this->addFlash('warning', $error);
            return $this->redirectToRoute(self::PROFILE_PERSONAL_DATA_ROUTE);
        }

        $user = $this->userService->getByEmail($user->getUserIdentifier());

        if (strtolower($username) === mb_strtolower($user->getUsername())) {
            $this->addFlash('notice', $this->translator->trans('Username is the same. Not changed'));
            return $this->redirectToRoute(self::PROFILE_PERSONAL_DATA_ROUTE);
        }

        $this->userService->updateUsername($user, $username);
        $this->addFlash('success', $this->translator->trans('Username has been changed'));

        return $this->redirectToRoute(self::PROFILE_PERSONAL_DATA_ROUTE);
    }

    #[Route('/change-email', name: 'app_profile_personal_data_email', methods: 'post')]
    public function updateEmail(Request $request, ChangeEmailRequestMail $changeEmailRequestMail): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($user, $currentPassword)) {
            $this->addFlash('warning', $this->translator->trans('Please type your current password'));
            return $this->redirectToRoute(self::PROFILE_PERSONAL_DATA_ROUTE);
        }

        $newEmail = $this->validateEmail($request->request->get('newEmail'));

        if ($newEmail === null) {
            $this->addFlash('warning', $this->translator->trans('Please type a valid email'));
            return $this->redirectToRoute(self::PROFILE_PERSONAL_DATA_ROUTE);
        }

        if ($newEmail === $user->getUserIdentifier()) {
            $this->addFlash(
                'notice',
                $this->translator->trans('Please use another email address. You have entered the current one')
            );
            return $this->redirectToRoute(self::PROFILE_PERSONAL_DATA_ROUTE);
        }

        // check if another user with the same email (newEmail) exists to prevent duplicated entries
        if ($this->userService->getByEmail($newEmail)) {
            $this->addFlash('notice', $this->translator->trans('Please use another email address'));
            return $this->redirectToRoute(self::PROFILE_PERSONAL_DATA_ROUTE);
        }

        $token = $this->getRandomToken(32);
        $changeEmailRequestMail->send($user->getUsername(), $newEmail, $token);

        // save token and email in user table.
        // ignore isVerified - if the new email is not verified, the old one won't be updated.
        $this->userService->addTokenAndTempEmail($user, $newEmail, $token);

        $this->addFlash(
            'notice',
            $this->translator->trans('An email with a link has been sent to your new email. Please verify it now')
        );

        return $this->redirectToRoute(self::PROFILE_PERSONAL_DATA_ROUTE);
    }

    #[Route('/email/confirm/{token}', name: 'app_profile_personal_data_email_confirm')]
    public function confirmEmail(string $token): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $token = $this->validate($token);
        $user = $this->userService->getByToken($token);

        if ($user !== null) {
            $auth = $this->twoFactorAuthService->getByUser($user);

            $tempEmail = $user->getTempEmail();

            if (
                $auth && $auth->getAlternativeEmail() !== null &&
                strtolower($auth->getAlternativeEmail()) === strtolower($user->getEmail())
            ) {
                $this->twoFactorAuthService->updateAlternativeEmail($user, $tempEmail);
            }

            $this->userService->updateEmail($user, $tempEmail); // new one

            $this->userService->addTokenAndTempEmail($user);

            $this->addFlash('success', $this->translator->trans('Email address has been changed'));

            return $this->redirectToRoute(self::PROFILE_PERSONAL_DATA_ROUTE);
        }

        $this->addFlash('warning', $this->translator->trans('Token is invalid'));

        return $this->redirectToRoute(self::PROFILE_PERSONAL_DATA_ROUTE);
    }

    #[Route('/change-password', name: 'app_profile_personal_data_password', methods: 'post')]
    public function updatePassword(Request $request, PasswordChangedMail $passwordChangedMail): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($user, $currentPassword)) {
            $this->addFlash('warning', $this->translator->trans('Please type your current password'));
            return $this->redirectToRoute(self::PROFILE_PERSONAL_DATA_ROUTE);
        }

        $newPassword = $this->validate($request->request->get('newPassword'));
        $newPasswordError = $this->passwordError($newPassword);

        if ($newPasswordError !== null) {
            $this->addFlash('warning', $newPasswordError);
            return $this->redirectToRoute(self::PROFILE_PERSONAL_DATA_ROUTE);
        }

        $confirmNewPassword = $this->validate($request->request->get('confirmPassword'));
        $confirmNewPasswordError = $this->passwordError($newPassword);

        if ($confirmNewPasswordError !== null) {
            $this->addFlash('warning', $confirmNewPasswordError);
            return $this->redirectToRoute(self::PROFILE_PERSONAL_DATA_ROUTE);
        }

        if ($newPassword !== $confirmNewPassword) {
            $this->addFlash('warning', $this->translator->trans('Password does not match'));
            return $this->redirectToRoute(self::PROFILE_PERSONAL_DATA_ROUTE);
        }

        $hashedPassword = $this->userService->hashPassword($user, $newPassword);
        $this->userService->updatePassword($user, $hashedPassword);

        $passwordChangedMail->send($user->getUsername(), $user->getEmail());

        $this->addFlash(
            'success',
            $this->translator->trans('Password has been updated and an Info email has been sent to you')
        );

        return $this->redirectToRoute(self::PROFILE_PERSONAL_DATA_ROUTE);
    }

    #[Route('/export-personal-data', name: 'app_profile_personal_data_export_as_json', methods: ['get', 'post'])]
    public function exportAsJson(Request $request, ExportPersonalDataService $exportPersonalDataService): RedirectResponse|Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $user = $this->getUser();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($user, $currentPassword)) {
            $this->addFlash('warning', $this->translator->trans('Please type your current password'));
            return $this->redirectToRoute(self::PROFILE_PERSONAL_DATA_ROUTE);
        }

        try {
            $data = $exportPersonalDataService->prepareJsonHeader($user);
        } catch (ExportPersonalDataException | JsonException $e) {
            Logger::addLog('Failed to export personal data. ' . $e->getMessage(), 'error');
            $data = '';
        }

        if ($data === '') {
            return $this->redirectToRoute(self::PROFILE_PERSONAL_DATA_ROUTE);
        }

        return new Response($data);
    }
}