<?php

declare(strict_types=1);

namespace App\Controller\Account\Profile;

use App\Controller\AbstractBaseController;
use App\Helper\SessionName;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\TwoFactorAuthService;
use App\Service\FormSubmitGuardService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class SecurityTwoFactorAuthController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;

    private const PROFILE_ROUTE = 'app_profile_index';
    private const TWO_F_A_ROUTE = 'app_profile_security_2fa_index';
    private const PROFILE_SECURITY_ROUTE = 'app_profile_security_index';
    private const TOO_MANY_REQUEST_ROUTE = 'app_meta_too_many_requests';

    public function __construct(
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly UserService $userService,
        private readonly TwoFactorAuthService $twoFactorAuthService,
        private readonly TranslatorInterface $translator,
        private readonly FormSubmitGuardService $formSubmitGuardService,
    ) {}


    #[Route(path: '/token-auth', name: 'app_profile_security_2fa_index')]
    public function tokenAuth(SessionInterface $session): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        if ($session->get(SessionName::TWO_F_A) !== sha1('TRUE')) {
            return $this->redirectToRoute(self::HOME_ROUTE);
        }

        if ($this->formSubmitGuardService->isAllowedToSubmit(10)) {
            return $this->redirectToRoute(self::TOO_MANY_REQUEST_ROUTE);
        }

        return $this->render('account/profile/2fa.html.twig');
    }

    #[Route(path: '/enable-disable-2fa', name: 'app_profile_security_2fa_enable_disable', methods: 'post')]
    public function twoFactorAuthConfig(Request $request): Response
    {
        $this->granteUserRoleUserOrRedirectToLogin();

        $user = $this->getUser();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($user, $currentPassword)) {
            $this->addFlash('warning', $this->translator->trans('Please type your current password'));
            return $this->redirectToRoute(self::PROFILE_SECURITY_ROUTE);
        }

        $alternativeEmail = $this->validateEmail($request->request->get('aEmail'));

        if ($alternativeEmail !== null) {
            $alternativeEmail = $this->validate($alternativeEmail);
        }

        $isEnabled = $this->validateCheckbox($request->request->get('twoFa'));

        $this->twoFactorAuthService->updateAlternativeEmailAndIsEnabled($user, $alternativeEmail, $isEnabled);
        $status = $isEnabled ? $this->translator->trans('Enabled') : $this->translator->trans('Disabled');
        $this->addFlash('success', $this->translator->trans('Two-Factor-Authentication is') . ' ' . $status . '.');

        return $this->redirectToRoute(self::PROFILE_SECURITY_ROUTE);
    }

    #[Route(path: '/check-otp', name: 'app_profile_security_2fa_check_otp', methods: 'post')]
    public function checkOtp(Request $request): Response
    {
        $otp = $this->validate($request->request->get(SessionName::TWO_F_A));

        if ($otp === '' || $request->getSession()->get(SessionName::TWO_F_A) !== sha1('TRUE')) {
            $this->addFlash('warning', $this->translator->trans('Field is required'));
            return $this->redirectToRoute(self::TWO_F_A_ROUTE);
        }

        $auth = $this->twoFactorAuthService->getByUserAndOtp($this->getUser(), (int)$otp);

        if ($auth) {
            $this->twoFactorAuthService->updateOtp($auth);
            $request->getSession()->remove(SessionName::TWO_F_A);
            $request->getSession()->remove(SessionName::TWO_F_A_USER_IDENTIFIER);
            $this->formSubmitGuardService->finishProcess();

            if ($this->validateCheckbox($request->request->get('ignoreBrowser'))) {
                $this->handelTwoFactorAuthService->markDeviceAsTrustedOne($auth->getUser());
            }

            return $this->redirectToRoute(self::PROFILE_ROUTE);
        }

        $this->addFlash('warning', $this->translator->trans('One time password is not correct'));

        return $this->redirectToRoute(self::TWO_F_A_ROUTE);
    }

    #[Route(path: '/resend-otp', name: 'app_profile_security_2fa_resend_otp', methods: 'post')]
    public function resendOtp(Request $request): Response
    {
        if ($this->formSubmitGuardService->isAllowedToSubmit(5)) {
            return $this->redirectToRoute(self::TOO_MANY_REQUEST_ROUTE);
        }

        $email = $this->validateEmail($request->request->get('cEmail'));

        if ($email === null || $request->getSession()->get(SessionName::TWO_F_A) !== sha1('TRUE')) {
            $this->addFlash('warning', $this->translator->trans('Email is not valid'));
            return $this->redirectToRoute(self::TWO_F_A_ROUTE);
        }

        // get user (alternative email / current user email) or false
        $user = $this->handelTwoFactorAuthService->isUserExistByProvidedEmail($email);

        if ($user === null ||
            $request->getSession()->get(SessionName::TWO_F_A_USER_IDENTIFIER) !== sha1($user->getUserIdentifier())) {
            $this->addFlash(
                'notice',
                $this->translator->trans('If your email exists, then a OPT has been sent to your email right now')
            );
            return $this->redirectToRoute(self::TWO_F_A_ROUTE);
        }

        $this->handelTwoFactorAuthService->sendOtp($user);

        $this->addFlash(
            'notice',
            $this->translator->trans('A new OTP has been just sent your email') . ' `' . $email . '`'
        );

        return $this->redirectToRoute(self::TWO_F_A_ROUTE);
    }
}
