<?php

declare(strict_types=1);

namespace App\Controller\Account;

use App\Controller\AbstractBaseController;
use App\Mails\Account\PasswordChangedMail;
use App\Mails\Account\ResetPasswordRequestMail;
use App\Service\Account\ResetPasswordService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use App\Traits\ResetPasswordTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/reset-password')]
class ResetPasswordController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;
    use ResetPasswordTrait;

    private const PAGE_NOT_FOUND_ROUTE = 'app_meta_page_not_found';
    private const RESET_PASSWORD_ROUTE = 'app_forgot_password_request';

    public function __construct(
        private readonly UserService $userService,
        private readonly ResetPasswordService $resetPasswordService,
        private readonly TranslatorInterface $translator
    ) {}

    #[Route('', name: 'app_forgot_password_request')]
    public function index(): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_profile_index');
        }

        return $this->render('account/reset_password/request.html.twig');
    }

    // check and validate email
    #[Route('/check', name: 'app_forgot_password_request_check', methods: 'post')]
    public function checkEmail(Request $request, ResetPasswordRequestMail $resetPasswordRequestMail): Response
    {
        $email = $this->validateEmail($request->request->get('email'));

        if (empty($email)) {
            return $this->render('account/reset_password/check_email.html.twig');
        }

        $user = $this->userService->getByEmail($email);

        if (!$user) {
            return $this->render('account/reset_password/check_email.html.twig');
        }

        if ($user->isDeleted()) {
            $this->addFlash(
                'notice',
                $this->translator->trans('Contact support to get your account back. You have requested to delete your account')
            );
            return $this->redirectToRoute(self::HOME_ROUTE);
        }

        if ($user->isDisabled()) {
            $this->addFlash('notice', $this->translator->trans('Contact support to get your account back. Your account is disabled'));
            return $this->redirectToRoute(self::HOME_ROUTE);
        }

        $token = $this->getRandomToken(28);

        $this->resetPasswordService->createOrUpdate($user, $token);

        $resetPasswordRequestMail->send($user->getUsername(), $user->getEmail(), $token);

        $this->addFlash(
            'notice',
            $this->translator->trans('An email with a link has been sent to your provided email. Please click on that link to reset your password')
        );

        return $this->redirectToRoute(self::HOME_ROUTE);
    }

    #[Route('/reset/{token}', name: 'app_reset_password')]
    public function reset(string $token = null): Response
    {
        if (!$token) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE);
        }

        $this->storeResetPasswordTokenInSession($this->validate($token));

        $token = $this->getResetPasswordTokenFromSession();

        if (!$token) {
            $this->addFlash('warning', $this->translator->trans('Token is invalid. Please try again'));
            return $this->redirectToRoute('app_forgot_password_request');
        }

        $resetPassword = $this->resetPasswordService->getByToken($token);

        if (null === $resetPassword || null === $resetPassword->getUser()) {
            $this->addFlash(
                'notice',
                $this->translator->trans('Type your email and hit the button bellow. You will get then an email with a link')
            );

            return $this->redirectToRoute(self::RESET_PASSWORD_ROUTE);
        }

        $this->addFlash('notice', $this->translator->trans('You can reset your password now'));

        return $this->render('account/reset_password/reset.html.twig');
    }

    #[Route('/try-again', name: 'app_reset_password_show_on_warning')]
    public function showFormWithoutToken(): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_profile_index');
        }

        if (!$this->getResetPasswordTokenFromSession()) {
            return $this->redirectToRoute(self::RESET_PASSWORD_ROUTE);
        }

        return $this->render('account/reset_password/reset.html.twig');
    }

    #[Route('/store-password', name: 'app_forgot_password_request_store', methods: 'post')]
    public function storePassword(Request $request, PasswordChangedMail $passwordChangedMail): Response
    {
        $token = $this->getResetPasswordTokenFromSession();

        if(!$token) {
            $this->addFlash('warning', $this->translator->trans('Token is invalid'));
            return $this->redirectToRoute('app_forgot_password_request');
        }

        $newPassword = $this->validate($request->request->get('newPassword'));
        $confirmPassword = $this->validate($request->request->get('confirmPassword'));

        if (empty($newPassword) || empty($confirmPassword)) {
            $this->addFlash('warning', $this->translator->trans('All fields are required'));
            return $this->redirectToRoute('app_reset_password_show_on_warning');
        }

        if ($newPassword !== $confirmPassword) {
            $this->addFlash('warning', $this->translator->trans('Password does not match'));
            return $this->redirectToRoute('app_reset_password_show_on_warning');
        }

        $resetPassword = $this->resetPasswordService->getByToken($token);

        if ($resetPassword === null || $resetPassword->getUser() === null) {
            $this->addFlash('warning', $this->translator->trans('Token is invalid. Please try again'));
            return $this->redirectToRoute('app_forgot_password_request');
        }

        $user = $this->userService->getById($resetPassword->getUser()->getId());

        if (null === $user) {
            $this->addFlash('warning', $this->translator->trans('Token is invalid'));
            return $this->redirectToRoute('app_forgot_password_request');
        }

        $encodedPassword = $this->userService->hashPassword($user, $newPassword);

        $this->userService->updatePassword($user, $encodedPassword);

        $this->resetPasswordService->delete($resetPassword);

        $this->clearResetPasswordTokenFromSession();

        $passwordChangedMail->send($user->getUsername(), $user->getEmail());

        $this->addFlash('success', $this->translator->trans('Your password has been changed. Login to access your account'));

        return $this->redirectToRoute(self::LOGIN_ROUTE);
    }
}
