<?php

declare(strict_types=1);

namespace App\Controller\Account;

use App\Controller\AbstractBaseController;
use App\Mails\Account\AccountConfirmationMail;
use App\Service\Account\RegisterNewUserService;
use App\Service\NotificationService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class RegistrationController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;

    private const PROFILE_ROUTE = 'app_profile_index';
    private const REGISTER_ROUTE = 'app_register';
    private const PAGE_NOT_FOUND_ROUTE = 'app_meta_page_not_found';

    public function __construct(
        private readonly NotificationService $notificationService,
        private readonly UserService $userService,
        private readonly TranslatorInterface $translator
    ) {}

    #[Route('/sign-up', name: 'app_register')]
    public function index(): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute(self::PROFILE_ROUTE);
        }

        return $this->render('account/registration/register.html.twig');
    }

    #[Route('/register', name: 'app_register_new', methods: 'post')]
    public function new(
        Request $request, 
        AccountConfirmationMail $accountConfirmationMail, 
        RegisterNewUserService $registerNewUserService
    ): Response {
        if ($this->getUser()) {
            return $this->redirectToRoute(self::PROFILE_ROUTE);
        }

        $username = $this->validateUsernameAndReplaceSpace($request->request->get('username'));
        $usernameError = $this->usernameError($username);

        if ($usernameError !== null) {
            $this->addFlash('warning', $usernameError);
            return $this->redirectToRoute(self::REGISTER_ROUTE);
        }

        $email = $this->validateEmail($request->request->get('email'));

        if (empty($email)) {
            $this->addFlash('warning', $this->translator->trans('Email is invalid'));
            return $this->redirectToRoute(self::REGISTER_ROUTE);
        }

        $plainPassword = $this->validate($request->request->get('password'));
        $passwordError = $this->passwordError($plainPassword, true); // allow simple password

        if ($passwordError !== null) {
            $this->addFlash('warning', $passwordError);
            return $this->redirectToRoute(self::REGISTER_ROUTE);
        }

        if ($this->userService->getByEmail($email)) {
            $this->addFlash('notice', $this->translator->trans('Please login or reset your password'));
            return $this->redirectToRoute(self::LOGIN_ROUTE);
        }

        $token = $this->getRandomToken(16); // don't change
        $registerNewUserService->setUp($username, $email, $plainPassword, $token);
        $accountConfirmationMail->send($username, $email, $token);

        $this->addFlash(
            'notice',
            $this->translator->trans('An email was sent to your mailbox. Please follow instruction to get started')
        );

        $this->notificationService->notifyAdmin(
            'New User',
            'A new User has just registered via form'
        );

        return $this->redirectToRoute(self::HOME_ROUTE);
    }

    #[Route('/verify/email/{token}', name: 'app_register_verify_email')]
    public function verifyUserEmail(string $token): Response
    {
        $token = $this->validate($token);

        if (strlen($token) !== 32) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE);
        }

        $user = $this->userService->getByToken($token);

        if (!$user) {
            $this->addFlash('warning', $this->translator->trans('Your email could not be verified'));
            return $this->redirectToRoute(self::HOME_ROUTE);
        }

        $this->userService->updateTokenAndIsVerified($user);

        $this->addFlash('notice', $this->translator->trans('Your email address has been verified'));

        return $this->redirectToRoute(self::LOGIN_ROUTE);
    }
}
