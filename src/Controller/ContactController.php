<?php

declare(strict_types=1);

namespace App\Controller;

use App\Helper\Logger;
use App\Helper\SessionName;
use App\Mails\Account\ContactFormSendCopyToUserMail;
use App\Service\ContactFormService;
use App\Service\FormSecurityCodeService;
use App\Service\NotificationService;
use App\Traits\FormValidationTrait;
use App\Traits\RemoteTrait;
use App\Traits\UserAgentTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/contact-us')]
class ContactController extends AbstractBaseController
{
    use FormValidationTrait;
    use UserAgentTrait;
    use RemoteTrait;

    private const CONTACT_ROUTE_NAME = 'app_contact_form_index';

    public function __construct(
        private readonly ContactFormService $contactFormService,
        private readonly NotificationService $notificationService,
        private readonly TranslatorInterface $translator
    ) {}

    #[Route('/form', name: 'app_contact_form_index')]
    public function index(Request $request): Response
    {
        $formSecurityCode = new FormSecurityCodeService($request->getSession(), SessionName::CONTACT_FORM_SUBMISSION);

        return $this->render('meta/contact_form.html.twig', [
            'phoneNumber' => $this->getParameter('phone_number'),
            'address' => $this->getParameter('address'),
            'info_email' => $this->getParameter('info_email'),
            'numbers' => $formSecurityCode->getSecurityCode(),
        ]);
    }

    #[Route('/send-message', name: 'app_contact_form_new', methods: 'post')]
    public function new(Request $request, ContactFormSendCopyToUserMail $contactFormSendCopyToUser): Response
    {
        $formSecurity = new FormSecurityCodeService($request->getSession(), SessionName::CONTACT_FORM_SUBMISSION);

        $code = $this->validate($request->request->get('code'));

        if (!$formSecurity->isSecurityCodeCorrect($code)) {
            Logger::addLog('wrong security code has been provided. ' . $this->getIP(true), 'notice');
            $this->addFlash('warning', $this->translator->trans('Security code is not correct'));
            return $this->redirectToRoute(self::CONTACT_ROUTE_NAME);
        }

        $name = $this->validate($request->request->get('name'));
        $email = $this->validateEmail($request->request->get('email'));
        $subject = $this->validate($request->request->get('subject'));
        $message = $this->validateTextarea($request->request->get('message'));

        if (empty($name) || empty($email) || empty($subject) || empty($message)) {
            $this->addFlash('warning', $this->translator->trans('All fields are required'));
            return $this->redirectToRoute(self::CONTACT_ROUTE_NAME);
        }

        $isCopySent = $this->validateCheckbox($request->request->get('sendCopyToEmail'));
        
        $this->contactFormService->create($name, $email, $subject, $message, $isCopySent);

        if ($isCopySent) {
            $contactFormSendCopyToUser->send($name, $email, $subject, $message);
        }

        $formSecurity->cleanUp();

        $this->addFlash('success', $this->translator->trans('Thank you for contacting us. We will make a Response as soon as possible'));

        $this->notificationService->notifyAdmin('New Message', 'A new message has been sent from Contact Form');

        return $this->redirectToRoute(self::HOME_ROUTE);
    }

    #[Route('/new-security-code', name: 'app_contact_form_new_code')]
    public function securityCode(Session $session): Response
    {
        $formSecurityCode = new FormSecurityCodeService($session, SessionName::CONTACT_FORM_SUBMISSION);
       
        $numbers = $formSecurityCode->getSecurityCode();

        return new Response($numbers[0] . ';' . $numbers[1]);
    }
}
