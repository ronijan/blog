<?php

declare(strict_types=1);

namespace App\Controller;

use App\Mails\Account\NewSubscriberMail;
use App\Service\NewsletterService;
use App\Service\NotificationService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/newsletter')]
class NewsletterController extends AbstractBaseController
{
    use FormValidationTrait;

    public function __construct(
        private readonly NewsletterService $newsletterService,
        private readonly TranslatorInterface $translator,
        private readonly NotificationService $notificationService
    ) {}

    #[Route('/new-subscriber', name: 'app_newsletter_new', methods: 'post')]
    public function new(Request $request, NewSubscriberMail $newSubscriberMail): Response
    {
        $email = $this->validateEmail($request->request->get('email'));

        if (empty($email)) {
            $this->addFlash('warning', $this->translator->trans('Please type a valid email'));
            return $this->redirectToRoute(self::HOME_ROUTE);
        }

        if ($subscriber = $this->newsletterService->addNewSubscriber($email)) {
            $newSubscriberMail->send($subscriber->getName(), $subscriber->getEmail(), $subscriber->getToken());

            $this->addFlash(
                'notice',
                $this->translator->trans(
                    'An email with a link has been sent to your provided email. Please confirm your email'
                )
            );

            $this->notificationService->notifyAdmin(
                $this->translator->trans('New Subscriber'),
                $this->translator->trans('A new subscriber has been added to the Newsletter-List') . ' ' . $email
            );

            return $this->redirectToRoute(self::HOME_ROUTE);
        }

        if ($this->newsletterService->updateUserSubscription($email)) {
            $this->addFlash('success', $this->translator->trans('Your email has been added to our Newsletter List'));
        }

        return $this->redirectToRoute(self::HOME_ROUTE);
    }

    #[Route('/confirm/email/{token}', name: 'app_newsletter_confirm_email')]
    public function confirm(string $token): Response
    {
        $token = $this->validate($token);

        $subscriber = $this->newsletterService->getByToken($token);

        if (!$subscriber) {
            $this->addFlash('warning', $this->translator->trans('Token is invalid'));
            return $this->redirectToRoute(self::HOME_ROUTE);
        }

        $this->newsletterService->updateIsDeleted($subscriber, true);

        $this->addFlash('success', $this->translator->trans('Your email has been added to our Newsletter List'));

        return $this->redirectToRoute(self::HOME_ROUTE);
    }

    #[Route('/un-subscribe/{token}', name: 'app_newsletter_store', methods: ['get', 'post'])]
    public function store(string $token): Response
    {
        $token = $this->validate($token);

        $subscriber = $this->newsletterService->getByToken($token);

        if (!$subscriber) {
            $this->addFlash('warning', $this->translator->trans('Token is invalid'));
            return $this->redirectToRoute(self::HOME_ROUTE);
        }

        $this->newsletterService->unSubscribeUser($subscriber);
        
        $this->addFlash('success', $this->translator->trans('Your email has been removed from our Newsletter List'));

        return $this->redirectToRoute(self::HOME_ROUTE);
    }
}
