<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\AbstractBaseController;
use App\Helper\TestimonialHelper;
use App\Service\FileHandlerService;
use App\Service\FileUploaderService;
use App\Service\NotificationService;
use App\Service\TestimonialService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class TestimonialController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;

    private const HOME_ROUTE_INDEX = 'app_home';
    private const PAGE_NOT_FOUND_ROUTE_INDEX = 'app_home_status_code_404';

    public function __construct(
        private readonly TestimonialService $testimonialService,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route('/feedback/{token}', name: 'app_api_testimonial_index')]
    public function index(string $token): Response
    {
        $testimonial = $this->testimonialService->getByToken($token);

        if (!$testimonial) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE_INDEX);
        }

        return $this->render('api/add_new_testimonial.html.twig', [
            'token' => $token,
            'testimonial' => $testimonial,
        ]);
    }

    #[Route('/add', name: 'app_api_testimonial_store', methods: 'post')]
    public function store(
        Request $request,
        FileHandlerService $fileHandler,
        FileUploaderService $uploader,
        NotificationService $notificationService
    ): Response {
        $firstName = $this->validate($request->request->get('firstName'));
        $lastName = $this->validate($request->request->get('lastName'));
        $position = $this->validate($request->request->get('position'));
        $description = $this->validate($request->request->get('description'));
        $token = $this->validate($request->request->get('token'));
        $agreement = $this->validateCheckbox($request->request->get('agreement'));

        $testimonial = $this->testimonialService->getByToken($token);

        /** @var UploadedFile $file */
        $file = $request->files->get('avatar');
        $avatar = TestimonialHelper::DEFAULT_AVATAR;

        if ($file && $fileHandler->isImageExtensionAllowed($file->getClientOriginalExtension())) {

            $dir = $this->getParameter('testimonial_dir');

            $uploader->setTargetDirectory($dir);

            $avatar = $uploader->upload($file);
        }


        if ($firstName && $lastName && $position && $description && $agreement && $testimonial) {

            $token = TestimonialHelper::TOKEN_MUST_BE_RESETED_AFTER_CUSTOMER_SUBMIT;

            $this->testimonialService->update(
                $testimonial,
                $firstName,
                $lastName,
                $position,
                $description,
                $avatar,
                false,
                false,
                $testimonial->getEmail(),
                null
            );

            $notificationService->notifyAdmin('New Feedback', 'The customer has submitted the form - see Testimonials tab in dashboard.');

            $this->addFlash('success', $this->translator->trans('Thank you for adding a Feedback.'));

            return $this->redirectToRoute(self::HOME_ROUTE_INDEX);
        }

        $this->addFlash('warning', $this->translator->trans('All fields with (*) are required'));

        return $this->redirectToRoute(self::HOME_ROUTE_INDEX);
    }
}