<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AboutUsController extends AbstractBaseController
{
    #[Route('/about-us', name: 'app_about_us_index')]
    public function index(): Response
    {
        return $this->render('meta/about_us.html.twig');
    }
}
