<?php

declare(strict_types=1);

namespace App\Controller\Admin\Contact;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\ContactFormService;
use App\Service\NotificationService;
use App\Service\PaginationService;
use App\Service\UserService;
use App\Service\UserSettingService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/contacts')]
class ContactsController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const ADMIN_CONTACTS_INDEX_ROUTE = 'app_admin_contacts_index';
    private const PAGE_NOT_FOUND_ROUTE_INDEX = 'app_meta_page_not_found';
    private const SEARCH_ROUTE = 'app_admin_contacts_search_show';

    public function __construct(
        private readonly ProfileService $profileService,
        private readonly ContactFormService $contactFormService,
        private readonly UserSettingService $userSettingService,
        private readonly NotificationService $notificationService,
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route('/home/{page?}', name: 'app_admin_contacts_index')]
    public function index(PaginationService $paginationService, ?int $page): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $page = $this->validateNumber($page);

        if ($page < 0) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $limit = $this->userSettingService->getPaginationLimit($user);

        $offset = $paginationService->getOffset($page, $limit);

        $contacts = $this->contactFormService->getContactsWithOffsetAndLimit($offset, $limit);

        $pagination = $paginationService->paginate(
            $this->contactFormService->getContacts(),
            $page,
            $limit
        );

        return $this->render('admin/contact/contact_form.html.twig', [
            'contacts' => $contacts,
            'profile' => $profile,
            'notifications' => $notifications,
            'pagination' => $pagination,
            'paginationLimit' => $limit,
            'searchRoute' => self::SEARCH_ROUTE,
        ]);
    }

    #[Route('/edit/{id}', name: 'app_admin_contact_edit')]
    public function edit(string|int $id, Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $route = $request->getSession()->get('_security.main.target_path');

        $id = $this->validateNumber($id);

        if ($id <= 0) {
            $this->addFlash('warning', $this->translator->trans('Contact id is not defined'));
            return $this->redirect($route);
        }

        $contact = $this->contactFormService->getById($id);

        $user = $this->getUser();

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        return $this->render('admin/contact/edit_contact.html.twig', [
            'contact' => $contact,
            'profile' => $profile,
            'notifications' => $notifications,
        ]);
    }

    #[Route('/store', name: 'app_admin_contact_store', methods: 'post')]
    public function store(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $id = $this->validateNumber($request->request->get('contactId'));

        $contact = $this->contactFormService->getById($id);

        if ($id <= 0 || !$contact) {
            $this->addFlash('warning', $this->translator->trans('Contact id is not defined'));
            return $this->redirectToRoute(self::ADMIN_CONTACTS_INDEX_ROUTE);
        }

        $name = $this->validate($request->request->get('name'));
        $email = $this->validateEmail($request->request->get('email'));
        $subject = $this->validate($request->request->get('subject'));
        $message = $this->validateTextarea($request->request->get('message'));
        $ip = $this->validateTextarea($request->request->get('ip')) ?? '';
        $isCopySent = $this->validateCheckbox($request->request->get('copySent'));
        $isArchived = $this->validateCheckbox($request->request->get('archived'));
        $isDeleted = $this->validateCheckbox($request->request->get('deleted'));
        $updated = $this->validate($request->request->get('updated'));
        $created = $this->validate($request->request->get('created'));

        if ($isDeleted) {
            $isArchived = false;
        }

        if (empty($name) || empty($email) || empty($subject) || empty($message)) {
            $this->addFlash('warning', $this->translator->trans('Fields with star (*) are required'));
            return $this->redirectToRoute(self::ADMIN_CONTACTS_INDEX_ROUTE);
        }

        $this->contactFormService->update(
            $contact,
            $name,
            $email,
            $subject,
            $message,
            $ip,
            $isCopySent,
            $isArchived,
            $isDeleted,
            $updated,
            $created
        );

        $this->addFlash('success', $this->translator->trans('Changes has been saved'));

        return $this->redirectToRoute(self::ADMIN_CONTACTS_INDEX_ROUTE);
    }

    #[Route('/delete', name: 'app_admin_contact_delete', methods: 'post')]
    public function delete(Request $request, UserService $userService): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$userService->isPasswordValid($this->getUser(), $currentPassword)) {
            $this->addFlash('warning', $this->translator->trans('Please type your current password'));
            return $this->redirectToRoute(self::ADMIN_CONTACTS_INDEX_ROUTE);
        }

        $contactId = $this->validateNumber($request->request->get('contactId'));

        if ($contactId <= 0) {
            $this->addFlash('warning', $this->translator->trans('Contact can not be deleted. Contact id is not defined'));
            return $this->redirectToRoute(self::ADMIN_CONTACTS_INDEX_ROUTE);
        }

        $contact = $this->contactFormService->getById($contactId);

        if (null !== $contact) {
            $this->contactFormService->delete($contact);
            $this->addFlash('success', $this->translator->trans('Contact has been deleted'));

            return $this->redirectToRoute(self::ADMIN_CONTACTS_INDEX_ROUTE);
        }

        $this->addFlash('warning', $this->translator->trans('Unable to delete contact'));

        return $this->redirectToRoute(self::ADMIN_CONTACTS_INDEX_ROUTE);
    }
}