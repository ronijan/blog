<?php

declare(strict_types=1);

namespace App\Controller\Admin\Contact;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\ContactFormService;
use App\Service\NotificationService;
use App\Service\PaginationService;
use App\Service\UserSettingService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/contacts')]
class ArchiveController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const ADMIN_CONTACTS_INDEX_ROUTE = 'app_admin_contacts_index';
    private const PAGE_NOT_FOUND_ROUTE_INDEX = 'app_meta_page_not_found';
    private const SEARCH_ROUTE = 'app_admin_contacts_search_show';

    public function __construct(
        private readonly ProfileService $profileService,
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly UserSettingService $userSettingService,
        private readonly NotificationService $notificationService,
        private readonly ContactFormService $contactFormService
    ) {}

    #[Route('/archived-contacts/{page?}', name: 'app_admin_contacts_archived_index')]
    public function index(PaginationService $paginationService, ?int $page): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $page = $this->validateNumber($page);

        if ($page < 0) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE_INDEX);
        }
        
        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $limit = $this->userSettingService->getPaginationLimit($user);

        $offset = $paginationService->getOffset($page, $limit);

        $contacts = $this->contactFormService->getArchivedContactsWithOffsetAndLimit($offset, $limit);

        $pagination = $paginationService->paginate(
            $this->contactFormService->getArchivedContacts(),
            $page,
            $limit
        );

        return $this->render('admin/contact/archived_contacts.html.twig', [
            'contacts' => $contacts,
            'profile' => $profile,
            'notifications' => $notifications,
            'pagination' => $pagination,
            'paginationLimit' => $limit,
            'searchRoute' => self::SEARCH_ROUTE,
        ]);
    }

    #[Route('/archive', name: 'app_admin_contact_archive', methods: 'post')]
    public function store(Request $request, TranslatorInterface $translator): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $contactId = $this->validateNumber($request->request->get('contactId'));

        if ($contactId <= 0) {
            $this->addFlash('warning', $translator->trans('Contact can not be archived. Contact id is not defined'));
            return $this->redirectToRoute(self::ADMIN_CONTACTS_INDEX_ROUTE);
        }

        $contact = $this->contactFormService->getById($contactId);

        if (null !== $contact) {
            $this->contactFormService->updateIsArchived($contact);
            $this->addFlash('success', $translator->trans('Contact has been archived'));

            return $this->redirectToRoute(self::ADMIN_CONTACTS_INDEX_ROUTE);
        }

        $this->addFlash('warning', $translator->trans('Unable to archived contact'));

        return $this->redirectToRoute(self::ADMIN_CONTACTS_INDEX_ROUTE);
    }
}
