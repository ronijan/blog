<?php

declare(strict_types=1);

namespace App\Controller\Admin\Contact;

use App\Controller\Admin\AbstractBaseController;
use App\Helper\SearchableRoutes;
use App\Service\Account\ProfileService;
use App\Service\ContactFormService;
use App\Service\NotificationService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/contacts')]
class SearchController extends AbstractBaseController
{
    use FormValidationTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const CONTACTS_ROUTE = 'app_admin_contacts_index';

    public function __construct(
        private readonly ProfileService $profileService,
        private readonly NotificationService $notificationService,
        private readonly ContactFormService $contactFormService
    ) {}

    #[Route('/search', name: 'app_admin_contacts_search_show', methods: 'post')]
    public function show(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $keyword = $this->validate($request->request->get('keyword'));

        $route = $this->validate($request->request->get('route'));

        if (!in_array($route, SearchableRoutes::ADMIN_CONTACTS, true)) {
            $route = self::CONTACTS_ROUTE;
        }

        if (empty($keyword) || strlen($keyword) > 100) {
            return $this->redirectToRoute($route);
        }

        $user = $this->getUser();
        
        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $contacts = $this->contactFormService->search($keyword);

        return $this->render('admin/contact/search.html.twig', [
            'contacts' => $contacts,
            'profile' => $profile,
            'notifications' => $notifications,
            'route' => $route,
            'keyword' => $keyword,
        ]);
    }
}
