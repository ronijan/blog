<?php

declare(strict_types=1);

namespace App\Controller\Admin\Contact;

use App\Controller\Admin\AbstractBaseController;
use App\Service\ContactFormService;
use App\Service\Admin\Job\DeleteContactsMarkedAsDeletedPermanentlyJob;
use App\Service\Admin\Job\DeleteOldContactsBasedOnModifierJob;
use App\Service\Admin\Job\RemoveIpAddressFromMessagesJob;
use App\Service\Admin\Job\TransformOldContactsJob;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/contacts/job')]
class JobController extends AbstractBaseController
{
    use FormValidationTrait;

    private const ADMIN_CONTACTS_INDEX_ROUTE = 'app_admin_contacts_index';
    private const ADMIN_DELETED_CONTACTS_ROUTE_INDEX = 'app_admin_contacts_deleted_contacts_index';

    public function __construct(
        private readonly ContactFormService $contactFormService,
        private readonly TranslatorInterface $translator,
        private readonly UserService $userService
    ) {
    }

    #[Route('/delete-old-contacts-permanently', name: 'app_admin_contacts_delete_permanently', methods: 'post')]
    public function delete(
        Request $request,
        DeleteContactsMarkedAsDeletedPermanentlyJob $deleteContactsMarkedAsDeletedPermanentlyJob,
        DeleteOldContactsBasedOnModifierJob $deleteOldContactsBasedOnModifierJob
    ): Response {
        $this->denyAccessUnlessGrantedRoleAdmin();

        if ($this->validateCheckbox($request->request->get('deleteAll'))) {
            $countDeletedMessage = $deleteContactsMarkedAsDeletedPermanentlyJob->start();
            $this->addFlash(
                'success', $this->translator->trans('No Message has been left') .
                ' [' . $countDeletedMessage . ']'
            );

            return $this->redirectToRoute(self::ADMIN_CONTACTS_INDEX_ROUTE);
        }

        if ($this->validateCheckbox($request->request->get('deleteOlder'))) {
            if ($countDeletedContacts = $deleteOldContactsBasedOnModifierJob->start()) {
                $this->addFlash(
                    'notice', $this->translator->trans('Job has been executed') .
                    ' [' . $countDeletedContacts . ']'
                );

                return $this->redirectToRoute(self::ADMIN_DELETED_CONTACTS_ROUTE_INDEX);
            }
        }

        $this->addFlash('notice', $this->translator->trans('Job could not be executed'));

        return $this->redirectToRoute(self::ADMIN_DELETED_CONTACTS_ROUTE_INDEX);
    }

    #[Route('/archive-all', name: 'app_admin_contacts_job_archive_all', methods: 'post')]
    public function archiveAll(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {

            $this->addFlash('warning', $this->translator->trans('Please type your current password'));

            return $this->redirectToRoute(self::ADMIN_CONTACTS_INDEX_ROUTE);
        }

        $count = 0;

        foreach ($this->contactFormService->getUnArchivedContacts() as $messge) {

            $this->contactFormService->updateIsArchived($messge);

            $count++;
        }

        $this->addFlash(
            'success',
            $this->translator->trans(
                sprintf('All messages (but not deleted) has been archived [%s]', $count)
            )
        );

        return $this->redirectToRoute(self::ADMIN_CONTACTS_INDEX_ROUTE);
    }

    #[Route('/delete-all', name: 'app_admin_contacts_job_delete_all', methods: 'post')]
    public function deleteAll(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {

            $this->addFlash('warning', $this->translator->trans('Please type your current password'));

            return $this->redirectToRoute(self::ADMIN_CONTACTS_INDEX_ROUTE);
        }

        $count = 0;

        foreach ($this->contactFormService->getDeletedContacts() as $messge) {

            $this->contactFormService->delete($messge);

            $count++;
        }

        $this->addFlash(
            'success',
            $this->translator->trans(
                sprintf('All messages (is_deleted=1) has been deleted [%s]', $count)
            )
        );

        return $this->redirectToRoute(self::ADMIN_DELETED_CONTACTS_ROUTE_INDEX);
    }

    #[Route('/transform-data', name: 'app_admin_contacts_transform', methods: 'post')]
    public function transform(Request $request, TransformOldContactsJob $transformOldContactsJob)
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {
            $this->addFlash('warning', $this->translator->trans('Please type your current password'));
            return $this->redirectToRoute(self::ADMIN_DELETED_CONTACTS_ROUTE_INDEX);
        }

        if ($this->validateCheckbox($request->request->get('transformDeleted'))) {
            $countTransformedOldContacts = $transformOldContactsJob->start();

            if ($countTransformedOldContacts > 0) {
                $this->addFlash(
                    'success', $this->translator->trans('Data has been transformed') .
                    ' [' . $countTransformedOldContacts . ']'
                );

                return $this->redirectToRoute(self::ADMIN_DELETED_CONTACTS_ROUTE_INDEX);
            }
        }

        $this->addFlash('notice', $this->translator->trans('No data has been transformed'));

        return $this->redirectToRoute(self::ADMIN_DELETED_CONTACTS_ROUTE_INDEX);
    }

    #[Route('/clear-ip-addresses', name: 'app_admin_contacts_clear_its_ip_addresses', methods: 'post')]
    public function clearIPs(Request $request, RemoveIpAddressFromMessagesJob $removeIpAddressFromMessagesJob): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        if ($count = $removeIpAddressFromMessagesJob->start()) {
            $this->addFlash(
                'success',
                $this->translator->trans('All IPs from contacts has been cleared') . ' [' . $count . ']'
            );

            return $this->redirectToRoute(self::ADMIN_CONTACTS_INDEX_ROUTE);
        }

        $this->addFlash('notice', $this->translator->trans('No data has been found'));

        return $this->redirectToRoute(self::ADMIN_CONTACTS_INDEX_ROUTE);
    }
}