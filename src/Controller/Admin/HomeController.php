<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\NotificationService;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin')]
class HomeController extends AbstractBaseController
{
    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';

    public function __construct(
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly UserService $userService,
        private readonly ProfileService $profileService,
        private readonly NotificationService $notificationService
    ) {}

    #[Route('/dashboard', name: 'app_admin_index')]
    public function index(): Response
    {
        $this->denyAccessUnlessGrantedRoleEditor();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $profile = $this->profileService->getByUser($user);

        $activeUsers = $this->userService->getActiveUsers();

        $disabledAccount = $this->userService->getDisabledAccounts();

        $deletedAccount = $this->userService->getDeletedAccounts();

        $notifications = $this->notificationService->getUnseenNotifications();

        return $this->render('admin/index.html.twig', [
            'profile' => $profile,
            'activeUsers' => count($activeUsers),
            'deletedAccount' => count($deletedAccount),
            'disabledAccounts' => count($disabledAccount),
            'notifications' => $notifications,
        ]);
    }
}
