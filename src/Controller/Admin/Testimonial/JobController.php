<?php

declare(strict_types=1);

namespace App\Controller\Admin\Testimonial;

use App\Controller\Admin\AbstractBaseController;
use App\Helper\TestimonialHelper;
use App\Service\FileHandlerService;
use App\Service\FileUploaderService;
use App\Service\TestimonialService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/testimonial')]
class JobController extends AbstractBaseController
{
    use FormValidationTrait;

    private const ADMIN_TESTIMONIAL_ROUTE_INDEX = 'app_admin_testimonial_index';

    public function __construct(
        private readonly TestimonialService $testimonialService,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route('/store-image', name: 'app_admin_testimonial_upload_image_store', methods: 'post')]
    public function edit(Request $request, FileUploaderService $uploader, FileHandlerService $fileHandler): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $id = $this->validateNumber($request->request->get('id'));

        $isRealImage = $this->validateCheckbox($request->request->get('isRealImage'));

        /** @var UploadedFile $file */
        $file = $request->files->get('avatar');

        $avatar = TestimonialHelper::DEFAULT_AVATAR;

        $dir = $this->getParameter('testimonial_dir');

        $testimonial = $this->testimonialService->getById($id);

        if (!$testimonial) {

            $this->addFlash(
                'warning',
                $this->translator->trans(
                    sprintf('Testimonial with id [%s] could not be found', $id)
                )
            );

            return $this->redirectToRoute(self::ADMIN_TESTIMONIAL_ROUTE_INDEX);
        }


        if ($testimonial->getAvatar() !== $avatar) {

            $fileHandler->unlinkFile($dir, $testimonial->getAvatar());
        }

        if ($file && $fileHandler->isImageExtensionAllowed($file->getClientOriginalExtension())) {

            $uploader->setTargetDirectory($dir);

            $avatar = $uploader->upload($file);
        }

        if ($isRealImage && $testimonial->getId() === $id) {

            $this->testimonialService->updateImage($testimonial, $avatar);

            $this->addFlash('success', $this->translator->trans('Image has been updated'));

            return $this->redirectToRoute(self::ADMIN_TESTIMONIAL_ROUTE_INDEX);
        }

        $this->addFlash('warning', $this->translator->trans('All fields are required'));

        return $this->redirectToRoute(self::ADMIN_TESTIMONIAL_ROUTE_INDEX);
    }
}