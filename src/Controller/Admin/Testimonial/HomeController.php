<?php

declare(strict_types=1);

namespace App\Controller\Admin\Testimonial;

use App\Controller\Admin\AbstractBaseController;
use App\Helper\TestimonialHelper;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\FileHandlerService;
use App\Service\FileUploaderService;
use App\Service\NotificationService;
use App\Service\TestimonialService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/testimonial')]
class HomeController extends AbstractBaseController
{
    use FormValidationTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const ADMIN_TESTIMONIAL_ROUTE_INDEX = 'app_admin_testimonial_index';
    private const PAGE_NOT_FOUND_ROUTE_INDEX = 'app_home_status_code_404';
    private const SEARCH_ROUTE = 'app_admin_testimonial_search_show';

    public function __construct(
        private readonly UserService $userService,
        private readonly ProfileService $profileService,
        private readonly TestimonialService $testimonialService,
        private readonly NotificationService $notificationService,
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly TranslatorInterface $translator,
        private readonly FileHandlerService $fileHandler,
    ) {
    }

    #[Route('/home/{page?}', name: 'app_admin_testimonial_index')]
    public function index(): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {

            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $this->notificationService->updateIsSeen($notifications);

        $testimonials = $this->testimonialService->getAll();

        return $this->render('admin/testimonial/index.html.twig', [
            'profile' => $profile,
            'testimonials' => $testimonials,
            'notifications' => $notifications,
        ]);
    }

    #[Route('/view/{id}', name: 'app_admin_testimonial_show')]
    public function show(string $id): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {

            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $testimonial = $this->testimonialService->getById(
            $this->validateNumber($id)
        );

        if ($testimonial === null) {

            $this->addFlash('warning', $this->translator->trans('Testimonial cannot be found'));

            return $this->redirectToRoute(self::ADMIN_TESTIMONIAL_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        return $this->render('admin/testimonial/edit.html.twig', [
            'profile' => $profile,
            'testimonial' => $testimonial,
            'notifications' => $notifications,
        ]);
    }

    #[Route('/add', name: 'app_admin_testimonial_new', methods: 'post')]
    public function new(Request $request, FileUploaderService $uploader): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $firstName = $this->validate($request->request->get('firstName'));
        $lastName = $this->validate($request->request->get('lastName'));
        $position = $this->validate($request->request->get('position'));
        $description = $this->validateTextarea($request->request->get('description'));
        $isPublished = $this->validateCheckbox($request->request->get('isPublished'));

        /** @var UploadedFile $file */
        $file = $request->files->get('avatar');
        $avatar = TestimonialHelper::DEFAULT_AVATAR;

        if ($file && $this->fileHandler->isImageExtensionAllowed($file->getClientOriginalExtension())) {

            $dir = $this->getParameter('testimonial_dir');

            $uploader->setTargetDirectory($dir);

            $avatar = $uploader->upload($file);
        }

        if ($firstName && $lastName && $position && $description) {

            $this->testimonialService->create($firstName, $lastName, $position, $description, $avatar, '', '', $isPublished);

            $this->addFlash('success', $this->translator->trans('Testimonial has been added'));

            return $this->redirectToRoute(self::ADMIN_TESTIMONIAL_ROUTE_INDEX);
        }

        $this->addFlash('warning', $this->translator->trans('All fields with (*) are required'));

        return $this->redirectToRoute(self::ADMIN_TESTIMONIAL_ROUTE_INDEX);
    }

    #[Route('/store', name: 'app_admin_testimonial_store', methods: 'post')]
    public function edit(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $firstName = $this->validate($request->request->get('firstName'));
        $lastName = $this->validate($request->request->get('lastName'));
        $email = $this->validate($request->request->get('email'));
        $position = $this->validate($request->request->get('position'));
        $description = $this->validateTextarea($request->request->get('description'));
        $isDeleted = $this->validateCheckbox($request->request->get('isDeleted'));
        $token = $this->validate($request->request->get('token'));
        $isPublished = $this->validateCheckbox($request->request->get('isPublished')) && $isDeleted !== true;
        $avatar = $this->validate($request->request->get('avatar'));

        $testimonial = $this->testimonialService->getById(
            $this->validateNumber($request->request->get('id'))
        );

        if ($firstName && $lastName && $position && $description && $testimonial) {

            $defaultAvatar = TestimonialHelper::DEFAULT_AVATAR;

            if ($this->validateCheckbox($request->request->get('removeAvatar')) && $testimonial->getAvatar() !== $defaultAvatar) {

                $dir = $this->getParameter('testimonial_dir');

                $this->fileHandler->unlinkFile($dir, $testimonial->getAvatar());

                $avatar = $defaultAvatar;
            }

            $this->testimonialService->update(
                $testimonial,
                $firstName,
                $lastName,
                $position,
                $description,
                $avatar,
                $isPublished,
                $isDeleted,
                $email,
                $token
            );

            $this->addFlash('success', $this->translator->trans('Testimonial has been updated'));

            return $this->redirectToRoute(self::ADMIN_TESTIMONIAL_ROUTE_INDEX);
        }

        $this->addFlash('warning', $this->translator->trans('All fields with (*) are required'));

        return $this->redirectToRoute(self::ADMIN_TESTIMONIAL_ROUTE_INDEX);
    }

    #[Route('/delete', name: 'app_admin_testimonial_delete', methods: 'post')]
    public function delete(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {

            $this->addFlash('warning', $this->translator->trans('Please type your current password'));

            return $this->redirectToRoute(self::ADMIN_TESTIMONIAL_ROUTE_INDEX);
        }

        $count = 0;

        $dir = $this->getParameter('testimonial_dir');

        foreach ($this->testimonialService->getDeletedOrNotPublished() as $testimonial) {

            $this->testimonialService->delete($testimonial);

            if (TestimonialHelper::DEFAULT_AVATAR !== $testimonial->getAvatar()) {
                $this->fileHandler->unlinkFile($dir, $testimonial->getAvatar());
            }

            $count++;
        }

        $count > 0
            ? $this->addFlash(
                'success',
                $this->translator->trans(
                    sprintf('Testimonial has been deleted [%s]', $count)
                )
            )
            : $this->addFlash('notice', $this->translator->trans('Nothing has been deleted'));

        return $this->redirectToRoute(self::ADMIN_TESTIMONIAL_ROUTE_INDEX);
    }
}