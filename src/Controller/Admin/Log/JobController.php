<?php

declare(strict_types=1);

namespace App\Controller\Admin\Log;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Admin\Job\RemoveIpAddressesFromArchivedLogsJob;
use App\Service\SystemLogsService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/system-logs')]
class JobController extends AbstractBaseController
{
    use FormValidationTrait;

    private const ADMIN_SYSTEM_LOGS_ARCHIVED_ROUTE_INDEX = 'app_admin_system_logs_archive_index';

    private const ADMIN_SYSTEM_LOGS_ROUTE_INDEX = 'app_admin_log_index';

    public function __construct(
        private readonly UserService $userService,
        private readonly SystemLogsService $systemLogsService,
        private readonly RemoveIpAddressesFromArchivedLogsJob $removeIpAddressesFromArchivedLogsJob,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route('/archive/remove-ips-from-arrchived-logs', name: 'app_admin_system_logs_archive_remove_ips', methods: 'post')]
    public function store(Request $request)
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        if ($countRemovedIps = $this->removeIpAddressesFromArchivedLogsJob->start()) {

            $this->addFlash(
                'success',
                $this->translator->trans(
                    sprintf('IPs has been removed from archived Logs [%s]', $countRemovedIps)
                )
            );

            return $this->redirectToRoute(self::ADMIN_SYSTEM_LOGS_ARCHIVED_ROUTE_INDEX);
        }

        $this->addFlash('notice', $this->translator->trans('No Data has been found'));

        return $this->redirectToRoute(self::ADMIN_SYSTEM_LOGS_ARCHIVED_ROUTE_INDEX);
    }

    #[Route('/delete-all-logs', name: 'app_admin_system_logs_job_delete', methods: 'post')]
    public function delete(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {

            $this->addFlash('warning', $this->translator->trans('Please type your current password'));

            return $this->redirectToRoute(self::ADMIN_SYSTEM_LOGS_ROUTE_INDEX);
        }

        $count = 0;

        foreach ($this->systemLogsService->getAllButNotArchived() as $systemLog) {

            $this->systemLogsService->delete($systemLog);

            $count++;
        }

        $this->addFlash(
            'success',
            $this->translator->trans(
                sprintf('System-Logs has been deleted [%s]', $count)
            )
        );

        return $this->redirectToRoute(self::ADMIN_SYSTEM_LOGS_ROUTE_INDEX);
    }

    #[Route('/archived/delete-all', name: 'app_admin_system_logs_job_delete_all_archived', methods: 'post')]
    public function deleteAllArchived(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {

            $this->addFlash('warning', $this->translator->trans('Please type your current password'));

            return $this->redirectToRoute(self::ADMIN_SYSTEM_LOGS_ARCHIVED_ROUTE_INDEX);
        }

        $count = 0;

        foreach ($this->systemLogsService->getAllArchived() as $systemLog) {

            $this->systemLogsService->delete($systemLog);

            $count++;
        }

        $this->addFlash(
            'success',
            $this->translator->trans(
                sprintf('Archived System-Logs has been deleted [%s]', $count)
            )
        );

        return $this->redirectToRoute(self::ADMIN_SYSTEM_LOGS_ARCHIVED_ROUTE_INDEX);
    }

    #[Route('/archive-all', name: 'app_admin_system_logs_job_archive_all', methods: 'post')]
    public function archiveAll(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $count = 0;

        foreach ($this->systemLogsService->getAllButNotArchived() as $systemLog) {

            $this->systemLogsService->updateIsArchived($systemLog->getId(), true);

            $count++;
        }

        $this->addFlash(
            'success',
            $this->translator->trans(
                sprintf('System-Logs has been archived [%s]', $count)
            )
        );

        return $this->redirectToRoute(self::ADMIN_SYSTEM_LOGS_ROUTE_INDEX);
    }
}