<?php

declare(strict_types=1);

namespace App\Controller\Admin\Log;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Admin\Export\ExportSystemLogsService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/system-logs')]
class ExportController extends AbstractBaseController
{
    use FormValidationTrait;

    private const ADMIN_SYSTEM_LOGS_ARCHIVED_ROUTE_INDEX = 'app_admin_system_logs_archive_index';

    public function __construct(
        private readonly UserService $userService,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route('/export/archived', name: 'app_admin_system_logs_job_export_archived', methods: 'post')]
    public function exportArchived(Request $request, ExportSystemLogsService $exportSystemLogsService): RedirectResponse|Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {

            $this->addFlash('warning', $this->translator->trans('Please type your current password'));

            return $this->redirectToRoute(self::ADMIN_SYSTEM_LOGS_ARCHIVED_ROUTE_INDEX);
        }

        return new Response($exportSystemLogsService->asJson());
    }
}