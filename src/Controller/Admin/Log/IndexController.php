<?php

declare(strict_types=1);

namespace App\Controller\Admin\Log;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\NotificationService;
use App\Service\PaginationService;
use App\Service\SystemLogsService;
use App\Service\UserSettingService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/system-logs')]
class IndexController extends AbstractBaseController
{
    use FormValidationTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const PAGE_NOT_FOUND_ROUTE_INDEX = 'app_meta_page_not_found';
    private const ADMIN_SYSTEM_LOGS_ROUTE_INDEX = 'app_admin_log_index';

    public function __construct(
        private readonly ProfileService $profileService, 
        private readonly NotificationService $notificationService,
        private readonly SystemLogsService $systemLogsService,
        private readonly UserSettingService $userSettingService,
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly TranslatorInterface $translator
    ) {}

    #[Route('/home/{page?}', name: 'app_admin_log_index')]
    public function index(PaginationService $paginationService, ?int $page ): Response
     {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $page = $this->validateNumber($page);

        if ($page < 0) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $limit = $this->userSettingService->getPaginationLimit($user);

        $offset = $paginationService->getOffset($page, $limit);

        $systemLogs = $this->systemLogsService->getAllButNotArchivedByOffsetAndLimit($offset, $limit);

        $pagination = $paginationService->paginate(
            $this->systemLogsService->getAllButNotArchived(),
            $page,
            $limit
        );

        return $this->render('admin/log/index.html.twig', [
            'systemLogs' => $systemLogs,
            'profile' => $profile,
            'notifications' => $notifications,
            'pagination' => $pagination,
            'paginationLimit' => $limit,
        ]);
    }

    #[Route('/view/{id}', name: 'app_admin_system_logs_show')]
    public function show(string $id): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $logId = $this->validateNumber($id);

        if ($logId <= 0) {
            $this->addFlash('warning', $this->translator->trans('Undefined ID'));
            return $this->redirectToRoute(self::ADMIN_SYSTEM_LOGS_ROUTE_INDEX);
        }

        $systemLog = $this->systemLogsService->getById($logId);

        if ($systemLog === null) {
            $this->addFlash('warning', $this->translator->trans('Data could not be found'));
            return $this->redirectToRoute(self::ADMIN_SYSTEM_LOGS_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        return $this->render('admin/log/view.html.twig', [
            'systemLog' => $systemLog,
            'profile' => $profile,
            'notifications' => $notifications,
        ]);
    }
}
