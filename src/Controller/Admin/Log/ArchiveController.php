<?php

declare(strict_types=1);

namespace App\Controller\Admin\Log;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\NotificationService;
use App\Service\PaginationService;
use App\Service\SystemLogsService;
use App\Service\UserSettingService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/system-logs/archive')]
class ArchiveController extends AbstractBaseController
{
    use FormValidationTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const PAGE_NOT_FOUND_ROUTE_INDEX = 'app_meta_page_not_found';

    public function __construct(
        private readonly ProfileService $profileService,
        private readonly NotificationService $notificationService,
        private readonly SystemLogsService $systemLogsService,
        private readonly UserSettingService $userSettingService,
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route('/home/{page?}', name: 'app_admin_system_logs_archive_index')]
    public function index(PaginationService $paginationService, ?int $page): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $page = $this->validateNumber($page);

        if ($page < 0) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $limit = $this->userSettingService->getPaginationLimit($user);

        $offset = $paginationService->getOffset($page, $limit);

        $archivedSystemLogs = $this->systemLogsService->getAllArchivedByOffsetAndLimit($offset, $limit);

        $pagination = $paginationService->paginate(
            $this->systemLogsService->getAllArchived(),
            $page,
            $limit
        );

        return $this->render('admin/log/archive.html.twig', [
            'systemLogs' => $archivedSystemLogs,
            'profile' => $profile,
            'notifications' => $notifications,
            'pagination' => $pagination,
            'paginationLimit' => $limit,
        ]);
    }

    #[Route('/store', name: 'app_admin_system_logs_archive_store', methods: 'post')]
    public function store(Request $request)
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $logId = $this->validateNumber($request->request->get('logId'));
        $route = $this->validate($request->request->get('route'));

        if ($logId > 0) {
            $this->systemLogsService->updateIsArchived($logId, true);
            $this->addFlash('success', $this->translator->trans('Changes has been saved'));
            return $this->redirectToRoute($route);
        }

        $this->addFlash('warning', $this->translator->trans('Data could not be found'));

        return $this->redirectToRoute($route);
    }

    #[Route('/delete', name: 'app_admin_system_logs_archive_delete', methods: 'post')]
    public function delete(Request $request)
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $logId = $this->validateNumber($request->request->get('id'));
        $route = $this->validate($request->request->get('route'));

        $systemLog = $this->systemLogsService->getById($logId);

        if ($systemLog) {
            $this->systemLogsService->delete($systemLog);
            $this->addFlash('success', $this->translator->trans('Log has been deleted'));
            return $this->redirectToRoute($route);
        }

        $this->addFlash('warning', $this->translator->trans('Data could not be found'));

        return $this->redirectToRoute($route);
    }
}