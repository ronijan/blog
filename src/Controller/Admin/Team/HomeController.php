<?php

declare(strict_types=1);

namespace App\Controller\Admin\Team;

use App\Controller\Admin\AbstractBaseController;
use App\Helper\TeamHelper;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\FileHandlerService;
use App\Service\FileUploaderService;
use App\Service\NotificationService;
use App\Service\TeamService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/teams')]
class HomeController extends AbstractBaseController
{
    use FormValidationTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const ADMIN_TEAMS_ROUTE_INDEX = 'app_admin_teams_index';

    public function __construct(
        private readonly TeamService $teamService,
        private readonly UserService $userService,
        private readonly ProfileService $profileService,
        private readonly NotificationService $notificationService,
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly TranslatorInterface $translator,
        private readonly FileHandlerService $fileHandler,
    ) {
    }

    #[Route('/home/{page?}', name: 'app_admin_teams_index')]
    public function index(): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {

            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $teams = $this->teamService->getAll();

        return $this->render('admin/team/index.html.twig', [
            'profile' => $profile,
            'teams' => $teams,
            'notifications' => $notifications,
        ]);
    }

    #[Route('/member/view/{id}', name: 'app_admin_team_show')]
    public function show(string $id): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {

            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $team = $this->teamService->getById($this->validateNumber($id));

        if (!$team) {

            $this->addFlash('warning', $this->translator->trans('Team member cannot be found'));

            return $this->redirectToRoute(self::ADMIN_TEAMS_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        return $this->render('admin/team/edit.html.twig', [
            'profile' => $profile,
            'team' => $team,
            'notifications' => $notifications,
        ]);
    }

    #[Route('/member/add', name: 'app_admin_team_new', methods: 'post')]
    public function new(Request $request, FileUploaderService $uploader): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $firstName = $this->validate($request->request->get('firstName'));
        $lastName = $this->validate($request->request->get('lastName'));
        $position = $this->validate($request->request->get('position'));
        $email = $this->validateEmail($request->request->get('email'));
        $phone = $this->validate($request->request->get('phone'));
        $isActive = $this->validateCheckbox($request->request->get('isActive'));

        /** @var UploadedFile $file */
        $file = $request->files->get('avatar');

        $avatar = TeamHelper::DEFAULT_AVATAR;

        if ($file && $this->fileHandler->isImageExtensionAllowed($file->getClientOriginalExtension())) {

            $dir = $this->getParameter('teams_dir');

            $uploader->setTargetDirectory($dir);

            $avatar = $uploader->upload($file);
        }

        if ($firstName && $lastName && $position) {

            $this->teamService->createOrUpdate(null, $firstName, $lastName, $position, $isActive, $avatar, $email, $phone);

            $this->addFlash('success', $this->translator->trans('Team member has been added'));

            return $this->redirectToRoute(self::ADMIN_TEAMS_ROUTE_INDEX);
        }

        $this->addFlash('warning', $this->translator->trans('All fields with (*) are required'));

        return $this->redirectToRoute(self::ADMIN_TEAMS_ROUTE_INDEX);
    }

    #[Route('/member/store', name: 'app_admin_team_store', methods: 'post')]
    public function edit(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $id = $this->validateNumber($request->request->get('id'));

        $member = $this->teamService->getById($id);

        if ($this->validateCheckbox($request->request->get('isDeleted')) && $member) {

            $this->teamService->delete($member);

            $this->addFlash('success', $this->translator->trans('Team member has been deleted'));

            return $this->redirectToRoute(self::ADMIN_TEAMS_ROUTE_INDEX);
        }

        $firstName = $this->validate($request->request->get('firstName'));
        $lastName = $this->validate($request->request->get('lastName'));
        $position = $this->validate($request->request->get('position'));
        $avatar = $this->validate($request->request->get('avatar'));
        $email = $this->validate($request->request->get('email'));
        $phone = $this->validate($request->request->get('phone'));
        $isActive = $this->validateCheckbox($request->request->get('isActive'));

        if ($firstName && $lastName && $position) {

            $defaultAvatar = TeamHelper::DEFAULT_AVATAR;

            if ($this->validateCheckbox($request->request->get('removeAvatar')) && $member->getImage() !== $defaultAvatar) {

                $dir = $this->getParameter('teams_dir');

                $this->fileHandler->unlinkFile($dir, $member->getImage());

                $avatar = $defaultAvatar;
            }

            $this->teamService->createOrUpdate($member, $firstName, $lastName, $position, $isActive, $avatar, $email, $phone);

            $this->addFlash('success', $this->translator->trans('Team member details has been updated'));

            return $this->redirectToRoute(self::ADMIN_TEAMS_ROUTE_INDEX);
        }

        $this->addFlash('warning', $this->translator->trans('Fields with (*) are required'));

        return $this->redirectToRoute(self::ADMIN_TEAMS_ROUTE_INDEX);
    }
}