<?php

declare(strict_types=1);

namespace App\Controller\Admin\Team;

use App\Controller\Admin\AbstractBaseController;
use App\Helper\TeamHelper;
use App\Service\FileHandlerService;
use App\Service\FileUploaderService;
use App\Service\TeamService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/teamservice')]
class JobController extends AbstractBaseController
{
    use FormValidationTrait;

    private const ADMIN_TEAMS_ROUTE_INDEX = 'app_admin_teams_index';

    public function __construct(
        private readonly TeamService $teamService,
        private readonly UserService $userService,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route('/deactivate-members', name: 'app_admin_team_job_deactivate', methods: 'post')]
    public function deactivate(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {

            $this->addFlash('warning', $this->translator->trans('Please type your current password'));

            return $this->redirectToRoute(self::ADMIN_TEAMS_ROUTE_INDEX);
        }

        $members = $this->teamService->getAll();

        $count = 0;

        foreach ($members as $member) {
            
            if ($member->isIsActive()) {

                $this->teamService->updateIsActive($member, false);

                $count++;
            }
        }

        $count > 0
            ? $this->addFlash(
                'success',
                $this->translator->trans(
                    sprintf('The members has been deactivated [%s]', $count)
                )
            )
            : $this->addFlash('notice', $this->translator->trans('No changes has been made.'));

        return $this->redirectToRoute(self::ADMIN_TEAMS_ROUTE_INDEX);
    }

    #[Route('/store-image', name: 'app_admin_team_job_upload_image_store', methods: 'post')]
    public function edit(Request $request, FileUploaderService $uploader, FileHandlerService $fileHandler): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $id = $this->validateNumber($request->request->get('id'));

        $teammember = $this->teamService->getById($id);

        if (!$teammember) {

            $this->addFlash(
                'warning',
                $this->translator->trans(
                    sprintf('Team member with id [%s] could not be found', $id)
                )
            );

            return $this->redirectToRoute(self::ADMIN_TEAMS_ROUTE_INDEX);
        }

        if ($this->validateCheckbox($request->request->get('isRealImage')) && $teammember->getId() === $id) {
            $avatar = TeamHelper::DEFAULT_AVATAR;

            $dir = $this->getParameter('teams_dir');

            if ($teammember->getImage() !== $avatar) {

                $fileHandler->unlinkFile($dir, $teammember->getImage());
            }

            /** @var UploadedFile $file */
            $file = $request->files->get('avatar');

            if ($file && $fileHandler->isImageExtensionAllowed($file->getClientOriginalExtension())) {

                $uploader->setTargetDirectory($dir);

                $avatar = $uploader->upload($file);
            }

            $this->teamService->updateImage($teammember, $avatar);

            $this->addFlash('success', $this->translator->trans('Image has been updated'));

            return $this->redirectToRoute(self::ADMIN_TEAMS_ROUTE_INDEX);
        }

        $this->addFlash('warning', $this->translator->trans('All fields are required'));

        return $this->redirectToRoute(self::ADMIN_TEAMS_ROUTE_INDEX);
    }
}