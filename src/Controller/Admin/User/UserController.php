<?php

declare(strict_types=1);

namespace App\Controller\Admin\User;

use App\Controller\Admin\AbstractBaseController;
use App\Helper\DataTransformer;
use App\Helper\UserHelper;
use App\Mails\Account\AccountDeletedMail;
use App\Mails\Admin\NewUserViaAdminMail;
use App\Mails\Blog\WelcomeWriterMail;
use App\Service\Account\DeleteUserPermanentlyService;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\Account\RegisterNewUserService;
use App\Service\Blog\AutherService;
use App\Service\NotificationService;
use App\Service\PaginationService;
use App\Service\SystemLogsService;
use App\Service\UserService;
use App\Service\UserSettingService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/users')]
class UserController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const ADMIN_USERS_INDEX_ROUTE = 'app_admin_user_index';
    private const PAGE_NOT_FOUND_ROUTE_INDEX = 'app_meta_page_not_found';
    private const SEARCH_ROUTE = 'app_admin_user_search_show';

    public function __construct(
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly UserService $userService,
        private readonly ProfileService $profileService,
        private readonly NotificationService $notificationService,
        private readonly TranslatorInterface $translator,
        private readonly AutherService $autherService,
        private readonly SystemLogsService $systemLogsService
    ) {
    }

    #[Route('/active-users/{page?}', name: 'app_admin_user_index')]
    public function index(
        UserSettingService $userSettingService,
        PaginationService $paginationService,
        ?int $page
    ): Response {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $page = $this->validateNumber($page);

        if ($page < 0) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $limit = $userSettingService->getPaginationLimit($user);

        $offset = $paginationService->getOffset($page, $limit);

        $users = $this->userService->getActiveUsersWithOffsetAndLimit($offset, $limit);

        $pagination = $paginationService->paginate(
            $this->userService->getActiveUsers(),
            $page,
            $limit
        );

        return $this->render('admin/user/users.html.twig', [
            'users' => $users,
            'profile' => $profile,
            'notifications' => $notifications,
            'pagination' => $pagination,
            'paginationLimit' => $limit,
            'searchRoute' => self::SEARCH_ROUTE,
        ]);
    }

    #[Route('/add-new-user', name: 'app_admin_users_new', methods: 'post')]
    public function new(
        Request $request,
        NewUserViaAdminMail $newUserViaAdminMail,
        RegisterNewUserService $registerNewUserService
    ): Response {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $username = $this->validateUsernameAndReplaceSpace($request->request->get('username'));
        $usernameErrorMessage = $this->usernameError($username);

        if ($usernameErrorMessage) {
            $this->addFlash('warning', $usernameErrorMessage);
            return $this->redirectToRoute(self::ADMIN_USERS_INDEX_ROUTE);
        }

        $email = $this->validateEmail($request->request->get('email'));

        if (empty($email)) {
            $this->addFlash('warning', $this->translator->trans('Email is invalid'));
            return $this->redirectToRoute(self::ADMIN_USERS_INDEX_ROUTE);
        }

        if ($this->userService->getByEmail($email)) {
            $this->addFlash('warning', $this->translator->trans('User already exists'));
            return $this->redirectToRoute(self::ADMIN_USERS_INDEX_ROUTE);
        }

        if ($this->validateCheckbox($request->request->get('sendEmailAndOtp'))) {
            $otp = $this->generateRandomPassword();
            $user = $registerNewUserService->setUp($username, $email, $otp);
            $newUserViaAdminMail->send($username, $email, $otp);
            $this->userService->updateIsVerified($user); // user not need to confirm email since added by admin

            $this->addFlash('notice', $this->translator->trans('An email with OTP has been sent to user') . '`' . $email . '`');

            return $this->redirectToRoute(self::ADMIN_USERS_INDEX_ROUTE);
        }

        $this->addFlash('warning', $this->translator->trans('All fields are required'));

        return $this->redirectToRoute(self::ADMIN_USERS_INDEX_ROUTE);
    }

    #[Route('/edit/{id}', name: 'app_admin_users_edit', methods: 'post')]
    public function edit(Request $request, int $id): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        if ($id <= 0) {
            $this->addFlash('warning', $this->translator->trans('User id is not defined'));
            return $this->redirectToRoute(self::ADMIN_USERS_INDEX_ROUTE);
        }

        $user = $this->userService->getById($id);

        if (!$user) {
            $this->addFlash('warning', $this->translator->trans('User not found'));
            return $this->redirectToRoute(self::ADMIN_USERS_INDEX_ROUTE);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        return $this->render('admin/user/edit_user.html.twig', [
            'user' => $user,
            'profile' => $profile,
            'notifications' => $notifications,
            'route' => $this->validate($request->request->get('route')),
            'roles' => UserHelper::ROLES
        ]);
    }

    #[Route('/store', name: 'app_admin_users_store', methods: 'post')]
    public function store(Request $request, WelcomeWriterMail $welcomeWriterMail): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $route = $this->validate($request->request->get('route'));

        if (empty($route)) {
            $route = self::ADMIN_USERS_INDEX_ROUTE;
        }

        $userId = $this->validateNumber($request->request->get('uId'));

        if ($userId <= 0) {
            $this->addFlash('warning', $this->translator->trans('User id is not defined'));
            return $this->redirectToRoute($route);
        }

        $user = $this->userService->getById($userId);

        if (null === $user) {
            $this->addFlash('warning', $this->translator->trans('User not found'));
            return $this->redirectToRoute($route);
        }

        if ($this->userService->isAdmin($user) && $this->getParameter('ALLOW_MODIFY_ADMIN_DETAILS') === 'off') {
            $this->addFlash(
                'notice', $this->translator->trans(
                    'Admin Details is not allowed to be modified. Config [ALLOW_MODIFY_ADMIN_DETAILS] must be active.'
                )
            );
            return $this->redirectToRoute($route);
        }

        $username = $this->validateUsernameAndReplaceSpace($request->request->get('username'));
        $usernameError = $this->usernameError($username);

        if ($usernameError) {
            $this->addFlash('warning', $usernameError);
            return $this->redirectToRoute($route);
        }

        $email = $this->validateEmail($request->request->get('email'));
        $role = $this->validate($request->request->get('role'));

        if (empty($email) || empty($role)) {
            $this->addFlash('warning', $this->translator->trans('Fields with star (*) are required'));
            return $this->redirectToRoute($route);
        }

        $role = UserHelper::ROLE_PREFIX . str_replace(' ', '_', strtoupper($role));
        $counts = max($this->validateNumber($request->request->get('counts')), 0);
        $tempEmail = $this->validateEmail($request->request->get('tempEmail'));
        $token = $this->validate($request->request->get('token'));
        $verified = $this->validateCheckbox($request->request->get('verified'));
        $disabled = $this->validateCheckbox($request->request->get('disabled'));
        $deleted = $this->validateCheckbox($request->request->get('deleted'));
        $isWriter = $this->validateCheckbox($request->request->get('isWriter'));
        $updatedAt = $this->validate($request->request->get('updated'));
        $createdAt = $this->validate($request->request->get('created'));

        if ($deleted) {
            $disabled = false;
        }

        if ($isWriter && $role !== UserHelper::ROLE_EDITOR) {
            $this->addFlash('warning', $this->translator->trans('User must have the role editor'));
            return $this->redirectToRoute($route);
        }

        $auther = $this->autherService->getOneByUser($user);

        if (
            $user->isWriter()
            && $auther
            && $auther->getArticlesCount() > 0
            && ($disabled || $deleted || !$isWriter || $role !== UserHelper::ROLE_EDITOR)
        ) {
            $this->addFlash(
                'warning',
                $this->translator->trans(
                    sprintf(
                        'Transfer Articles to another User. Article coutns [%s] - see Logs for more Infos!',
                        $auther->getArticlesCount()
                    )
                )
            );

            $this->systemLogsService->add(
                'Auther Details',
                sprintf(
                    'The user [%s] is an Auther and has [%s] Articles. The Articles must be transferd to another user
                     before changing the following details (is_disabled , is_deleted, is_writer and the role.)',
                    $user->getEmail(),
                    $auther->getArticlesCount()
                )
            );

            return $this->redirectToRoute($route);
        }

        $this->userService->update(
            $user,
            $username,
            $email,
            $role,
            $counts,
            $tempEmail,
            $token,
            $verified,
            $disabled,
            $deleted,
            $isWriter,
            $updatedAt,
            $createdAt
        );

        if ($isWriter && null === $this->autherService->getOneByUser($user)) {
            if ($this->autherService->add($user)) {
                $welcomeWriterMail->send($username, $email);
            }
        }

        $this->addFlash('success', $this->translator->trans('Changes has been saved'));

        return $this->redirectToRoute($route);
    }

    #[Route('/delete-user-permanently', name: 'app_admin_users_delete', methods: 'post')]
    public function delete(
        Request $request,
        AccountDeletedMail $accountDeletedMail,
        DeleteUserPermanentlyService $deleteUserPermanentlyService
    ): Response {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $route = $this->validate($request->request->get('route'));

        if (empty($route)) {
            $route = self::ADMIN_USERS_INDEX_ROUTE;
        }

        $userId = $this->validateNumber($request->request->get('uId'));

        if ($userId <= 0) {
            $this->addFlash('warning', $this->translator->trans('User can not be deleted. User id is not defined'));
            return $this->redirectToRoute($route);
        }

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {
            $this->addFlash('warning', $this->translator->trans('Please type your current password'));
            return $this->redirectToRoute($route);
        }

        $user = $this->userService->getById($userId);

        if (!$user) {
            $this->addFlash('warning', $this->translator->trans('Unable to delete user'));
            return $this->redirectToRoute($route);
        }

        if ($this->getUser()->getUserIdentifier() === $user->getEmail()) {
            $this->addFlash('notice', $this->translator->trans('Try once again. :)'));
            return $this->redirectToRoute($route);
        }

        $username = $user->getUsername();
        $email = $user->getEmail();

        if (!$deleteUserPermanentlyService->cleanUp($user)) {
            $this->addFlash('warning', $this->translator->trans('User can not be deleted. See the Exception in System-Logs'));
            return $this->redirectToRoute($route);
        }

        $sendEmailToUser = $this->validateCheckbox($request->request->get('accountDeletedEmail'));

        if ($sendEmailToUser && $user->getUsername() !== DataTransformer::TRANSFORMED_USERNAME) {
            $accountDeletedMail->send($username, $email);
        }

        $this->addFlash('success', $this->translator->trans('User and all related data has been deleted'));

        return $this->redirectToRoute($route);
    }
}