<?php

declare(strict_types=1);

namespace App\Controller\Admin\User;

use App\Controller\Admin\AbstractBaseController;
use App\Helper\SearchableRoutes;
use App\Service\Account\ProfileService;
use App\Service\NotificationService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/users')]
class SearchController extends AbstractBaseController
{
    use FormValidationTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const USERS_ROUTE = 'app_admin_user_index';

    public function __construct(
        private readonly ProfileService $profileService,
        private readonly UserService $userService,
        private readonly NotificationService $notificationService
    ) {}

    #[Route('/search', name: 'app_admin_user_search_show', methods: 'post')]
    public function show(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $keyword = $this->validate($request->request->get('keyword'));

        $route = $this->validate($request->request->get('route'));

        if (!in_array($route, SearchableRoutes::ADMIN_USERS, true)) {
            $route = self::USERS_ROUTE;
        }

        if (empty($keyword) || strlen($keyword) > 100) {
            return $this->redirectToRoute($route);
        }

        $user = $this->getUser();

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $users = $this->userService->search($keyword);

        return $this->render('admin/user/search.html.twig', [
            'users' => $users,
            'profile' => $profile,
            'notifications' => $notifications,
            'backToRoute' => $route,
            'keyword' => $keyword,
        ]);
    }
}
