<?php

declare(strict_types=1);

namespace App\Controller\Admin\User;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Admin\Job\TransformDeletedAccountsJob;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/users/job')]
class JobController extends AbstractBaseController
{
    use FormValidationTrait;

    private const ADMIN_USER_DELETED_ACCOUNTS_ROUTE = 'app_admin_user_deleted_accounts_index';

    public function __construct(
        private readonly UserService $userService,
        private readonly TransformDeletedAccountsJob $transformDeletedAccountsJob,
        private readonly TranslatorInterface $translator
    ) {}

    #[Route('/transform', name: 'app_admin_user_job_tranfom', methods: 'post')]
    public function transform(Request $request): Response 
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {
            $this->addFlash('warning', $this->translator->trans('Please type your current password'));
            return $this->redirectToRoute(self::ADMIN_USER_DELETED_ACCOUNTS_ROUTE);
        }

        $count = $this->transformDeletedAccountsJob->start();

        $count > 0
        ? $this->addFlash(
            'success',
            $this->translator->trans(
                sprintf('Data has been transformed for users [%s]', $count)
            )
        )
        : $this->addFlash('notice', $this->translator->trans('No data has been transformed'));

        return $this->redirectToRoute(self::ADMIN_USER_DELETED_ACCOUNTS_ROUTE);
    }
}
