<?php

declare(strict_types=1);

namespace App\Controller\Admin\User;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\NotificationService;
use App\Service\PaginationService;
use App\Service\UserService;
use App\Service\UserSettingService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/users')]
class DeletedAccountsController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const PAGE_NOT_FOUND_ROUTE_INDEX = 'app_meta_page_not_found';
    private const SEARCH_ROUTE = 'app_admin_user_search_show';

    public function __construct(
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly ProfileService $profileService,
        private readonly NotificationService $notificationService,
        private readonly UserService $userService
    ) {}

    #[Route('/deleted-accounts/{page?}', name: 'app_admin_user_deleted_accounts_index')]
    public function index(
        ?int $page,
        UserSettingService $userSettingService,
        PaginationService $paginationService
    ): Response {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $page = $this->validateNumber($page);

        if ($page < 0) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $limit = $userSettingService->getPaginationLimit($user);

        $offset = $paginationService->getOffset($page, $limit);

        $users = $this->userService->getDeletedAccountsWithOffsetAndLimit($offset, $limit);

        $pagination = $paginationService->paginate(
            $this->userService->getDeletedAccounts(),
            $page,
            $limit
        );

        return $this->render('admin/user/deleted_accounts.html.twig', [
            'users' => $users,
            'profile' => $profile,
            'notifications' => $notifications,
            'pagination' => $pagination,
            'paginationLimit' => $limit,
            'searchRoute' => self::SEARCH_ROUTE,
        ]);
    }
}
