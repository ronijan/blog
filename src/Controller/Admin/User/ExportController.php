<?php

declare(strict_types=1);

namespace App\Controller\Admin\User;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Admin\Export\ExportUserService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/users')]
class ExportController extends AbstractBaseController
{
    use FormValidationTrait;

    private const ADMIN_USERS_INDEX_ROUTE = 'app_admin_user_index';

    public function __construct(
        private readonly UserService $userService,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route('/export', name: 'app_admin_users_export', methods: 'post')]
    public function export(Request $request, ExportUserService $exportUserService): RedirectResponse|Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {

            $this->addFlash('warning', $this->translator->trans('Please type your current password'));

            return $this->redirectToRoute(self::ADMIN_USERS_INDEX_ROUTE);
        }

        return new Response($exportUserService->asJson());
    }
}