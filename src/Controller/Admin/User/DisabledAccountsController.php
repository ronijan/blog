<?php

declare(strict_types=1);

namespace App\Controller\Admin\User;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\NotificationService;
use App\Service\PaginationService;
use App\Service\UserService;
use App\Service\UserSettingService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/users')]
class DisabledAccountsController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const ADMIN_USERS_INDEX_ROUTE = 'app_admin_user_index';
    private const PAGE_NOT_FOUND_ROUTE_INDEX = 'app_meta_page_not_found';
    private const SEARCH_ROUTE = 'app_admin_user_search_show';

    public function __construct(
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly ProfileService $profileService,
        private readonly UserService $userService,
        private readonly NotificationService $notificationService
    ) {}

    #[Route('/disabled-accounts/{page?}', name: 'app_admin_user_disabled_accounts_index')]
    public function index(
        UserSettingService $userSettingService,
        PaginationService $paginationService,
        ?int $page
    ): Response {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $page = $this->validateNumber($page);

        if ($page < 0) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $limit = $userSettingService->getPaginationLimit($user);

        $offset = $paginationService->getOffset($page, $limit);

        $users = $this->userService->getDisabledAccountsWithOffsetAndLimit($offset, $limit);

        $pagination = $paginationService->paginate(
            $this->userService->getDisabledAccounts(),
            $page,
            $limit
        );

        return $this->render('admin/user/disabled_accounts.html.twig', [
            'users' => $users,
            'profile' => $profile,
            'notifications' => $notifications,
            'pagination' => $pagination,
            'paginationLimit' => $limit,
            'searchRoute' => self::SEARCH_ROUTE,
        ]);
    }
}
