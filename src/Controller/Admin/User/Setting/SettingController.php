<?php

declare(strict_types=1);

namespace App\Controller\Admin\User\Setting;

use App\Controller\Admin\AbstractBaseController;
use App\Helper\UserSettingConfig;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\Account\ResetPasswordService;
use App\Service\Account\TwoFactorAuthService;
use App\Service\NotificationService;
use App\Service\PaginationService;
use App\Service\UserService;
use App\Service\UserSettingService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/users/setting')]
class SettingController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;

    private const PAGE_NOT_FOUND_ROUTE_INDEX = 'app_meta_page_not_found';
    private const SEARCH_ROUTE = 'app_admin_user_setting_search_show';
    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const ADMIN_USER_SETTING_ROUTE = 'app_admin_user_setting_index';

    public function __construct(
        private readonly ProfileService $profileService,
        private readonly NotificationService $notificationService,
        private readonly UserSettingService $userSettingService,
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly TwoFactorAuthService $twoFactorAuthService,
        private readonly ResetPasswordService $resetPasswordService,
        private readonly TranslatorInterface $translator,
        private readonly UserService $userService
    ) {
    }

    #[Route('/home/{page?}', name: 'app_admin_user_setting_index')]
    public function index(PaginationService $paginationService, ?int $page): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        $page = $this->validateNumber($page);

        if ($page < 0) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $limit = $this->userSettingService->getPaginationLimit($user);

        $offset = $paginationService->getOffset($page, $limit);

        $userSetting = $this->userSettingService->getAllWithOffsetAndLimit($offset, $limit);

        $pagination = $paginationService->paginate($this->userSettingService->getAll(), $page, $limit);

        return $this->render('admin/user/setting/index.html.twig', [
            'userSetting' => $userSetting,
            'profile' => $profile,
            'notifications' => $notifications,
            'pagination' => $pagination,
            'paginationLimit' => $limit,
            'searchRoute' => self::SEARCH_ROUTE,
        ]);
    }

    #[Route('/view/{id}', name: 'app_admin_user_setting_show')]
    public function show(string $id): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $userSettingId = $this->validateNumber($id);

        if ($userSettingId <= 0) {
            $this->addFlash('warning', $this->translator->trans('Undefined ID'));
            return $this->redirectToRoute(self::ADMIN_USER_SETTING_ROUTE);
        }

        $userSetting = $this->userSettingService->getById($userSettingId);

        if ($userSetting === null) {
            $this->addFlash('warning', $this->translator->trans('Data could not be found'));
            return $this->redirectToRoute(self::ADMIN_USER_SETTING_ROUTE);
        }

        $auth = $this->twoFactorAuthService->getByUser($userSetting->getUser());

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        return $this->render('admin/user/setting/show.html.twig', [
            'profile' => $profile,
            'userSetting' => $userSetting,
            'notifications' => $notifications,
            'logsLimit' => UserSettingConfig::LOGS,
            'paginationLimits' => UserSettingConfig::PAGINATION,
            'auth' => $auth
        ]);
    }

    #[Route('store', name: 'app_admin_user_setting_store', methods: 'post')]
    public function store(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $paginationLimit = $this->validateNumber($request->request->get('paginationLimit'));
        $logsLimit = $this->validateNumber($request->request->get('logsLimit'));
        $sendLog = $this->validateCheckbox($request->request->get('sendLog'));
        $autoDeleteNotifications = $this->validateCheckbox($request->request->get('autoDeleteNotifications'));
        $informUserNewNotificationArrived = $this->validateCheckbox($request->request->get('informUser'));
        $userSettingUserId = $this->validateNumber($request->request->get('uID'));

        $user = $this->userService->getById($userSettingUserId);

        if ($userSettingUserId > 0 && $user) {
            $this->userSettingService->update(
                $user,
                $paginationLimit,
                $logsLimit,
                $sendLog,
                $autoDeleteNotifications,
                $informUserNewNotificationArrived
            );

            $this->addFlash('success', $this->translator->trans('Changes has been saved'));
        }

        return $this->redirectToRoute(self::ADMIN_USER_SETTING_ROUTE);
    }

    #[Route('store-2fa', name: 'app_admin_user_setting_store_2fa', methods: 'post')]
    public function storeTwoStepVerification(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $userId = $this->validateNumber($request->request->get('uId'));
        $twoStepVerification = $this->validateCheckbox($request->request->get('2fa'));

        $user = $this->userService->getById($userId);

        if (!$user) {
            $this->addFlash('warning', $this->translator->trans('User could not be found'));
            return $this->redirectToRoute(self::ADMIN_USER_SETTING_ROUTE);
        }

        $this->twoFactorAuthService->updateAlternativeEmailAndIsEnabled($user, null, $twoStepVerification);

        $twoStepVerification
            ? $this->addFlash('success', $this->translator->trans('Two step verification is enabled'))
            : $this->addFlash('notice', $this->translator->trans('Two step verification is disabled'));

        return $this->redirectToRoute(self::ADMIN_USER_SETTING_ROUTE);
    }

    #[Route('force-reset-pw', name: 'app_admin_user_setting_force_reset_pw', methods: 'post')]
    public function forceResetPassword(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $userId = $this->validateNumber($request->request->get('uId'));

        $user = $this->userService->getById($userId);

        if (!$user) {
            $this->addFlash('warning', $this->translator->trans('User could not be found'));
            return $this->redirectToRoute(self::ADMIN_USER_SETTING_ROUTE);
        }

        $this->resetPasswordService->createOrUpdate($user, $this->getRandomToken(28));

        $this->validateCheckbox($request->request->get('reset'))
            ? $this->addFlash(
                'notice',
                $this->translator->trans(
                    sprintf('The user (%s) will be forced to reset password', $user->getEmail())
                )
            )
            : $this->addFlash('notice', $this->translator->trans('Hmm. user will not be forced to reset password'));

        return $this->redirectToRoute(self::ADMIN_USER_SETTING_ROUTE);
    }
}