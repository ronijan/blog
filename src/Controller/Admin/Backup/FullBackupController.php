<?php

declare(strict_types=1);

namespace App\Controller\Admin\Backup;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Admin\Job\FullBackupJob;
use App\Service\NotificationService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin')]
class FullBackupController extends AbstractBaseController
{
    private const DASHBOARD_ROUTE = 'app_admin_index';

    public function __construct(private readonly NotificationService $notificationService) {}

    #[Route('/full-backup', name: 'app_admin_full_backup_index')]
    public function index(FullBackupJob $fullBackupJob, TranslatorInterface $translator): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $fullBackupJob->start();

        $this->addFlash('success', $translator->trans('Done! full backup was successfully'));

        $this->notificationService->notifyAdmin('Full-backup', 'Full backup script is executed successfully.');;

        return $this->redirectToRoute(self::DASHBOARD_ROUTE);
    }
}
