<?php

declare(strict_types=1);

namespace App\Controller\Admin\Notification;

use App\Controller\Admin\AbstractBaseController;
use App\Mails\Account\NotifyUserMail;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\NotificationService;
use App\Service\PaginationService;
use App\Service\UserService;
use App\Service\UserSettingService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/notification')]
class NotificationsController extends AbstractBaseController
{
    use FormValidationTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const ADMIN_NOTIFICATION_ROUTE_INDEX = 'app_admin_notification_index';
    private const PAGE_NOT_FOUND_ROUTE_INDEX = 'app_meta_page_not_found';
    private const SEARCH_ROUTE = 'app_admin_notification_search_show';

    public function __construct(
        private readonly ProfileService $profileService,
        private readonly NotificationService $notificationService,
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly UserService $userService,
        private readonly TranslatorInterface $translator,
        private readonly UserSettingService $userSettingService,
    ) {}

    #[Route('/home/{page?}', name: 'app_admin_notification_index')]
    public function index(UserSettingService $userSettingService, PaginationService $paginationService, ?int $page): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $page = $this->validateNumber($page);

        if ($page < 0) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $users = $this->userService->getActiveUsers();

        $notifications = $this->notificationService->getUnseenNotifications();

        $this->notificationService->updateIsSeen($notifications);

        $limit = $userSettingService->getPaginationLimit($user);

        $offset = $paginationService->getOffset($page, $limit);

        $allNotifications = $this->notificationService->getNotificationsWithOffsetAndLimit($offset, $limit);

        $pagination = $paginationService->paginate(
            $this->notificationService->getNotifications(),
            $page,
            $limit
        );

        return $this->render('admin/notification/index.html.twig', [
            'profile' => $profile,
            'users' => $users,
            'notifications' => $notifications,
            'allNotifications' => $allNotifications,
            'pagination' => $pagination,
            'paginationLimit' => $limit,
            'searchRoute' => self::SEARCH_ROUTE,
        ]);
    }

    #[Route('/view/{id}', name: 'app_admin_notification_show')]
    public function show(string $id): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $notificationId = $this->validateNumber($id);

        if ($notificationId <= 0) {
            $this->addFlash('warning', $this->translator->trans('Undefined ID'));
            return $this->redirectToRoute(self::ADMIN_NOTIFICATION_ROUTE_INDEX);
        }

        $notification = $this->notificationService->getById($notificationId);

        if ($notification === null) {
            $this->addFlash('warning', $this->translator->trans('Notification cannot be found'));
            return $this->redirectToRoute(self::ADMIN_NOTIFICATION_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        return $this->render('admin/notification/view.html.twig', [
            'profile' => $profile,
            'notification' => $notification,
            'notifications' => $notifications,
        ]);
    }

    #[Route('/add', name: 'app_admin_notification_new', methods: 'post')]
    public function new(Request $request, NotifyUserMail $notifyUserMail): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $subject = $this->validate($request->request->get('subject'));
        $message = $this->validateTextarea($request->request->get('message'));
        $isAllUsers = $this->validateCheckbox($request->request->get('all'));
        $userIds = $this->validateArray($request->get('uId'));

        if ($isAllUsers) {
            foreach ($this->userService->getActiveUsers() as $user) {
                if ($this->userSettingService->isActiveSendNotification($user)) {
                    $notifyUserMail->send($user->getUsername(), $user->getEmail());
                }

                $this->notificationService->create($user, $subject, $message);
            }

            $this->addFlash('success', $this->translator->trans('Notification has been added for all users'));

            return $this->redirectToRoute(self::ADMIN_NOTIFICATION_ROUTE_INDEX);
        }

        if (count($userIds) > 0) {
            foreach ($userIds as $userId) {
                $user = $this->userService->getById((int)$userId);

                if ($user !== null) {
                    $this->notificationService->create($user, $subject, $message);

                    if ($this->userSettingService->isActiveSendNotification($user)) {
                        $notifyUserMail->send($user->getUsername(), $user->getEmail());
                    }
                }
            }

            $this->addFlash('success', $this->translator->trans('Notification has been added for selected users'));

            return $this->redirectToRoute(self::ADMIN_NOTIFICATION_ROUTE_INDEX);
        }

        $this->addFlash('warning', $this->translator->trans('Hmm. Unable to send notification to unknown users'));

        return $this->redirectToRoute(self::ADMIN_NOTIFICATION_ROUTE_INDEX);
    }

    #[Route('/delete', name: 'app_admin_notification_delete', methods: 'post')]
    public function delete(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $notificationId = $this->validateNumber($request->request->get('nId'));

        if ($notificationId <= 0) {
            $this->addFlash('warning', $this->translator->trans('Notification can not be deleted'));
            return $this->redirectToRoute(self::ADMIN_NOTIFICATION_ROUTE_INDEX);
        }

        $notification = $this->notificationService->getById($notificationId);

        if ($notification === null) {
            $this->addFlash('warning', $this->translator->trans('Notification can not be found'));
            return $this->redirectToRoute(self::ADMIN_NOTIFICATION_ROUTE_INDEX);
        }

        if ($this->validateCheckbox($request->request->get('all'))) {
            $notifications = $this->notificationService->getAllByMessage($notification->getMessage());

            foreach ($notifications as $notification) {
                $this->notificationService->delete($notification);
            }

            $this->addFlash('success', $this->translator->trans('Notification has been deleted for all users'));

            return $this->redirectToRoute(self::ADMIN_NOTIFICATION_ROUTE_INDEX);
        }

        $this->notificationService->delete($notification);

        $this->addFlash('success', $this->translator->trans('Notification has been deleted'));

        return $this->redirectToRoute(self::ADMIN_NOTIFICATION_ROUTE_INDEX);
    }
}
