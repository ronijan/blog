<?php

declare(strict_types=1);

namespace App\Controller\Admin\Notification;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Account\ProfileService;
use App\Service\NotificationService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/notifications')]
class SearchController extends AbstractBaseController
{
    use FormValidationTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const NOTIFICATIONS_ROUTE = 'app_admin_notification_index';

    public function __construct(
        private readonly ProfileService $profileService,
        private readonly NotificationService $notificationService,
        private readonly UserService $userService
    ) {}

    #[Route('/search', name: 'app_admin_notification_search_show', methods: 'post')]
    public function show(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $keyword = $this->validate($request->request->get('keyword'));

        $route = $this->validate($request->request->get('route'));

        if ($route !== self::NOTIFICATIONS_ROUTE) {
            $route = self::NOTIFICATIONS_ROUTE;
        }

        if (empty($keyword) || strlen($keyword) > 100) {
            return $this->redirectToRoute($route);
        }

        $user = $this->getUser();

        $profile = $this->profileService->getByUser($user);

        $users = $this->userService->getActiveUsers();

        $notifications = $this->notificationService->getUnseenNotifications();

        $allNotifications = $this->notificationService->search($keyword);

        return $this->render('admin/notification/search.html.twig', [
            'allNotifications' => $allNotifications,
            'users' => $users,
            'profile' => $profile,
            'notifications' => $notifications,
            'route' => $route,
            'keyword' => $keyword,
        ]);
    }
}
