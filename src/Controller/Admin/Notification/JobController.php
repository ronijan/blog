<?php

declare(strict_types=1);

namespace App\Controller\Admin\Notification;

use App\Controller\Admin\AbstractBaseController;
use App\Service\NotificationService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/notifications/job')]
class JobController extends AbstractBaseController
{
    use FormValidationTrait;

    private const NOTIFICATIONS_ROUTE = 'app_admin_notification_index';

    public function __construct(
        private readonly UserService $userService,
        private readonly NotificationService $notificationService,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route('/delete-all', name: 'app_admin_notification_job_delete_all', methods: 'post')]
    public function show(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {

            $this->addFlash('warning', $this->translator->trans('Please type your current password'));

            return $this->redirectToRoute(self::NOTIFICATIONS_ROUTE);
        }

        $count = 0;

        foreach ($this->notificationService->getNotifications() as $notification) {

            $this->notificationService->delete($notification);

            $count++;
        }

        $this->addFlash(
            'success',
            $this->translator->trans(
                sprintf('All notifications has been deleted [%s]', $count)
            )
        );

        return $this->redirectToRoute(self::NOTIFICATIONS_ROUTE);
    }
}