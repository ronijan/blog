<?php

declare(strict_types=1);

namespace App\Controller\Admin\Newsletter;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Admin\Job\DeleteOldSubscribersBasedOnModifierJob;
use App\Service\NewsletterService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/newsletter/job')]
class JobController extends AbstractBaseController
{
    use FormValidationTrait;

    private const ADMIN_NEWSLETTER_ROUTE_INDEX = 'app_admin_newsletter_subscribers_index';

    public function __construct(
        private readonly DeleteOldSubscribersBasedOnModifierJob $deleteOldSubscribersJob,
        private readonly UserService $userService,
        private readonly NewsletterService $newsletterService,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route('/delete-all-older-than-six-months', name: 'app_admin_newsletter_job_delete_by_date', methods: 'post')]
    public function delete(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        if ($this->validateCheckbox($request->request->get('deleteSubscribers'))) {
            if ($count = $this->deleteOldSubscribersJob->start()) {
                $this->addFlash(
                    'success',
                    $this->translator->trans(sprintf('Subscribers has been deleted [%s]', $count))
                );

                return $this->redirectToRoute(self::ADMIN_NEWSLETTER_ROUTE_INDEX);
            }
        }

        $this->addFlash('notice', $this->translator->trans('No Data has been found'));

        return $this->redirectToRoute(self::ADMIN_NEWSLETTER_ROUTE_INDEX);
    }

    #[Route('/delete-all-permanently', name: 'app_admin_newsletter_job_delete_all', methods: 'post')]
    public function deleteAll(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {

            $this->addFlash('warning', $this->translator->trans('Please type your current password'));

            return $this->redirectToRoute(self::ADMIN_NEWSLETTER_ROUTE_INDEX);
        }

        $this->validateCheckbox($request->request->get('justInActive'))
            ? $subscribers = $this->newsletterService->getUnActiveSubscribers()
            : $subscribers = $this->newsletterService->getAll();

        $count = 0;

        if (count($subscribers) > 0) {
            foreach ($subscribers as $subscriber) {

                $this->newsletterService->delete($subscriber);

                $count++;
            }
        }

        $this->addFlash(
            'success',
            $this->translator->trans(sprintf('Subscribers has been deleted permanently [%s]', $count))
        );

        return $this->redirectToRoute(self::ADMIN_NEWSLETTER_ROUTE_INDEX);
    }
}