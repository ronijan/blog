<?php

declare(strict_types=1);

namespace App\Controller\Admin\Newsletter;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Admin\Export\ExportSubscribersDataService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/newsletter/export')]
class ExportController extends AbstractBaseController
{
    use FormValidationTrait;

    private const ADMIN_NEWSLETTER_ROUTE_INDEX = 'app_admin_newsletter_subscribers_index';

    public function __construct(
        private readonly UserService $userService,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route('/subscribers-data', name: 'app_admin_newsletter_export', methods: 'post')]
    public function export(Request $request, ExportSubscribersDataService $exportSubscribersDataService): RedirectResponse|Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {

            $this->addFlash('warning', $this->translator->trans('Please type your current password'));

            return $this->redirectToRoute(self::ADMIN_NEWSLETTER_ROUTE_INDEX);
        }

        if ($this->validateCheckbox($request->request->get('justEmails'))) {

            return new Response($exportSubscribersDataService->emailsAsJson());
        }

        return new Response($exportSubscribersDataService->asJson());
    }
}