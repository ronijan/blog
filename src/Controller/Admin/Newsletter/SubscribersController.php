<?php

declare(strict_types=1);

namespace App\Controller\Admin\Newsletter;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\NewsletterService;
use App\Service\NotificationService;
use App\Service\PaginationService;
use App\Service\UserSettingService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/subscriber')]
class SubscribersController extends AbstractBaseController
{
    use FormValidationTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const PAGE_NOT_FOUND_ROUTE_INDEX = 'app_meta_page_not_found';
    private const ADMIN_NEWSLETTER_ROUTE_INDEX = 'app_admin_newsletter_subscribers_index';
    private const SEARCH_ROUTE = 'app_admin_newsletter_search_show';

    public function __construct(
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly ProfileService $profileService,
        private readonly NotificationService $notificationService,
        private readonly NewsletterService $newsletterService,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route('/home/{page?}', name: 'app_admin_newsletter_subscribers_index')]
    public function index(
        ?int $page,
        PaginationService $paginationService,
        UserSettingService $userSettingService
    ): Response {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $page = $this->validateNumber($page);

        if ($page < 0) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $limit = $userSettingService->getPaginationLimit($user);

        $offset = $paginationService->getOffset($page, $limit);

        $subscribers = $this->newsletterService->getAllWithOffsetAndLimit($offset, $limit);

        $pagination = $paginationService->paginate(
            $this->newsletterService->getAll(),
            $page,
            $limit
        );

        return $this->render('admin/newsletter/index.html.twig', [
            'profile' => $profile,
            'notifications' => $notifications,
            'subscribers' => $subscribers,
            'pagination' => $pagination,
            'paginationLimit' => $limit,
            'searchRoute' => self::SEARCH_ROUTE,
        ]);
    }

    #[Route('/view/{id}', name: 'app_admin_newsletter_subscriber_show')]
    public function show(?int $id): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $subscriberId = $this->validateNumber($id);

        if ($subscriberId <= 0) {
            $this->addFlash('warning', $this->translator->trans('Undefined ID'));
            return $this->redirectToRoute(self::ADMIN_NEWSLETTER_ROUTE_INDEX);
        }

        $subscriber = $this->newsletterService->getById($subscriberId);

        if (!$subscriber) {
            $this->addFlash('warning', $this->translator->trans('Subscriber could not be found'));
            return $this->redirectToRoute(self::ADMIN_NEWSLETTER_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        return $this->render('admin/newsletter/edit.html.twig', [
            'subscriber' => $subscriber,
            'profile' => $profile,
            'notifications' => $notifications,
        ]);
    }

    #[Route('/store', name: 'app_admin_newsletter_subscriber_store')]
    public function store(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $subscriberId = $this->validateNumber($request->request->get('newsId'));

        if ($subscriberId <= 0) {
            $this->addFlash('warning', $this->translator->trans('Undefined ID'));
            return $this->redirectToRoute(self::ADMIN_NEWSLETTER_ROUTE_INDEX);
        }

        $subscriber = $this->newsletterService->getById($subscriberId);

        if (!$subscriber) {
            $this->addFlash('warning', $this->translator->trans('Subscriber could not be found'));
            return $this->redirectToRoute(self::ADMIN_NEWSLETTER_ROUTE_INDEX);
        }

        if ($this->validateCheckbox($request->request->get('delete'))) {
            $this->newsletterService->delete($subscriber);
            $this->addFlash('success', $this->translator->trans('Subscriber has been deleted permanentelly'));
            return $this->redirectToRoute(self::ADMIN_NEWSLETTER_ROUTE_INDEX);
        }

        $name = $this->validate($request->request->get('name'));
        $email = $this->validate($request->request->get('email'));

        if (empty($name) || empty($email)) {
            $this->addFlash('warning', $this->translator->trans('Fields with star (*) are required'));
            return $this->redirectToRoute(self::ADMIN_NEWSLETTER_ROUTE_INDEX);
        }

        $token = $this->validate($request->request->get('token'));
        $isSubscribed = $this->validateCheckbox($request->request->get('subscribed'));

        $this->newsletterService->update($subscriber, $name, $email, $token, $isSubscribed);

        $this->addFlash('success', $this->translator->trans('Changes has been saved'));

        return $this->redirectToRoute(self::ADMIN_NEWSLETTER_ROUTE_INDEX);
    }
}