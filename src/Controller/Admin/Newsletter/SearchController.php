<?php

declare(strict_types=1);

namespace App\Controller\Admin\Newsletter;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Account\ProfileService;
use App\Service\NewsletterService;
use App\Service\NotificationService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/newsletter')]
class SearchController extends AbstractBaseController
{
    use FormValidationTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const SUBSCRIBERS_ROUTE = 'app_admin_newsletter_subscribers_index';

    public function __construct(
        private readonly ProfileService $profileService,
        private readonly NotificationService $notificationService,
        private readonly NewsletterService $newsletterService
    ) {}

    #[Route('/search', name: 'app_admin_newsletter_search_show', methods: 'post')]
    public function show(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $keyword = $this->validate($request->request->get('keyword'));

        $route = $this->validate($request->request->get('route'));

        if ($route !== self::SUBSCRIBERS_ROUTE) {
            $route = self::SUBSCRIBERS_ROUTE;
        }

        if (empty($keyword) || strlen($keyword) > 100) {
            return $this->redirectToRoute($route);
        }

        $user = $this->getUser();

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $subscribers = $this->newsletterService->search($keyword);

        return $this->render('admin/newsletter/search.html.twig', [
            'subscribers' => $subscribers,
            'profile' => $profile,
            'notifications' => $notifications,
            'route' => $route,
            'keyword' => $keyword,
        ]);
    }
}
