<?php

declare(strict_types=1);

namespace App\Controller\Admin\Reference;

use App\Controller\Admin\AbstractBaseController;
use App\Service\FileHandlerService;
use App\Service\FileUploaderService;
use App\Service\ReferenceService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/reference')]
class JobController extends AbstractBaseController
{
    use FormValidationTrait;

    private const ADMIN_REFERENCES_ROUTE_INDEX = 'app_admin_references_index';

    public function __construct(
        private readonly ReferenceService $referenceService,
        private readonly UserService $userService,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route('/deactivate', name: 'app_admin_references_job_deactivate', methods: 'post')]
    public function deactivate(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {

            $this->addFlash('warning', $this->translator->trans('Please type your current password'));

            return $this->redirectToRoute(self::ADMIN_REFERENCES_ROUTE_INDEX);
        }

        $references = $this->referenceService->getPublished();

        $count = 0;

        foreach ($references as $reference) {

            $this->referenceService->updateIsPublished($reference, false);

            $count++;
        }

        $this->addFlash(
            'success',
            $this->translator->trans(
                sprintf('The references has been deactivated [%s]', $count)
            )
        );

        return $this->redirectToRoute(self::ADMIN_REFERENCES_ROUTE_INDEX);
    }

    #[Route('/store-image', name: 'app_admin_reference_job_upload_image_store', methods: 'post')]
    public function edit(Request $request, FileUploaderService $uploader, FileHandlerService $fileHandler): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $id = $this->validateNumber($request->request->get('id'));

        $reference = $this->referenceService->getById($id);

        if (!$reference) {

            $this->addFlash(
                'warning',
                $this->translator->trans(
                    sprintf('Reference with id [%s] could not be found', $id)
                )
            );

            return $this->redirectToRoute(self::ADMIN_REFERENCES_ROUTE_INDEX);
        }

        if ($reference->getId() === $id) {

            $dir = $this->getParameter('reference_dir');

            $fileHandler->unlinkFile($dir, $reference->getRefImage());

            /** @var UploadedFile $file */
            $file = $request->files->get('image');

            if ($file && $fileHandler->isImageExtensionAllowed($file->getClientOriginalExtension())) {

                $uploader->setTargetDirectory($dir);

                $refImage = $uploader->upload($file);
            }

            $this->referenceService->updateRefImage($reference, $refImage);

            $this->addFlash('success', $this->translator->trans('Image has been updated'));

            return $this->redirectToRoute(self::ADMIN_REFERENCES_ROUTE_INDEX);
        }

        $this->addFlash('warning', $this->translator->trans('Field is required'));

        return $this->redirectToRoute(self::ADMIN_REFERENCES_ROUTE_INDEX);
    }
}