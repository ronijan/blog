<?php

declare(strict_types=1);

namespace App\Controller\Admin\Reference;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\FileHandlerService;
use App\Service\FileUploaderService;
use App\Service\NotificationService;
use App\Service\ReferenceService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/reference')]
class HomeController extends AbstractBaseController
{
    use FormValidationTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const ADMIN_REFERENCES_ROUTE_INDEX = 'app_admin_references_index';

    public function __construct(
        private readonly ReferenceService $referenceService,
        private readonly UserService $userService,
        private readonly ProfileService $profileService,
        private readonly NotificationService $notificationService,
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly TranslatorInterface $translator,
        private readonly FileHandlerService $fileHandler,
    ) {
    }

    #[Route('/home', name: 'app_admin_references_index')]
    public function index(): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {

            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $references = $this->referenceService->getAll();

        return $this->render('admin/reference/index.html.twig', [
            'profile' => $profile,
            'references' => $references,
            'notifications' => $notifications,
        ]);
    }

    #[Route('/view/{id}', name: 'app_admin_references_show')]
    public function show(string $id): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {

            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $reference = $this->referenceService->getById($this->validateNumber($id));

        if (!$reference) {

            $this->addFlash('warning', $this->translator->trans('Reference cannot be found'));

            return $this->redirectToRoute(self::ADMIN_REFERENCES_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        return $this->render('admin/reference/edit.html.twig', [
            'profile' => $profile,
            'reference' => $reference,
            'notifications' => $notifications,
        ]);
    }

    #[Route('/add', name: 'app_admin_reference_new', methods: 'post')]
    public function new(Request $request, FileUploaderService $uploader): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $title = $this->validate($request->request->get('title'));
        $url = $this->validate($request->request->get('url'));
        $isPublished = $this->validateCheckbox($request->request->get('isPublished'));

        /** @var UploadedFile $file */
        $file = $request->files->get('avatar');

        if ($file && $this->fileHandler->isImageExtensionAllowed($file->getClientOriginalExtension())) {

            $dir = $this->getParameter('reference_dir');

            $uploader->setTargetDirectory($dir);

            $refImage = $uploader->upload($file);

            $this->referenceService->createOrUpdate(null, $refImage, $title, $url, $isPublished);

            $this->addFlash('success', $this->translator->trans('Reference has been added'));

            return $this->redirectToRoute(self::ADMIN_REFERENCES_ROUTE_INDEX);
        }

        $this->addFlash('warning', $this->translator->trans('Reference image field is required'));

        return $this->redirectToRoute(self::ADMIN_REFERENCES_ROUTE_INDEX);
    }

    #[Route('/store', name: 'app_admin_reference_store', methods: 'post')]
    public function edit(Request $request, FileHandlerService $fileHandlerService): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $id = $this->validateNumber($request->request->get('id'));

        $reference = $this->referenceService->getById($id);

        if ($this->validateCheckbox($request->request->get('isDeleted')) && $reference) {

            $fileHandlerService->unlinkFile(
                $this->getParameter('reference_dir'),
                $reference->getRefImage()
            );

            $this->referenceService->delete($reference);

            $this->addFlash('success', $this->translator->trans('Reference has been deleted'));

            return $this->redirectToRoute(self::ADMIN_REFERENCES_ROUTE_INDEX);
        }

        $title = $this->validate($request->request->get('title'));
        $url = $this->validate($request->request->get('url'));
        $image = $this->validate($request->request->get('image'));
        $isPublished = $this->validateCheckbox($request->request->get('isPublished'));

        $this->referenceService->createOrUpdate($reference, $image, $title, $url, $isPublished);

        $this->addFlash('success', $this->translator->trans('Reference details has been updated'));

        return $this->redirectToRoute(self::ADMIN_REFERENCES_ROUTE_INDEX);
    }
}