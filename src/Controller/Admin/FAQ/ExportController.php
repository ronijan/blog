<?php

declare(strict_types=1);

namespace App\Controller\Admin\FAQ;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Admin\Export\ExportFAQService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/faq')]
class ExportController extends AbstractBaseController
{
    use FormValidationTrait;

    private const ADMIN_FAQ_ROUTE_INDEX = 'app_admin_faq_index';

    public function __construct(
        private readonly UserService $userService,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route('/export', name: 'app_admin_faq_export', methods: 'post')]
    public function export(Request $request, ExportFAQService $exportFAQService): RedirectResponse|Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {

            $this->addFlash('warning', $this->translator->trans('Please type your current password'));

            return $this->redirectToRoute(self::ADMIN_FAQ_ROUTE_INDEX);
        }

        return new Response($exportFAQService->asJson());
    }
}