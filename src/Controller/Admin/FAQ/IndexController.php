<?php

declare(strict_types=1);

namespace App\Controller\Admin\FAQ;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\FAQService;
use App\Service\NotificationService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/faq')]
class IndexController extends AbstractBaseController
{
    use FormValidationTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const ADMIN_FAQ_ROUTE_INDEX = 'app_admin_faq_index';
    private const ADMIN_ROUTE_INDEX = 'app_admin_index';

    public function __construct(
        private readonly FAQService $faqService, 
        private readonly ProfileService $profileService, 
        private readonly NotificationService $notificationService,
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly TranslatorInterface $translator
    ) {}

    #[Route('/home', name: 'app_admin_faq_index')]
    public function index(): Response
     {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        if ($this->getParameter('ALLOW_RENDER_FAQ') === 'off') {
            $this->addFlash(
                'notice', 
                $this->translator->trans('This feature is not active. Please contact support  to activate it')
            );
            
            return $this->redirectToRoute(self::ADMIN_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $faq = $this->faqService->getAll();

        return $this->render('admin/faq/index.html.twig', [
            'faq' => $faq,
            'profile' => $profile,
            'notifications' => $notifications,
        ]);
    }

    #[Route('/add-new-faq', name: 'app_admin_faq_new', methods: 'post')]
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $title = $this->validate($request->request->get('title'));
        $description = $this->validateTextarea($request->request->get('description'));

        if (empty($title) || empty($description)) {
            $this->addFlash('warning', $this->translator->trans('Fields with (*) are required'));
            return $this->redirectToRoute(self::ADMIN_FAQ_ROUTE_INDEX);
        }

        $this->faqService->create($title, $description);

        $this->addFlash('success', $this->translator->trans('New FAQ has been added'));

        return $this->redirectToRoute(self::ADMIN_FAQ_ROUTE_INDEX);
    }

    #[Route('/view/{id}', name: 'app_admin_faq_show')]
    public function show(string $id): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::ADMIN_FAQ_ROUTE_INDEX);
        }

        if ($this->getParameter('ALLOW_RENDER_FAQ') === 'off') {
            $this->addFlash(
                'notice', 
                $this->translator->trans('This feature is not active. Please contact support to activate it.')
            );
            return $this->redirectToRoute(self::ADMIN_ROUTE_INDEX);
        }

        $id = $this->validateNumber($id);

        if ($id <= 0) {
            $this->addFlash('warning', $this->translator->trans('Undefined ID'));
            return $this->redirectToRoute(self::ADMIN_FAQ_ROUTE_INDEX);
        }

        $faq = $this->faqService->getById($id);

        if ($faq === null) {
            $this->addFlash('warning', $this->translator->trans('Data could not be found'));
            return $this->redirectToRoute(self::ADMIN_FAQ_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        return $this->render('admin/faq/edit.html.twig', [
            'faq' => $faq,
            'profile' => $profile,
            'notifications' => $notifications,
        ]);
    }

    #[Route('/store', name: 'app_admin_faq_store', methods: 'post')]
    public function store(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $title = $this->validate($request->request->get('title'));
        $description = $this->validateTextarea($request->request->get('description'));

        if (empty($title) || empty($description)) {
            $this->addFlash('warning', $this->translator->trans('Fields with (*) are required'));
            return $this->redirectToRoute(self::ADMIN_FAQ_ROUTE_INDEX);
        }

        $id = $this->validateNumber($request->request->get('faqId'));

        if ($id < 0) {
            $this->addFlash('warning', $this->translator->trans('Unknow ID'));
            return $this->redirectToRoute(self::ADMIN_FAQ_ROUTE_INDEX);
        }

        $faq = $this->faqService->getById($id);

        if ($faq) {
            $this->faqService->update($faq, $title, $description);
            $this->addFlash('success', $this->translator->trans('New FAQ has been updated'));
            return $this->redirectToRoute(self::ADMIN_FAQ_ROUTE_INDEX);
        }

        $this->addFlash('warning', $this->translator->trans('Data could not be found'));

        return $this->redirectToRoute(self::ADMIN_FAQ_ROUTE_INDEX);
    }

    #[Route('/delete', name: 'app_admin_faq_delete', methods: 'post')]
    public function delete(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $id = $this->validateNumber($request->request->get('faqId'));

        if (!is_numeric($id)) {
            $this->addFlash('warning', $this->translator->trans('Unkown ID'));
            return $this->redirectToRoute(self::ADMIN_FAQ_ROUTE_INDEX);
        }

        $faq = $this->faqService->getById($id);

        if($faq) {
            $this->faqService->delete($faq);
            $this->addFlash('success', $this->translator->trans('FAQ has been deleted'));
            return $this->redirectToRoute(self::ADMIN_FAQ_ROUTE_INDEX);
        }

        $this->addFlash('warning', $this->translator->trans('Data could not be found'));

        return $this->redirectToRoute(self::ADMIN_FAQ_ROUTE_INDEX);
    }
}
