<?php

declare(strict_types=1);

namespace App\Controller\Admin\FAQ;

use App\Controller\Admin\AbstractBaseController;
use App\Service\FAQService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/faq/job')]
class JobController extends AbstractBaseController
{
    use FormValidationTrait;

    private const ADMIN_FAQ_ROUTE_INDEX = 'app_admin_faq_index';

    public function __construct(
        private readonly UserService $userService,
        private readonly FAQService $frequentlyAskedQuestionsService,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route('/delete-all', name: 'app_admin_faq_job_delete_all', methods: 'post')]
    public function deleteAll(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {

            $this->addFlash('warning', $this->translator->trans('Please type your current password'));

            return $this->redirectToRoute(self::ADMIN_FAQ_ROUTE_INDEX);
        }

        $count = 0;

        foreach ($this->frequentlyAskedQuestionsService->getAll() as $faq) {

            $this->frequentlyAskedQuestionsService->delete($faq);

            $count++;
        }

        $this->addFlash(
            'success',
            $this->translator->trans(
                sprintf('FAQs has been deleted [%s]', $count)
            )
        );

        return $this->redirectToRoute(self::ADMIN_FAQ_ROUTE_INDEX);
    }
}