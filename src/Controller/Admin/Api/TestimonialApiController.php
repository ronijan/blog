<?php

declare(strict_types=1);

namespace App\Controller\Admin\Api;

use App\Controller\Admin\AbstractBaseController;
use App\Helper\TestimonialHelper;
use App\Mails\Admin\Api\TestimonialApiInvitationMail;
use App\Service\TestimonialService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/api/testimonial')]
class TestimonialApiController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;

    private const ADMIN_TESTIMONIAL_ROUTE_INDEX = 'app_admin_testimonial_index';

    public function __construct(
        private readonly TestimonialService $testimonialService,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route('/invite-a-customer', name: 'app_admin_api_testimonial_new', methods: 'post')]
    public function new(Request $request, TestimonialApiInvitationMail $testimonialInvitationMail): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $firstName = $this->validate($request->request->get('firstName'));
        $lastName = $this->validate($request->request->get('lastName'));
        $email = $this->validateEmail($request->request->get('email'));
        $isConfirmed = $this->validateCheckbox($request->request->get('confirm'));

        if ($firstName && $lastName && $email && $isConfirmed) {

            $token = $this->getRandomTokenOnlyLetters();

            $avatar = TestimonialHelper::DEFAULT_AVATAR;

            $this->testimonialService->create($firstName, $lastName, '', '', $avatar, $email, $token);

            $testimonialInvitationMail->send($lastName, $email, $token);

            $this->addFlash('success', $this->translator->trans('Customer has been invited'));

            return $this->redirectToRoute(self::ADMIN_TESTIMONIAL_ROUTE_INDEX);
        }

        $this->addFlash('warning', $this->translator->trans('All fields with (*) are required'));

        return $this->redirectToRoute(self::ADMIN_TESTIMONIAL_ROUTE_INDEX);
    }
}