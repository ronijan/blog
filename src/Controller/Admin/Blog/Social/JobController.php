<?php

declare(strict_types=1);

namespace App\Controller\Admin\Blog\Social;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Blog\SocialShareService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/blog/social/job')]
class JobController extends AbstractBaseController
{
    use FormValidationTrait;
    private const ADMIN_BLOG_SOCIAL_LINKS_INDEX_ROUTE = 'app_admin_blog_social_share_links_index';
    public function __construct(
        private readonly SocialShareService $socialShareService,
        private readonly UserService $userService,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route('/delete-all', name: 'app_admin_blog_social_share_links_delete_all', methods: 'post')]
    public function delete(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $pw = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $pw)) {
            $this->addFlash('warning', $this->translator->trans('Please type your current password'));
            return $this->redirectToRoute(self::ADMIN_BLOG_SOCIAL_LINKS_INDEX_ROUTE);
        }

        $count = 0;

        foreach ($this->socialShareService->getAll() as $shareLink) {

            $this->socialShareService->delete($shareLink);

            $count++;
        }

        $this->addFlash(
            'success',
            $this->translator->trans(
                sprintf('Social share links has been deleted [%s]', $count)
            )
        );

        return $this->redirectToRoute(self::ADMIN_BLOG_SOCIAL_LINKS_INDEX_ROUTE);
    }
}