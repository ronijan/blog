<?php

declare(strict_types=1);

namespace App\Controller\Admin\Blog\Social;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\Blog\SocialShareService;
use App\Service\NotificationService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/blog/social-media')]
class HomeController extends AbstractBaseController
{
    use FormValidationTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const ADMIN_BLOG_SOCIAL_LINKS_INDEX_ROUTE = 'app_admin_blog_social_share_links_index';

    public function __construct(
        private readonly ProfileService $profileService,
        private readonly NotificationService $notificationService,
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly TranslatorInterface $translator,
        private readonly SocialShareService $socialShareService
    ) {
    }

    #[Route('/share-links', name: 'app_admin_blog_social_share_links_index')]
    public function index(): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $socialLinks = $this->socialShareService->getAll();

        return $this->render('admin/blog/social/index.html.twig', [
            'profile' => $profile,
            'notifications' => $notifications,
            'socialLinks' => $socialLinks,
        ]);
    }

    #[Route('/new', name: 'app_admin_blog_social_share_links_new', methods: 'post')]
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $name = $this->validate($request->request->get('name'));
        $icon = $this->validate($request->request->get('icon'));
        $color = $this->validate($request->request->get('color'));
        $url = $this->validate($request->request->get('url'), true);

        if (empty($name) || empty($icon) || empty($color) || empty($url)) {
            $this->addFlash('warning', $this->translator->trans('All Fields are required.'));
            return $this->redirectToRoute(self::ADMIN_BLOG_SOCIAL_LINKS_INDEX_ROUTE);
        }

        $this->socialShareService->add($name, $icon, $color, $url);

        $this->addFlash('success', $this->translator->trans('New social link has been added.'));

        return $this->redirectToRoute(self::ADMIN_BLOG_SOCIAL_LINKS_INDEX_ROUTE);
    }

    #[Route('/edit/{id}', name: 'app_admin_blog_social_share_links_edit', methods: 'post')]
    public function delete(Request $request, string $id): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $socialLinkId = $this->validateNumber($request->request->get('id'));

        if ($this->validateNumber($id) !== $socialLinkId) {
            $this->addFlash('warning', $this->translator->trans('Unknown data.'));
            return $this->redirectToRoute(self::ADMIN_BLOG_SOCIAL_LINKS_INDEX_ROUTE);
        }

        $socialLink = $this->socialShareService->getById($socialLinkId);

        return $this->render('admin/blog/social/edit.html.twig', [
            'profile' => [],
            'notifications' => [],
            'socialLink' => $socialLink
        ]);
    }

    #[Route('/store', name: 'app_admin_blog_social_share_links_store', methods: 'post')]
    public function store(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $socialLinkId = $this->validateNumber($request->request->get('id'));
        $name = $this->validate($request->request->get('name'));
        $icon = $this->validate($request->request->get('icon'));
        $color = $this->validate($request->request->get('color'));
        $url = $this->validate($request->request->get('url'), true);

        $back = $this->redirectToRoute(self::ADMIN_BLOG_SOCIAL_LINKS_INDEX_ROUTE);

        if ($socialLinkId <= 0 || empty($name) || empty($icon) || empty($color) || empty($url)) {
            $this->addFlash('warning', $this->translator->trans('All Fields are required.'));
            return $back;
        }

        $socialLink = $this->socialShareService->getById($socialLinkId);

        if (!$socialLink) {
            $this->addFlash('warning', $this->translator->trans('Unknown data.'));
            return $back;
        }

        if ($this->validateCheckbox($request->request->get('delete'))) {
            $this->socialShareService->delete($socialLink);
            $this->addFlash('success', $this->translator->trans('Social link has been deleted.'));
            return $back;
        }

        $this->socialShareService->update($socialLink, $name, $icon, $color, $url);

        $this->addFlash('success', $this->translator->trans('Social link has been updated.'));

        return $back;
    }
}
