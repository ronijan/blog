<?php

declare(strict_types=1);

namespace App\Controller\Admin\Blog\Article;

use App\Controller\Admin\AbstractBaseController;
use App\Helper\BlogHelper;
use App\Helper\UserHelper;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\Blog\ArticleService;
use App\Service\Blog\AutherService;
use App\Service\Blog\CategoryService;
use App\Service\ContactFormService;
use App\Service\FileHandlerService;
use App\Service\FileUploaderService;
use App\Service\NotificationService;
use App\Service\PaginationService;
use App\Service\UserService;
use App\Service\UserSettingService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/blog')]
class ArticleController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const ADMIN_CONTACTS_INDEX_ROUTE = 'app_admin_contacts_index';
    private const PAGE_NOT_FOUND_ROUTE_INDEX = 'app_meta_page_not_found';
    private const SEARCH_ROUTE = 'app_admin_blog_articles_q_show';
    private const ADMIN_BLOG_AUTHERS_INDEX_ROUTE = 'app_admin_blog_authers_index';
    private const ADMIN_BLOG_ARTICLES_INDEX_ROUTE = 'app_admin_blog_articles_index';

    public function __construct(
        private readonly ProfileService $profileService,
        private readonly ContactFormService $contactFormService,
        private readonly UserSettingService $userSettingService,
        private readonly NotificationService $notificationService,
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly TranslatorInterface $translator,
        private readonly AutherService $autherService,
        private readonly CategoryService $categoryService,
        private readonly ArticleService $articleService,
        private readonly UserService $userService
    ) {
    }

    #[Route('/articles/home/{page?}', name: 'app_admin_blog_articles_index')]
    public function index(PaginationService $paginationService, ?string $page): Response
    {
        $this->denyAccessUnlessGrantedRoleEditor();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $page = $this->validateNumber($page);

        if ($page < 0) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $limit = $this->userSettingService->getPaginationLimit($user);

        $offset = $paginationService->getOffset($page, $limit);

        $auther = $this->autherService->getOneByUser($user);

        $authers = $this->autherService->getAuthers();

        $articles = $this->isGranted(UserHelper::ROLE_ADMIN)
            ? $this->articleService->getAllByLimitAndOffset($limit, $offset)
            : $this->articleService->getAllByAutherAndLimitAndOffset($auther, $limit, $offset);

        $articlesForCount = $this->isGranted(UserHelper::ROLE_ADMIN) ?
            $this->articleService->getArticles() :
            $this->articleService->getAllByAuther($auther);

        $pagination = $paginationService->paginate($articlesForCount, $page, $limit);

        return $this->render('admin/blog/article/index.html.twig', [
            'articles' => $articles,
            'profile' => $profile,
            'authers' => $authers,
            'notifications' => $notifications,
            'pagination' => $pagination,
            'paginationLimit' => $limit,
            'searchRoute' => self::SEARCH_ROUTE,
            'backToRoute' => self::ADMIN_BLOG_ARTICLES_INDEX_ROUTE
        ]);
    }

    #[Route('/article/new', name: 'app_admin_blog_article_new', methods: ['get', 'post'])]
    public function new(Request $request, FileUploaderService $uploader, FileHandlerService $fileHandler): Response
    {
        $this->denyAccessUnlessGrantedRoleEditor();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        if ($request->isMethod('post')) {
            $categoryId = $this->validateNumber($request->request->get('category'));
            $title = $this->validate($request->request->get('title'));
            $tags = $this->validate($request->request->get('tags'));
            $readTime = $this->validate($request->request->get('readTime'));
            $content = $this->validateTextarea($request->request->get('content'), true);

            if ($categoryId <= 0 || empty($title) || empty($tags) || empty($readTime) || empty($content)) {
                $this->addFlash('warning', 'All fields are required.');
                return $this->redirectToRoute(self::ADMIN_BLOG_ARTICLES_INDEX_ROUTE);
            }

            /** @var UploadedFile $file */
            $file = $request->files->get('image');

            if (!$file) {
                $this->addFlash('warning', $this->translator->trans('Please choose a valid image'));
                return $this->redirectToRoute(self::ADMIN_BLOG_ARTICLES_INDEX_ROUTE);
            }

            if (!$fileHandler->isImageExtensionAllowed($file->getClientOriginalExtension())) {
                $this->addFlash('warning', $this->translator->trans('Extension is not allowed'));
                return $this->redirectToRoute(self::ADMIN_BLOG_ARTICLES_INDEX_ROUTE);
            }

            $auther = $this->autherService->getOneByUser($user);

            if (!$auther) {
                $this->addFlash('warning', $this->translator->trans('No authers has been found! You need an Auther with Role Editor.'));
                return $this->redirectToRoute(self::ADMIN_BLOG_ARTICLES_INDEX_ROUTE);
            }

            $category = $this->categoryService->getById($categoryId);

            if ($auther && $category) {
                $uploader->setTargetDirectory($this->getParameter('article_dir'));
                $haedImage = $uploader->upload($file);
                $this->articleService->add($auther, $category, $title, $content, $readTime, $tags, $haedImage);
                $this->autherService->increaseArticleCounts($auther);
                $this->addFlash('success', 'Artcile has been added. It is not published yet.');
            }

            return $this->redirectToRoute(self::ADMIN_BLOG_ARTICLES_INDEX_ROUTE);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $categories = $this->categoryService->getCategories();

        return $this->render('admin/blog/article/add.html.twig', [
            'categories' => $categories,
            'profile' => $profile,
            'notifications' => $notifications,
            'readTime' => BlogHelper::ARTICLE_READ_TIME
        ]);
    }

    #[Route('/article/edit/{id}', name: 'app_admin_blog_article_edit')]
    public function edit(string $id): Response
    {
        $this->denyAccessUnlessGrantedRoleEditor();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $articleId = $this->validateNumber($id);

        $auther = $this->autherService->getOneByUser($user);

        if (!$auther) {
            $this->addFlash('warning', $this->translator->trans('Authercould not been found!'));
            return $this->redirectToRoute(self::ADMIN_BLOG_ARTICLES_INDEX_ROUTE);
        }

        $isAdmin = $this->isGranted(UserHelper::ROLE_ADMIN);

        $article = $isAdmin
            ? $this->articleService->getById($articleId)
            : $this->articleService->getOneByAutherAndId($auther, $articleId);

        if (!$article || (!$isAdmin && $article->isPublished())) {
            $this->addFlash('warning', sprintf('Article [ID: %s] cannot be found.', $articleId));
            return $this->redirectToRoute(self::ADMIN_BLOG_ARTICLES_INDEX_ROUTE);
        }

        $user = $this->getUser();

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $categories = $this->categoryService->getCategories();

        return $this->render('admin/blog/article/edit.html.twig', [
            'article' => $article,
            'profile' => $profile,
            'notifications' => $notifications,
            'categories' => $categories,
            'readTime' => BlogHelper::ARTICLE_READ_TIME
        ]);
    }

    #[Route('/article/store', name: 'app_admin_blog_article_store', methods: 'post')]
    public function store(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleEditor();

        $articleId = $this->validateNumber($request->request->get('id'));

        if ($articleId <= 0) {
            $this->addFlash('warning', sprintf('Article [ID: %s] could not be found.', $articleId));
            return $this->redirectToRoute(self::ADMIN_BLOG_ARTICLES_INDEX_ROUTE);
        }

        $title = $this->validate($request->request->get('title'));
        $tags = $this->validate($request->request->get('tags'));
        $readTime = $this->validate($request->request->get('readTime'));
        $content = $this->validateTextarea($request->request->get('content'), true);

        if (empty($title) || empty($tags) || empty($readTime) || empty($content)) {
            $this->addFlash('warning', 'All fields are required.');
            return $this->redirectToRoute(self::ADMIN_BLOG_ARTICLES_INDEX_ROUTE);
        }

        $categoryId = $this->validateNumber($request->request->get('category'));

        $category = $this->categoryService->getById($categoryId);

        if (!$category) {
            $this->addFlash('warning', 'Category could not be found.');
            return $this->redirectToRoute(self::ADMIN_BLOG_ARTICLES_INDEX_ROUTE);
        }

        $auther = $this->autherService->getOneByUser($this->getUser());

        if (!$auther) {
            $this->addFlash('warning', 'Auther could not be found.');
            return $this->redirectToRoute(self::ADMIN_BLOG_ARTICLES_INDEX_ROUTE);
        }

        $isAdmin = $this->isGranted(UserHelper::ROLE_ADMIN);

        $article = $isAdmin
            ? $this->articleService->getById($articleId)
            : $this->articleService->getOneByAutherAndId($auther, $articleId);

        if ($article && $isAdmin) {
            $isHeadArticle = $this->validateCheckbox($request->request->get('isHead'));

            if ($isHeadArticle) {
                $this->articleService->markOtherHeadArticleToBasic();
            }

            $isPublished = $this->validateCheckbox($request->request->get('isPublished'));
            $isCommantable = $this->validateCheckbox($request->request->get('isCommantable'));
            $isDeleted = $this->validateCheckbox($request->request->get('isDeleted'));

            if ($article->getComments()->count() > 0) {
                $isCommantable = true;
            }

            if ($isDeleted) {
                $isPublished = $isHeadArticle = $isCommantable = false;
                $this->autherService->decreaseArticleCounts($auther);
                $this->articleService->markLastOnePublishedAsHead();
            }

            $this->articleService->update(
                $article,
                $category,
                $title,
                $content,
                $readTime,
                $tags,
                $isPublished,
                $isHeadArticle,
                $isCommantable,
                $isDeleted
            );

            $this->addFlash('success', 'Artcile has been updated.');
        }

        return $this->redirectToRoute(self::ADMIN_BLOG_ARTICLES_INDEX_ROUTE);
    }

    #[Route('/article/delete', name: 'app_admin_blog_article_delete', methods: 'post')]
    public function delete(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        if (!$this->validateCheckbox($request->request->get('emptyBin'))) {
            $this->addFlash('warning', $this->translator->trans('All fields are required.'));
            return $this->redirectToRoute(self::ADMIN_BLOG_ARTICLES_INDEX_ROUTE);
        }

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {
            $this->addFlash('warning', $this->translator->trans('Please type your current password'));
            return $this->redirectToRoute(self::ADMIN_BLOG_ARTICLES_INDEX_ROUTE);
        }

        $articlesDeletedCount = $this->articleService->emptyBin();

        $this->addFlash(
            'success', $this->translator->trans(
                sprintf('Articles [%s] and all related comments has been deleted.', $articlesDeletedCount)
            )
        );

        return $this->redirectToRoute(self::ADMIN_BLOG_ARTICLES_INDEX_ROUTE);
    }
}