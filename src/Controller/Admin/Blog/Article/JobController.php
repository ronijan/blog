<?php

declare(strict_types=1);

namespace App\Controller\Admin\Blog\Article;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Blog\ArticleService;
use App\Service\Blog\AutherService;
use App\Service\Blog\CommentService;
use App\Service\FileHandlerService;
use App\Service\FileUploaderService;
use App\Service\UserService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/blog/articles/job')]
class JobController extends AbstractBaseController
{
    use FormValidationTrait;
    private const ADMIN_ARTICLES_ROUTE = 'app_admin_blog_articles_index';
    public function __construct(
        private readonly AutherService $autherService,
        private readonly UserService $userService,
        private readonly ArticleService $articleService,
        private readonly CommentService $commentService,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route('/transfer-articles', name: 'app_admin_blog_articles_job_transfer_articles', methods: 'post')]
    public function transferArticles(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $pw = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $pw)) {
            $this->addFlash('warning', $this->translator->trans('Please type your current password'));
            return $this->redirectToRoute(self::ADMIN_ARTICLES_ROUTE);
        }

        $sourceAutherId = $this->validateNumber($request->request->get('fromAuther'));

        $targetAutherId = $this->validateNumber($request->request->get('toAuther'));

        if ($sourceAutherId === $targetAutherId) {
            $this->addFlash('warning', 'Article-Transferation job could not be completed. [Same User]');
            return $this->redirectToRoute(self::ADMIN_ARTICLES_ROUTE);
        }

        $auther = $this->autherService->getOneById($sourceAutherId);

        if (!$auther) {
            $this->addFlash('warning', 'Auther could not be found');
            return $this->redirectToRoute(self::ADMIN_ARTICLES_ROUTE);
        }

        if ($auther->getArticlesCount() <= 0) {
            $this->addFlash(
                'notice',
                sprintf('The user [%s] has no Articles yet.', $auther->getUser()->getEmail())
            );
            return $this->redirectToRoute(self::ADMIN_ARTICLES_ROUTE);
        }

        $articles = $this->articleService->getAllByAuther($auther);

        $this->autherService->updateArticleCounts($auther, 0);

        $auther = $this->autherService->getOneById($targetAutherId);

        if (!$auther) {
            $this->addFlash('warning', 'Auther coud not be found');
            return $this->redirectToRoute(self::ADMIN_ARTICLES_ROUTE);
        }

        $count = 0;

        foreach ($articles as $article) {
            $this->articleService->updateAuther($article, $auther);
            $count++;
        }

        $this->autherService->updateArticleCounts($auther, $auther->getArticlesCount() + $count);

        $count > 0
            ? $this->addFlash(
                'success',
                sprintf(
                    'The count Articles [%s] has been transfered to [%s].',
                    $count,
                    $auther->getUser()->getEmail()
                )
            )
            : $this->addFlash('warning', 'No Articles has been transfered.');

        return $this->redirectToRoute(self::ADMIN_ARTICLES_ROUTE);
    }

    #[Route('/update-image', name: 'app_admin_blog_articles_job_update_image', methods: 'post')]
    public function updateArtcileImage(Request $request, FileUploaderService $uploader, FileHandlerService $fileHandler): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $articleId = $this->validateNumber($request->request->get('id'));

        $article = $this->articleService->getById($articleId);

        if (!$article) {

            $this->addFlash('warning', $this->translator->trans('No Article has been found'));

            return $this->redirectToRoute(self::ADMIN_ARTICLES_ROUTE);
        }

        /** @var UploadedFile $file */
        $file = $request->files->get('image');

        if (!$file) {

            $this->addFlash('warning', $this->translator->trans('Please choose a valid image'));

            return $this->redirectToRoute(self::ADMIN_ARTICLES_ROUTE);
        }

        if (!$fileHandler->isImageExtensionAllowed($file->getClientOriginalExtension())) {

            $this->addFlash('warning', $this->translator->trans('Extension is not allowed'));

            return $this->redirectToRoute(self::ADMIN_ARTICLES_ROUTE);
        }

        $dir = $this->getParameter('article_dir');

        if ($fileHandler->unlinkFile($dir, $article->getHeadImage())) {

            $uploader->setTargetDirectory($dir);

            $haedImage = $uploader->upload($file);

            $this->articleService->updateImage($article, $haedImage);

            $this->addFlash('success', $this->translator->trans('Image has been changed'));

            return $this->redirectToRoute(self::ADMIN_ARTICLES_ROUTE);
        }

        $this->addFlash('warning', $this->translator->trans('Image could not been updated'));

        return $this->redirectToRoute(self::ADMIN_ARTICLES_ROUTE);
    }

    #[Route('/delete-comments-and-its-replies', name: 'app_admin_blog_articles_job_delete_comment_and_its_replies', methods: 'post')]
    public function archiveAll(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $currentPassword = $this->validate($request->request->get('currentPassword'));

        if (!$this->userService->isPasswordValid($this->getUser(), $currentPassword)) {

            $this->addFlash('warning', $this->translator->trans('Please type your current password'));

            return $this->redirectToRoute(self::ADMIN_ARTICLES_ROUTE);
        }

        $id = $this->validateNumber($request->request->get('id'));

        $article = $this->articleService->getById($id);

        if (!$article) {

            $this->addFlash('warning', $this->translator->trans('Article could not be found'));

            return $this->redirectToRoute(self::ADMIN_ARTICLES_ROUTE);
        }

        if ($article->isCommentsAllowed() && $comments = $article->getComments()) {

            $count = 0;

            foreach ($comments as $comment) {

                $this->commentService->delete($comment);

                $count++;
            }

            $this->addFlash(
                'success',
                $this->translator->trans(
                    sprintf('Comments with its Replies has been deleted [%s]', $count)
                )
            );

            return $this->redirectToRoute(self::ADMIN_ARTICLES_ROUTE);
        }

        $this->addFlash('notice', $this->translator->trans('Article has no comments'));

        return $this->redirectToRoute(self::ADMIN_ARTICLES_ROUTE);
    }
}