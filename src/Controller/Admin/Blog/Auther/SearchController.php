<?php

declare(strict_types=1);

namespace App\Controller\Admin\Blog\Auther;

use App\Controller\Admin\AbstractBaseController;
use App\Helper\SearchableRoutes;
use App\Service\Account\ProfileService;
use App\Service\Blog\AutherService;
use App\Service\NotificationService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/blog/authers')]
class SearchController extends AbstractBaseController
{
    use FormValidationTrait;

    private const AUTHERS_ROUTE = 'app_admin_blog_authers_index';

    public function __construct(
        private readonly ProfileService $profileService,
        private readonly NotificationService $notificationService,
        private readonly AutherService $autherService
    ) {
    }

    #[Route('/q', name: 'app_admin_blog_authers_q_show', methods: 'post')]
    public function show(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $keyword = $this->validate($request->request->get('keyword'));

        $route = $this->validate($request->request->get('route'));

        if (!in_array($route, SearchableRoutes::ADMIN_AUTHERS, true)) {
            $route = self::AUTHERS_ROUTE;
        }

        if (empty($keyword) || strlen($keyword) > 100) {
            return $this->redirectToRoute($route);
        }

        $user = $this->getUser();

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $authers = $this->autherService->search($keyword);

        return $this->render('admin/blog/auther/search.html.twig', [
            'authers' => $authers,
            'profile' => $profile,
            'notifications' => $notifications,
            'route' => $route,
            'keyword' => $keyword,
        ]);
    }
}
