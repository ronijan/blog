<?php

declare(strict_types=1);

namespace App\Controller\Admin\Blog\Auther;

use App\Controller\Admin\AbstractBaseController;
use App\Helper\UserHelper;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\Blog\AutherService;
use App\Service\ContactFormService;
use App\Service\NotificationService;
use App\Service\PaginationService;
use App\Service\UserService;
use App\Service\UserSettingService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/blog')]
class HomeController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const ADMIN_CONTACTS_INDEX_ROUTE = 'app_admin_contacts_index';
    private const PAGE_NOT_FOUND_ROUTE_INDEX = 'app_meta_page_not_found';
    private const SEARCH_ROUTE = 'app_admin_blog_authers_q_show';
    private const ADMIN_BLOG_AUTHERS_INDEX_ROUTE = 'app_admin_blog_authers_index';

    public function __construct(
        private readonly ProfileService $profileService,
        private readonly ContactFormService $contactFormService,
        private readonly UserSettingService $userSettingService,
        private readonly NotificationService $notificationService,
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly TranslatorInterface $translator,
        private readonly UserService $userService,
        private readonly AutherService $autherService,
    ) {
    }

    #[Route('/authers/home/{page?}', name: 'app_admin_blog_authers_index')]
    public function index(PaginationService $paginationService, ?string $page): Response
    {
        $this->denyAccessUnlessGrantedRoleEditor();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $page = $this->validateNumber($page);

        if ($page < 0) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $limit = $this->userSettingService->getPaginationLimit($user);

        $offset = $paginationService->getOffset($page, $limit);
   
        $authers = $this->isGranted(UserHelper::ROLE_ADMIN)
            ? $this->autherService->getByOffsetAndLimit($offset, $limit)
            : $this->autherService->getOneByUserAsArray($user); // 4 Editor

        $pagination = $paginationService->paginate($this->autherService->getAuthers(), $page, $limit);

        return $this->render('admin/blog/auther/index.html.twig', [
            'authers' => $authers,
            'profile' => $profile,
            'notifications' => $notifications,
            'pagination' => $pagination,
            'paginationLimit' => $limit,
            'searchRoute' => self::SEARCH_ROUTE,
            'backToRoute' => self::ADMIN_BLOG_AUTHERS_INDEX_ROUTE
        ]);
    }

    #[Route('/auther/edit/{id}', name: 'app_admin_blog_auther_edit', methods: 'post')]
    public function edit(Request $request, ?string $id): Response
    {
        $this->denyAccessUnlessGrantedRoleEditor();

        $id = $this->validateNumber($id);

        $userId = $this->validateNumber($request->request->get('uId'));

        if ($id <= 0 || $userId <= 0) {
            $this->addFlash('warning', $this->translator->trans('No data found.'));
            return $this->redirectToRoute(self::ADMIN_BLOG_AUTHERS_INDEX_ROUTE);
        }

        $user = $this->getUser();

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $userAsAuther = $this->userService->getById($userId);

        $auther = null;

        if ($userAsAuther) {
            $auther = $this->autherService->getByUserAndId($userAsAuther, $id);
        }

        return $this->render('admin/blog/auther/edit.html.twig', [
            'auther' => $auther,
            'profile' => $profile,
            'notifications' => $notifications,
            'route' => self::ADMIN_BLOG_AUTHERS_INDEX_ROUTE
        ]);
    }

    #[Route('/auther/store', name: 'app_admin_blog_auther_store', methods: 'post')]
    public function store(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleEditor();

        $id = $this->validateNumber($request->request->get('id'));
        
        $userId = $this->validateNumber($request->request->get('uId'));

        $auther = $this->autherService->getByUserAndId($this->userService->getById($userId), $id);

        if (!$auther) {
            $this->addFlash('warning', $this->translator->trans('No data found.'));
            return $this->redirectToRoute(self::ADMIN_BLOG_AUTHERS_INDEX_ROUTE);
        }

        $username = $this->isGranted(UserHelper::ROLE_ADMIN)
            ? $this->validate($request->request->get('username'))
            : $auther->getUsername();

        $about = $this->validateTextarea($request->request->get('about'));

        $count = $this->isGranted(UserHelper::ROLE_ADMIN)
            ? $this->validateNumber($request->request->get('count'))
            : $auther->getArticlesCount();
            
        $isProfilePublic = $this->validateCheckbox($request->request->get('isProfilePublic'));

        $this->autherService->update($auther, $username, $about, $count, $isProfilePublic);

        $this->addFlash('success', $this->translator->trans('Changes are being saved.'));

        return $this->redirectToRoute(self::ADMIN_BLOG_AUTHERS_INDEX_ROUTE);
    }
}
