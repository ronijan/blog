<?php

declare(strict_types=1);

namespace App\Controller\Admin\Blog\Tag;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\Blog\ArticleService;
use App\Service\Blog\TagService;
use App\Service\NotificationService;
use App\Service\PaginationService;
use App\Service\UserSettingService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/blog')]
class HomeController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const PAGE_NOT_FOUND_ROUTE_INDEX = 'app_meta_page_not_found';
    private const ADMIN_BLOG_TAG_INDEX_ROUTE = 'app_admin_blog_tags_index';

    public function __construct(
        private readonly ProfileService $profileService,
        private readonly UserSettingService $userSettingService,
        private readonly NotificationService $notificationService,
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly TranslatorInterface $translator,
        private readonly TagService $tagService,
        private readonly ArticleService $articleService
    ) {
    }

    #[Route('/tags/home/{page?}', name: 'app_admin_blog_tags_index')]
    public function index(PaginationService $paginationService, ?string $page): Response
    {
        $this->denyAccessUnlessGrantedRoleEditor();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $page = $this->validateNumber($page);

        if ($page < 0) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $limit = $this->userSettingService->getPaginationLimit($user);

        $offset = $paginationService->getOffset($page, $limit);

        $tags = $this->tagService->getByOffsetAndLimit($offset, $limit);

        $pagination = $paginationService->paginate($this->tagService->getTags(), $page, $limit);

        return $this->render('admin/blog/tag/index.html.twig', [
            'tags' => $tags,
            'profile' => $profile,
            'notifications' => $notifications,
            'pagination' => $pagination,
            'paginationLimit' => $limit,
            //'searchRoute' => self::SEARCH_ROUTE,
            'backToRoute' => self::ADMIN_BLOG_TAG_INDEX_ROUTE
        ]);
    }

    #[Route('/tag/new', name: 'app_admin_blog_tag_new', methods: 'post')]
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $name = $this->validate($request->request->get('name'));

        if (empty($name)) {
            $this->addFlash('warning', $this->translator->trans('Field tag name is required.'));
            return $this->redirectToRoute(self::ADMIN_BLOG_TAG_INDEX_ROUTE);
        }

        $this->tagService->add($name);

        $this->addFlash('success', $this->translator->trans('New tag has been added.'));

        return $this->redirectToRoute(self::ADMIN_BLOG_TAG_INDEX_ROUTE);
    }

    #[Route('/tag/delete/{id}', name: 'app_admin_blog_tag_delete', methods: 'post')]
    public function delete(Request $request, string $id): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $tagId = $this->validateNumber($request->request->get('id'));

        if ($this->validateNumber($id) !== $tagId) {
            $this->addFlash('warning', $this->translator->trans('Unknown data.'));
            return $this->redirectToRoute(self::ADMIN_BLOG_TAG_INDEX_ROUTE);
        }

        if ($tag = $this->tagService->getById($tagId)) {
            $this->tagService->delete($tag);
            $this->addFlash('success', $this->translator->trans('Tag has been deleted.'));
        }

        return $this->redirectToRoute(self::ADMIN_BLOG_TAG_INDEX_ROUTE);
    }
}
