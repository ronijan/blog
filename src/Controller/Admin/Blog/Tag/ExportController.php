<?php

declare(strict_types=1);

namespace App\Controller\Admin\Blog\Tag;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Admin\Export\ExportTagsService;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/blog/tags')]
class ExportController extends AbstractBaseController
{
    public function __construct(
        private readonly UserService $userService,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route('/export', name: 'app_admin_blog_tag_export', methods: 'post')]
    public function export(ExportTagsService $exportTagsService): RedirectResponse|Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        return new Response($exportTagsService->asJson());
    }
}