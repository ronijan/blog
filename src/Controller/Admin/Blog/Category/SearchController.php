<?php

declare(strict_types=1);

namespace App\Controller\Admin\Blog\Category;

use App\Controller\Admin\AbstractBaseController;
use App\Helper\SearchableRoutes;
use App\Service\Account\ProfileService;
use App\Service\Blog\CategoryService;
use App\Service\NotificationService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/blog/categories')]
class SearchController extends AbstractBaseController
{
    use FormValidationTrait;

    private const CATEGORIES_ROUTE = 'app_admin_blog_categories_index';

    public function __construct(
        private readonly ProfileService $profileService,
        private readonly NotificationService $notificationService,
        private readonly CategoryService $categoryService
    ) {
    }

    #[Route('/q', name: 'app_admin_blog_categories_q_show', methods: 'post')]
    public function show(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $keyword = $this->validate($request->request->get('keyword'));

        $route = $this->validate($request->request->get('route'));

        if (!in_array($route, SearchableRoutes::ADMIN_CATEGORIES, true)) {
            $route = self::CATEGORIES_ROUTE;
        }

        if (empty($keyword) || strlen($keyword) > 100) {
            return $this->redirectToRoute($route);
        }

        $user = $this->getUser();

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $categories = $this->categoryService->search($keyword);

        return $this->render('admin/blog/category/search.html.twig', [
            'categories' => $categories,
            'profile' => $profile,
            'notifications' => $notifications,
            'route' => $route,
            'keyword' => $keyword,
        ]);
    }
}
