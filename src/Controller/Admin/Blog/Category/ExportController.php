<?php

declare(strict_types=1);

namespace App\Controller\Admin\Blog\Category;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Admin\Export\ExportCategoriesService;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/blog/categories')]
class ExportController extends AbstractBaseController
{
    public function __construct(
        private readonly UserService $userService,
        private readonly TranslatorInterface $translator
    ) {
    }

    #[Route('/export', name: 'app_admin_blog_categories_export', methods: 'post')]
    public function export(ExportCategoriesService $exportCategoriesService): RedirectResponse|Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        return new Response($exportCategoriesService->asJson());
    }
}