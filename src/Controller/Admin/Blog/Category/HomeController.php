<?php

declare(strict_types=1);

namespace App\Controller\Admin\Blog\Category;

use App\Controller\Admin\AbstractBaseController;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\Account\ProfileService;
use App\Service\Blog\ArticleService;
use App\Service\Blog\CategoryService;
use App\Service\NotificationService;
use App\Service\PaginationService;
use App\Service\UserSettingService;
use App\Traits\FormValidationTrait;
use App\Traits\RandomTokenGeneratorTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/blog')]
class HomeController extends AbstractBaseController
{
    use FormValidationTrait;
    use RandomTokenGeneratorTrait;

    private const TWO_FACTOR_AUTH_ROUTE = 'app_profile_security_2fa_index';
    private const PAGE_NOT_FOUND_ROUTE_INDEX = 'app_meta_page_not_found';
    private const SEARCH_ROUTE = 'app_admin_blog_categories_q_show';
    private const ADMIN_BLOG_CATEGORY_INDEX_ROUTE = 'app_admin_blog_categories_index';

    public function __construct(
        private readonly ProfileService $profileService,
        private readonly UserSettingService $userSettingService,
        private readonly NotificationService $notificationService,
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService,
        private readonly TranslatorInterface $translator,
        private readonly CategoryService $categoryService,
        private readonly ArticleService $articleService
    ) {
    }

    #[Route('/categories/home/{page?}', name: 'app_admin_blog_categories_index')]
    public function index(PaginationService $paginationService, ?string $page): Response
    {
        $this->denyAccessUnlessGrantedRoleEditor();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $page = $this->validateNumber($page);

        if ($page < 0) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE_INDEX);
        }

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $limit = $this->userSettingService->getPaginationLimit($user);

        $offset = $paginationService->getOffset($page, $limit);

        $categories = $this->categoryService->getByOffsetAndLimit($offset, $limit);

        $pagination = $paginationService->paginate($this->categoryService->getCategories(), $page, $limit);

        return $this->render('admin/blog/category/index.html.twig', [
            'categories' => $categories,
            'profile' => $profile,
            'notifications' => $notifications,
            'pagination' => $pagination,
            'paginationLimit' => $limit,
            'searchRoute' => self::SEARCH_ROUTE,
            'backToRoute' => self::ADMIN_BLOG_CATEGORY_INDEX_ROUTE
        ]);
    }

    #[Route('/category/new', name: 'app_admin_blog_category_new', methods: 'post')]
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $title = $this->validate($request->request->get('title'));

        if (empty($title)) {
            $this->addFlash('warning', $this->translator->trans('Field category is required.'));
            return $this->redirectToRoute(self::ADMIN_BLOG_CATEGORY_INDEX_ROUTE);
        }

        $this->categoryService->add($title);

        $this->addFlash('success', $this->translator->trans('New category has been added.'));

        return $this->redirectToRoute(self::ADMIN_BLOG_CATEGORY_INDEX_ROUTE);
    }

    #[Route('/category/edit/{id}', name: 'app_admin_blog_category_edit')]
    public function edit(?string $id): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $user = $this->getUser();

        if ($this->handelTwoFactorAuthService->handleTwoStepVerification($user)) {
            return $this->redirectToRoute(self::TWO_FACTOR_AUTH_ROUTE);
        }

        $id = $this->validateNumber($id);

        if ($id <= 0) {
            $this->addFlash('warning', $this->translator->trans('No data found.'));
            return $this->redirectToRoute(self::ADMIN_BLOG_CATEGORY_INDEX_ROUTE);
        }

        $user = $this->getUser();

        $profile = $this->profileService->getByUser($user);

        $notifications = $this->notificationService->getUnseenNotifications();

        $category = $this->categoryService->getById($id);

        $articlesCountInCategory = sprintf(
            'Currently there is or are [%s] articles related to this category [%s]',
            count($this->articleService->getByCategory($category)),
            $category->getTitle()
        );

        return $this->render('admin/blog/category/edit.html.twig', [
            'profile' => $profile,
            'category' => $category,
            'articlesCountInCategory' => $articlesCountInCategory,
            'notifications' => $notifications,
            'route' => self::ADMIN_BLOG_CATEGORY_INDEX_ROUTE
        ]);
    }

    #[Route('/category/store', name: 'app_admin_blog_category_store', methods: 'post')]
    public function store(Request $request): Response
    {
        $this->denyAccessUnlessGrantedRoleAdmin();

        $id = $this->validateNumber($request->request->get('id'));

        $title = $this->validate($request->request->get('title'));

        if ($id <= 0 || empty($title)) {
            $this->addFlash('warning', $this->translator->trans('No data found.'));
            return $this->redirectToRoute(self::ADMIN_BLOG_CATEGORY_INDEX_ROUTE);
        }

        $category = $this->categoryService->getById($id);

        if (!$category) {
            $this->addFlash('warning', $this->translator->trans('No data found.'));
            return $this->redirectToRoute(self::ADMIN_BLOG_CATEGORY_INDEX_ROUTE);
        }

        if ($this->validateCheckbox($request->request->get('delete'))) {
            $countDeletedArticles = $this->articleService->deleteByCategory($category);

            $this->categoryService->delete($category);

            $this->addFlash(
                'success',
                $this->translator->trans(sprintf(
                    'Category and related Articles [%s] are being deleted.',
                    $countDeletedArticles
                ))
            );

            return $this->redirectToRoute(self::ADMIN_BLOG_CATEGORY_INDEX_ROUTE);
        }

        $this->categoryService->updateTitle($category, $title);

        $this->addFlash('success', $this->translator->trans('Title has been updated.'));

        return $this->redirectToRoute(self::ADMIN_BLOG_CATEGORY_INDEX_ROUTE);
    }
}
