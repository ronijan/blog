<?php

declare(strict_types=1);

namespace App\Controller\Meta;

use App\Controller\AbstractBaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractBaseController
{
    #[Route('/privacy-policy', name: 'app_meta_privacy_policy')]
    public function privacyPolicy(): Response
    {
        return $this->render('meta/privacy_policy.html.twig');
    }

    #[Route('/cookies', name: 'app_meta_cookies')]
    public function cookies(): Response
    {
        return $this->render('meta/cookies.html.twig');
    }

    #[Route('/404', name: 'app_meta_page_not_found')]
    public function pageNotFound(): Response
    {
        return $this->render('bundles/TwigBundle/Exception/error404.html.twig');
    }

    #[Route('/403', name: 'app_meta_forbidden')]
    public function forbidden(): Response
    {
        return $this->render('bundles/TwigBundle/Exception/error403.html.twig');
    }

    #[Route('/429', name: 'app_meta_too_many_requests')]
    public function tooManyRequests(): Response
    {
        return $this->render('bundles/TwigBundle/Exception/error429.html.twig');
    }

    #[Route('/500', name: 'app_meta_internal_error')]
    public function internalError(): Response
    {
        return $this->render('bundles/TwigBundle/Exception/error500.html.twig');
    }
}
