<?php

declare(strict_types=1);

namespace App\Controller\Blog;

use App\Controller\AbstractBaseController;
use App\Service\Blog\ArticleService;
use App\Service\Blog\CommentService;
use App\Service\Blog\ReplyService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/reply')]
class ReplyController extends AbstractBaseController
{
    use FormValidationTrait;

    public function __construct(
        private readonly CommentService $commentService,
        private readonly ArticleService $articleService,
        private readonly ReplyService $replyService,
    ) {
    }

    #[Route('/add', name: 'app_blog_reply_new', methods: 'post')]
    public function new(Request $request): Response
    {
        $user = $this->getUser();

        if (!$user) {
            return new Response('You have to login.');
        }

        $replyText = $this->validateTextarea($request->request->get('replyComment'));
        $articleId = $this->validateNumber($request->request->get('aId'));
        $commentId = $this->validateNumber($request->request->get('commentId'));

        if (!$replyText || $articleId <= 0 || $commentId <= 0) {
            return new Response('Reply comment failed.');
        }

        $article = $this->articleService->getById($articleId);

        if (!$article) {
            return new Response('Article not found.');
        }

        $comment = $this->commentService->getById($commentId);

        if (!$comment) {
            return new Response('Comment not found.');
        }

        $this->replyService->add($comment, $user, $replyText);

        return new Response('Reply comment has been added.');
    }
}
