<?php

declare(strict_types=1);

namespace App\Controller\Blog;

use App\Controller\AbstractBaseController;
use App\Service\Blog\ArticleService;
use App\Service\Blog\CategoryService;
use App\Service\Blog\TagService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class searchController extends AbstractBaseController
{
    use FormValidationTrait;

    public function __construct(
        private readonly ArticleService $articleService,
        private readonly CategoryService $categoryService,
        private readonly TagService $tagService,
    ) {
    }

    #[Route('/articles/q', name: 'app_blog_search_q', methods: 'post')]
    public function index(Request $request): Response
    {
        $title = $request->request->get('search');

        $articles = [];

        if ($title) {
            $articles = $this->articleService->search($this->validate($title));
        }

        $recentArticles = $this->articleService->getRecentArticles(); // sidebar

        $categories = $this->categoryService->getCategories(); // sidebar

        $tags = $this->tagService->getTags(); // sidebar

        return $this->render('blog/search.html.twig', [
            'articles' => $articles,
            'categories' => $categories,
            'recentArticles' => $recentArticles,
            'searchTerm' => $title,
            'tags' => $tags,
        ]);
    }
}
