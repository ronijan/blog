<?php

declare(strict_types=1);

namespace App\Controller\Blog;

use App\Controller\AbstractBaseController;
use App\Entity\Blog\Comment;
use App\Helper\UserHelper;
use App\Service\Blog\ArticleService;
use App\Service\Blog\CommentService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/comment')]
class CommentController extends AbstractBaseController
{
    use FormValidationTrait;

    private static $format = 'M d, Y H:i';

    public function __construct(
        private readonly CommentService $commentService,
        private readonly ArticleService $articleService,
    ) {
    }

    #[Route('/add', name: 'app_blog_comment_new', methods: 'post')]
    public function new(Request $request): Response
    {
        $user = $this->getUser();

        if (!$user) {
            return new Response('Please login.');
        }

        $comment = $this->validateTextarea($request->request->get('iComment'));
        $articleId = $this->validateNumber($request->request->get('aId'));

        if (!$comment || !$articleId) {
            return new Response('Comment failed.');
        }

        $article = $this->articleService->getById($articleId);

        if (!$article) {
            return new Response('Comment failed.');
        }

        $this->commentService->add($article, $user, $comment);

        return new Response('Comment has been added.');
    }

    #[Route('/show/{id}', name: 'app_blog_comment_show', methods: 'post')]
    public function show(string $id): Response|JsonResponse
    {
        $articleId = $this->validateNumber($id);

        $article = $this->articleService->getById($articleId);

        if (!$article) {
            return new Response('Article not found.');
        }

        $results = $replies = [];
        $avatarDir = UserHelper::AVATAR_DIR;

        /** @var Comment $comment */
        foreach ($this->commentService->getAllByArticle($article) as $comment) {
            if (count($comment->getReplyComments()) > 0) {
                foreach ($comment->getReplyComments() as $reply) {

                    $replies[] = [
                        'username' => $reply->getUser()->getUsername(),
                        'image' => $avatarDir. $reply->getUser()->getProfile()->getName(),
                        'reply' => $reply->getReply(),
                        'updatedAt' => $reply->getCreatedAt()->format(self::$format),
                    ];
                }
            }

            $results[] = [
                'id' => $comment->getId(),
                'username' => $comment->getUser()->getUsername(),
                'image' => $avatarDir. $comment->getUser()->getProfile()->getName(),
                'updatedAt' => $comment->getCreatedAt()->format(self::$format),
                'comment' => $comment->getComment(),
                'replyies' => $replies,
            ];
        }

        return new JsonResponse($results);
    }
}
