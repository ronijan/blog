<?php

declare(strict_types=1);

namespace App\Controller\Blog;

use App\Controller\AbstractBaseController;
use App\Service\Blog\ArticleService;
use App\Service\Blog\CategoryService;
use App\Service\Blog\TagService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractBaseController
{
    use FormValidationTrait;

    public function __construct(
        private readonly ArticleService $articleService,
        private readonly CategoryService $categoryService,
        private readonly TagService $tagService
    ) {}

    #[Route('/category/{title}', name: 'app_blog_category_index')]
    public function index(string $title): Response
    {
        $title = $this->validate($title);

        $category = $this->categoryService->getOneByTitle($title);

        if(empty($category)) {
            return $this->redirectToRoute(self::HOME_ROUTE);
        }

        $articles = $this->articleService->getByCategory($category);

        $recentArticles = $this->articleService->getRecentArticles(); // sidebar

        $categories = $this->categoryService->getCategories(); // sidebar

        $tags = $this->tagService->getTags(); // sidebar

        return $this->render('blog/category.html.twig', [
            'articles' => $articles,
            'categories' => $categories,
            'recentArticles' => $recentArticles,
            'categoryTitle' => $title,
            'tags' => $tags,
        ]);
    }
}
