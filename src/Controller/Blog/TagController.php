<?php

declare(strict_types=1);

namespace App\Controller\Blog;

use App\Controller\AbstractBaseController;
use App\Service\Blog\ArticleService;
use App\Service\Blog\CategoryService;
use App\Service\Blog\TagService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TagController extends AbstractBaseController
{
    use FormValidationTrait;

    public function __construct(
        private readonly ArticleService $articleService,
        private readonly CategoryService $categoryService,
        private readonly TagService $tagService,
    ) {}

    #[Route('/tag/{tag}', name: 'app_blog_tags_index')]
    public function index(string $tag): Response
    {
        $tag = $this->validate($tag);

        $articles = $this->articleService->getPublishedArticlesByTag($tag);

        $recentArticles = $this->articleService->getRecentArticles(); // sidebar

        $categories = $this->categoryService->getCategories(); // sidebar

        $tags = $this->tagService->getTags(); // sidebar

        return $this->render('blog/tag.html.twig', [
            'articles' => $articles,
            'categories' => $categories,
            'recentArticles' => $recentArticles,
            'tagName' => $tag,
            'tags' => $tags,
        ]);
    }
}
