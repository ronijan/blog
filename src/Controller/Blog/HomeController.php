<?php

declare(strict_types=1);

namespace App\Controller\Blog;

use App\Controller\AbstractBaseController;
use App\Helper\BlogHelper;
use App\Service\Blog\ArticleService;
use App\Service\Blog\CategoryService;
use App\Service\Blog\CommentService;
use App\Service\Blog\SocialShareService;
use App\Service\Blog\TagService;
use App\Service\FAQService;
use App\Service\PaginationService;
use App\Traits\FormValidationTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class HomeController extends AbstractBaseController
{
    use FormValidationTrait;

    private const PAGE_NOT_FOUND_ROUTE_INDEX = 'app_meta_page_not_found';

    public function __construct(
        private readonly ArticleService $articleService,
        private readonly CategoryService $categoryService,
        private readonly TagService $tagService,
        private readonly CommentService $commentService,
        private readonly SluggerInterface $slugger,
        private readonly SocialShareService $socialShareService,
        private readonly UrlGeneratorInterface $urlGenerator
    ) {}

    #[Route('/{page?}', name: 'app_home')]
    public function index(FAQService $faqService, PaginationService $paginationService, ?string $page): Response
    {
        $page = $this->validateNumber($page);

        if ($page < 0) {
            return $this->redirectToRoute(self::PAGE_NOT_FOUND_ROUTE_INDEX);
        }

        $headArticle = $this->articleService->getHeadArticle();

        $recentArticles = $this->articleService->getRecentArticles(); // sidebar

        $categories = $this->categoryService->getCategories(); // sidebar

        $tags = $this->tagService->getTags(); // sidebar

        $limit = BlogHelper::DEFAULT_ARTICLES_PAGINATION_LIMIT;

        $offset = $paginationService->getOffset($page, $limit);

        $articles = $this->articleService->getPublishedArticlesByOffsetAndLimit($offset, $limit);

        $pagination = $paginationService->paginate(
            $this->articleService->getPublishedArticles(),
            $page,
            $limit
        );

        return $this->render('index.html.twig', [
            'headArticle' => $headArticle,
            'articles' => $articles,
            'categories' => $categories,
            'tags' => $tags,
            'recentArticles' => $recentArticles,
            'faq' => $faqService->getAll(),
            'pagination' => $pagination,
            'paginationLimit' => $limit,
        ]);
    }

    #[Route('/article/{slug}', name: 'app_blog_view')]
    public function view(string $slug): Response
    {
        $urlForShareBtn = $this->urlGenerator->generate('app_blog_view', ['slug' => $slug], 0);
        
        $textForShareBtn = str_replace('-', ' ', $slug);

        $slug = str_replace(['-', ','], ' ', $this->validate($slug));

        $article = $this->articleService->getByTitle($slug);

        if (!$article) {
            return $this->redirectToRoute('app_home');
        }

        $relatedPosts = $recentArticles = $this->articleService->getRelatedByCategoryAndNotId(
            $article->getCategory(),
            $article->getId()
        );

        $recentArticles = $this->articleService->getRecentArticles(); // sidebar

        $categories = $this->categoryService->getCategories(); // sidebar

        $tags = $this->tagService->getTags(); // sidebar

        $comments = $this->commentService->getAllByArticle($article);

        $socialShare = $this->socialShareService->getPreparedSocialLinks($urlForShareBtn, $textForShareBtn);

        return $this->render('blog/view.html.twig', [
            'article' => $article,
            'categories' => $categories,
            'recentArticles' => $recentArticles,
            'auther' => $article->getAuther(),
            'relatedPosts' => $relatedPosts,
            'tags' => $tags,
            'comments' => $comments,
            'socialShare' => $socialShare
        ]);
    }
}
