<?php

declare(strict_types=1);

namespace App\Traits;

use App\Helper\SessionName;
use Symfony\Component\HttpFoundation\Session\Session;

trait ResetPasswordTrait
{
    public function storeResetPasswordTokenInSession(?string $token): void
    {
        if ($token !== null) {
            $this->session()->set(SessionName::REST_PASSWORD, $token);
        }
    }

    public function getResetPasswordTokenFromSession()
    {
        return $this->session()->get(SessionName::REST_PASSWORD);
    }

    public function clearResetPasswordTokenFromSession(): void
    {
        if ($this->session()->has(SessionName::REST_PASSWORD)) {
            $this->session()->remove(SessionName::REST_PASSWORD);
        }
    }

    private function session(): Session
    {
        return new Session();
    }
}