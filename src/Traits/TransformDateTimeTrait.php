<?php

declare(strict_types=1);

namespace App\Traits;

use DateTime;
use DateTimeInterface;
use Exception;

trait TransformDateTimeTrait
{
    private function transformDateTime(DateTimeInterface $datetime, bool $fullDateTime = false): string
    {
        $now = new DateTime();
        $ago = '';

        try {
            $ago = new DateTime($datetime->format('Y-m-d H:i:s'));
        } catch (Exception $e) {
        }

        $differentTime = $now->diff($ago);

        $differentTime->w = floor($differentTime->d / 7);
        $differentTime->d -= $differentTime->w * 7;

        $components = [
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        ];

        foreach ($components as $key => &$value) {
            if ($differentTime->$key) {
                $value = $differentTime->$key . ' ' . $value . ($differentTime->$key > 1 ? 's' : '');
            } else {
                unset($components[$key]);
            }
        }

        unset($value);

        if (!$fullDateTime) {
            $components = array_slice($components, 0, 1);
        }

        return $components ? implode(', ', $components) . ' ago' : 'just now';
    }
}