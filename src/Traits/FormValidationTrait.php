<?php

declare(strict_types=1);

namespace App\Traits;

trait FormValidationTrait
{
    private function validate(string $input, bool $isRaw = false): string
    {
        if ($isRaw) {
            return $this->limitWords($this->replacePipe($input), 200);
        }

        return $this->escape($input);
    }

    private function validateEmail(?string $input): ?string
    {
        if (empty($input)) {
            return null;
        }

        $input = strtolower($input);
        $input = $this->limitWords($input, 200);
        $input = $this->escape($input);

        return filter_var($input, FILTER_VALIDATE_EMAIL) ? $input : null;
    }

    private function validateURL(?string $input): ?string
    {
        if (empty($input)) {
            return null;
        }

        $input = $this->escape($input);

        return filter_var($input, FILTER_VALIDATE_URL) ? $input : null;
    }

    private function validateCheckbox(?string $input): bool
    {
        if (empty($input)) {
            return false;
        }

        return ($this->escape($input) === strtolower('on'));
    }

    private function validateTextarea(?string $input, bool $isRaw = false): ?string
    {
        if (empty($input)) {
            return null;
        }

        if ($isRaw) {
            return $this->limitWords($this->replacePipe($input), 50000);
        }

        return $this->limitWords($this->escape($input), 50000);
    }

    // At least one lowercase, uppercase, numeric alphabetic character and its length has to be at least 8 characters
    private function passwordError(?string $input, bool $allowSimplePassword = false): ?string
    {
        if ($input === null || strlen($input) < '8') {
            return "Password must contain at least 8 characters.";
        }

        if (true === $allowSimplePassword) {
            return null;
        }

        $password = $this->escape($input);
        $error = null;

        if (!preg_match("#\d+#", $password)) {
            $error = "Password must contain at least 1 number.";
        } elseif (!preg_match("#[A-Z]+#", $password)) {
            $error = "Password must contain at least 1 capital letter.";
        } elseif (!preg_match("#[a-z]+#", $password)) {
            $error = "Password must contain at least 1 lowercase letter.";
        } elseif (!preg_match('/[\'$_+%*?=@#~!]/', $password)) {
            $error = "Password must contain at least 1 special character.";
        }

        return $error;
    }

    private function usernameError(?string $input): ?string
    {
        $error = null;

        if (empty($input)) {
            $error = 'Username is required.';
        } elseif (strlen($input) < '3') {
            $error = 'Username is to short. Min 3 chars.';
        } elseif (strlen($input) > '20') {
            $error = 'Username is to long. Max 20 chars.';
        } elseif (preg_match('/[\'^$%&*()}{@#~?>,<|=-]/', $input)) {
            $error = 'Username contains invalid chars. Please use underscores.';
        }

        return $error;
    }

    private function validateNumber(null|string|int $input): int
    {
        if ($input === null) {
            return 0;
        }

       return (int)$this->escape((string)$input);
    }

    private function validateArray(?array $inputs): array
    {
        $results = [];

        if (!empty($inputs)) {
            foreach ($inputs as $input) {
                $results[] = $this->validate($input);
            }
        }

        return $results;
    }


    private function escape(string $input): string
    {
        if (empty($input)) {
            return '';
        }

        $temp = trim($input);
        $temp = stripslashes($temp);
        $temp = $this->replacePipe($temp);

        return htmlspecialchars($temp);
    }

    private function limitWords(string $input, int $length): string
    {
        if (!empty($input) && strlen($input) > $length) {
            $input = substr($input, 0, $length);
        }

        return $input;
    }

    // Pipe should be removed cause of data import. Pipe is being used as separator between columns
    private function replacePipe(string $input): string
    {
        if (!empty($input) && preg_match('/[\'|]/', $input)) {
            $input = str_replace('|', '/', $input);
        }

        return $input;
    }

    private function validateUsernameAndReplaceSpace(string $input, string $replacement = '_'): string
    {
        return $this->escape(str_replace(' ', $replacement, $input));
    }
}