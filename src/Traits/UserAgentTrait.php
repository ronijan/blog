<?php

declare(strict_types=1);

namespace App\Traits;

trait UserAgentTrait
{
    private function getBrowser(bool $shouldToBeHashed = true): string
    {
        $browser = $_SERVER['HTTP_USER_AGENT'];

        if ($shouldToBeHashed === true) {
            return sha1($browser);
        }

        return $browser;
    }

    private function getOperatingSystem(bool $shouldToBeHashed = true): string
    {
        $op = PHP_OS;

        if ($shouldToBeHashed === true) {
            return sha1($op);
        }

        return $op;
    }
}