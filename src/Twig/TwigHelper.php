<?php

declare(strict_types=1);

namespace App\Twig;

use App\Entity\User;
use App\Exception\InvalidConfigArgumentException;
use App\Helper\UserHelper;
use App\Service\Account\HandelTwoFactorAuthService;
use App\Service\ConfigService;
use App\Traits\RandomTokenGeneratorTrait;
use App\Traits\TransformDateTimeTrait;
use DateTime;
use DateTimeInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\String\AbstractUnicodeString;
use Symfony\Component\String\Slugger\SluggerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigHelper extends AbstractExtension
{
    use RandomTokenGeneratorTrait;
    use TransformDateTimeTrait;

    public function __construct(
        private readonly ConfigService $configService,
        private readonly SluggerInterface $slugger,
        private readonly HandelTwoFactorAuthService $handelTwoFactorAuthService
    ) {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('isAdmin', [$this, 'getIsAdmin']),
            new TwigFunction('stripTags', [$this, 'getStripTags']),
            new TwigFunction('slug', [$this, 'getSlug']),
            new TwigFunction('replaceLinkWithHref', [$this, 'replaceLinkInText']),
            new TwigFunction('timeAgo', [$this, 'getDateTimeTransformer']),
            new TwigFunction('count', [$this, 'getCount']),
            new TwigFunction('dash', [$this, 'getThreeDash']),
            new TwigFunction('role', [$this, 'getRole']),
            new TwigFunction('active', [$this, 'getActiveRoute']),
            new TwigFunction('secureEmail', [$this, 'getSecureEmail']),
            new TwigFunction('handelTwoStepVerification', [$this, 'getHandelTwoStepVerification']),
            new TwigFunction('isFaqEnalbed', [$this, 'getFaq']),
            new TwigFunction('hash', [$this, 'getHash']),
            new TwigFunction('isDev', [$this, 'getIsDev']),
            new TwigFunction('appName', [$this, 'getAppName']),
            new TwigFunction('year', [$this, 'getYear']),
            new TwigFunction('faCircle', [$this, 'getCircle']),
            new TwigFunction('faThumbsUp', [$this, 'getThumbsUp']),
            new TwigFunction('faThumbsDown', [$this, 'getThumbsDown']),
            new TwigFunction('appCreated', [$this, 'getCreationDateOfApp']),
            new TwigFunction('appAuthor', [$this, 'getAppAuthor']),
            new TwigFunction('supportEmail', [$this, 'getSupportEmail']),
            new TwigFunction('madeBy', [$this, 'getMadeBy']),
            new TwigFunction('madeByURL', [$this, 'getMadeByURL']),
            new TwigFunction('supportEmail', [$this, 'getSupportEmail']),
            new TwigFunction('version', [$this, 'getVersion']),
        ];
    }

    public function getIsAdmin(User|UserInterface $user): bool
    {
        return in_array(UserHelper::ROLE_ADMIN, $user->getRoles(), true);
    }

    public function getStripTags(string $text): string
    {
        return strip_tags($text);
    }

    public function getSlug(string $slug): AbstractUnicodeString
    {
        return $this->slugger->slug(strtolower($slug));
    }

    public function replaceLinkInText(string $text): string
    {
        preg_match_all('#\bhttps?://[^,\s()<>]+(?:\(\w+\)|([^,[:punct:]\s]|/))#', $text, $matches);

        if (count($matches[0]) <= 0) {
            return $text;
        }

        $result = '';

        foreach ($matches[0] as $link) {
            $result = str_replace($link, '<a href="' . $link . '" target="_blank">' . $link . '</a>', $text);
        }

        return $result;
    }

    public function getDateTimeTransformer(DateTimeInterface $dateTime): string
    {
        return $this->transformDateTime($dateTime);
    }

    public function getCount(mixed $obj): int
    {
        if (is_countable($obj)) {
            return count($obj);
        }

        return 0;
    }

    public function getThreeDash(): string
    {
        return '---';
    }

    public function getRole(string $userRole): string
    {
        $role = '';

        if ($userRole === 'ROLE_USER') {
            $role = 'User';
        } elseif ($userRole === 'ROLE_EDITOR') {
            $role = 'Editor';
        } elseif ($userRole === 'ROLE_SUPER_ADMIN') {
            $role = 'Admin';
        }

        return $role;
    }

    public function getActiveRoute(string $route, string $cuurentRoute, string $class = 'avtive-link')
    {
        return $route == $cuurentRoute ? $class : '';
    }

    public function getSecureEmail(string $email, string $replacement = '*'): ?string
    {
        return preg_replace('/(?<=.).(?=.*@)/u', $replacement, $email);
    }

    public function getHandelTwoStepVerification($user): bool
    {
        if (!$user) {
            return false;
        }

        return $this->handelTwoFactorAuthService->handleTwoStepVerification($user);
    }

    public function getHash(?string $string = null): string
    {
        if (!empty($string)) {
            return sha1($string);
        }

        return sha1($this->getRandomToken(8));
    }

    /**
     * @throws InvalidConfigArgumentException
     */
    public function getIsDev(): bool
    {
        return $this->configService->getParameter('SHOW_DUMMY_DATA_ON_BLOG') === 'on';
    }

    /**
     * @throws InvalidConfigArgumentException
     */
    public function getFaq(): bool
    {
        return $this->configService->getParameter('ALLOW_RENDER_FAQ') === 'on';
    }

    /**
     * @throws InvalidConfigArgumentException
     */
    public function getAppName(): string
    {
        return $this->configService->getParameter('app_name');
    }

    /**
     * @throws InvalidConfigArgumentException
     */
    public function getCreationDateOfApp(): string
    {
        return $this->configService->getParameter('app_created');
    }

    /**
     * @throws InvalidConfigArgumentException
     */
    public function getAppAuthor(): string
    {
        return $this->configService->getParameter('app_author');
    }

    public function getYear(): string
    {
        return (new DateTime())->format('Y');
    }

    public function getCircle(string $color): void
    {
        echo "<span class='fa fa-circle fa-sm $color'></span>";
    }

    public function getThumbsUp(): void
    {
        echo "<span class='fa fa-thumbs-up text-success'></span>";
    }

    public function getThumbsDown(): void
    {
        echo "<span class='fa fa-thumbs-down text-danger'></span>";
    }

    /**
     * @throws InvalidConfigArgumentException
     */
    public function getSupportEmail(): string
    {
        return $this->configService->getParameter('supporter_email');
    }

    /**
     * @throws InvalidConfigArgumentException
     */
    public function getMadeBy(): string
    {
        return $this->configService->getParameter('app_made_by');
    }

    /**
     * @throws InvalidConfigArgumentException
     */
    public function getMadeByURL(): string
    {
        return $this->configService->getParameter('app_made_by_url');
    }

    /**
     * @throws InvalidConfigArgumentException
     */
    public function getVersion(): string
    {
        return 'v' . $this->configService->getParameter('app_version');
    }
}