<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Team;
use App\Helper\TeamHelper;
use App\Repository\TeamRepository;
use DateTime;

final class TeamService
{
    public function __construct(private readonly TeamRepository $teamRepository)
    {
    }

    public function getById(int $id): ?Team
    {
        return $this->teamRepository->find($id);
    }

    /**
     * @return Team[]
     */
    public function getActives(): array
    {
        return $this->teamRepository->findBy(['isActive' => 1]);
    }

    /**
     * @return Team[]
     */
    public function getAll(): array
    {
        return $this->teamRepository->findAll(); // ! use order by
    }

    public function updateIsActive(Team $team, bool $isActive): void
    {
        $team
            ->setIsActive($isActive)
            ->setUpdatedAt(new DateTime());

        $this->teamRepository->createOrUpdate($team, true);
    }

    public function updateImage(Team $team, string $image): Team
    {
        $team
            ->setImage($image)
            ->setUpdatedAt(new DateTime());

        $this->teamRepository->createOrUpdate($team, true);

        return $team;
    }

    public function createOrUpdate(
        ?Team $entity,
        string $firstName,
        string $lastName,
        string $position,
        bool $isActive,
        string $avatar = TeamHelper::DEFAULT_AVATAR,
        ?string $email = null,
        ?string $phone = null,
    ): Team {

        if (empty($entity)) {
            $entity = new Team();
        }

        $entity
            ->setFirstName($firstName)
            ->setLastName($lastName)
            ->setPosition($position)
            ->setIsActive($isActive)
            ->setImage($avatar)
            ->setEmail($email)
            ->setPhone($phone)
            ->setUpdatedAt(new DateTime());

        $this->teamRepository->createOrUpdate($entity, true);

        return $entity;
    }

    public function delete(Team $team): void
    {
        $this->teamRepository->remove($team, true);
    }
}