<?php

declare(strict_types=1);

namespace App\Service;

use App\Helper\SessionName;
use Symfony\Component\HttpFoundation\Session\Session;

final class FormSubmitGuardService
{
    public function isAllowedToSubmit(int $limit): bool
    {
        $counter = 1;
        $session = $this->getSession();

        if ($session->has(SessionName::COUNT_FORM_SUBMISSION)) {
            $session->set(SessionName::COUNT_FORM_SUBMISSION, $this->getCountSubmits() + 1);
        } else {
            $session->set(SessionName::COUNT_FORM_SUBMISSION, $counter);
        }

        return $this->getCountSubmits() > $limit;
    }

    public function showLimit(int $limit): string
    {
        return ($this->getCountSubmits() === $limit) ? '-/-' : $this->getCountSubmits() . '/' . $limit;
    }

    public function finishProcess(): void
    {
        $this->getSession()->remove(SessionName::COUNT_FORM_SUBMISSION);
    }

    protected function getCountSubmits(): int
    {
        return (int)$this->getSession()->get(SessionName::COUNT_FORM_SUBMISSION);
    }

    private function getSession(): Session
    {
        return new Session();
    }
}
