<?php

declare(strict_types=1);

namespace App\Service;

use App\Helper\Logger;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

final class FileUploaderService
{
    public function __construct(
        private string $targetDirectory,
        private readonly SluggerInterface $slugger
    ) {
    }

    public function upload(UploadedFile $file): string
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

        $safeFilename = $this->slugger->slug($originalFilename, '_');
        
        $fileName = $safeFilename . '_' . date('YmdHis') . '.' . $file->guessExtension();

        try {
            $file->move($this->getTargetDirectory(), $fileName);
        } catch (FileException $e) {
            Logger::addLog($e->getMessage(), 'debug');
        }

        return $fileName;
    }

    public function getTargetDirectory(): string
    {
        return $this->targetDirectory;
    }

    public function setTargetDirectory(string $targetDirectory): FileUploaderService
    {
        $this->targetDirectory = $targetDirectory;

        return $this;
    }
}
