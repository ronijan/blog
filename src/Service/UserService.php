<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Helper\Job;
use App\Helper\Locale;
use App\Helper\UserHelper;
use App\Repository\UserRepository;
use App\Service\Account\TwoFactorAuthService;
use DateTime;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;
use Symfony\Component\Security\Core\User\UserInterface;

final class UserService
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly UserPasswordHasher $userPasswordHash,
        private readonly TwoFactorAuthService $twoFactorAuthService,
        private readonly SystemLogsService $systemLogsService,
        private readonly FakerService $fakerService
    ) {
    }

    public function getById(int $id): ?User
    {
        return $this->userRepository->find($id);
    }

    public function getByUsername(string $username): ?User
    {
        return $this->userRepository->findOneBy(['username' => $username]);
    }

    public function getByEmail(string $email): ?User
    {
        return $this->userRepository->findOneBy(['email' => $email]);
    }

    public function getByToken(string $token): ?User
    {
        return $this->userRepository->findOneBy(['token' => $token]);
    }

    /**
     * @return User[]
     */
    public function getAuthers(): array
    {
        return $this->userRepository->findBy(['isWriter' => 1]);
    }

    /**
     * @return User[]
     */
    public function getAll(): array
    {
        return $this->userRepository->findAll();
    }

    /**
     * @return User[]
     */
    public function getActiveUsers(): array
    {
        return $this->userRepository->findActiveUsers();
    }

    /**
     * @return User[]
     */
    public function getActiveUsersWithOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->userRepository->findActiveUsersWithOffsetAndLimit($offset, $limit);
    }

    /**
     * @return User[]
     */
    public function getDisabledAccounts(): array
    {
        return $this->userRepository->findDisabledAccounts();
    }

    /**
     * @return User[]
     */
    public function getDisabledAccountsWithOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->userRepository->findDisabledAccountsWithOffsetAndLimit($offset, $limit);
    }

    /**
     * @return User[]
     */
    public function getDeletedAccounts(): array
    {
        return $this->userRepository->findDeletedAccounts();
    }

    /**
     * @return User[]
     */
    public function getDeletedAccountsWithOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->userRepository->findDeletedAccountsWithOffsetAndLimit($offset, $limit);
    }

    public function isAdmin(UserInterface $user): bool
    {
        $user = $this->getByEmail($user->getUserIdentifier());

        return $user && in_array(UserHelper::ROLE_ADMIN, $user->getRoles(), true);
    }

    public function create(User $user, ...$contents): User
    {
        [$username, $email, $hashPassword, $token] = $contents;

        $user
            ->setUsername($username)
            ->setEmail($email)
            ->setPassword($hashPassword)
            ->setToken($token)
            // ->setRoles(['ROLE_SUPER_ADMIN'])
            ->setLastPasswordUpdated(new DateTime())
            ->setUpdatedAt(new DateTime());

        $this->userRepository->add($user, true);

        return $user;
    }

    public function update(UserInterface|User $user, ...$contents): User
    {
        [$username, $email, $role, $counts, $tempEmail, $token, $verified, $disabled, $deleted, $writer, $updated, $created] = $contents;

        $user
            ->setUsername($username)
            ->setEmail($email)
            ->setRoles([$role])
            ->setLoginCounts($counts)
            ->setTempEmail($tempEmail)
            ->setToken($token)
            ->setIsWriter($writer)
            ->setIsVerified($verified)
            ->setIsDisabled($disabled)
            ->setIsDeleted($deleted)
            ->setUpdatedAt(new DateTime($updated))
            ->setCreatedAt(new DateTime($created));

        $this->userRepository->add($user, true);

        return $user;
    }

    public function addTokenAndTempEmail(
        UserInterface|User $user,
        ?string $newEmail = null,
        ?string $token = null
    ): User {
        $user
            ->setTempEmail($newEmail)
            ->setToken($token)
            ->setUpdatedAt(new DateTime());

        $this->userRepository->add($user, true);

        return $user;
    }

    public function updateUsername(UserInterface|User $user, string $username): User
    {
        $user
            ->setUsername($username)
            ->setUpdatedAt(new DateTime());

        $this->userRepository->add($user, true);

        return $user;
    }

    public function updateRole(UserInterface|User $user, string $role): ?User
    {
        if (in_array($role, UserHelper::AVAILABLE_ROLES, true)) {
            $user
                ->setRoles([$role])
                ->setUpdatedAt(new DateTime());

            $this->userRepository->add($user, true);

            return $user;
        }

        return null;
    }

    public function transformUserDataGDRR(UserInterface|User $user): void
    {
        $transformedEmail = $this->fakerService->getRandomEmail($user->getId());

        $tempEmail = null;

        if ($user->getToken()) {
            $tempEmail = $transformedEmail;
        }

        $transformedName = $this->fakerService->getName();

        $this->systemLogsService->add(
            'Data Transformed',
            sprintf('%s (%s) to (%s)', $user->getUsername(), $user->getEmail(), $transformedEmail)
        );

        $user
            ->setUsername($transformedName)
            ->setEmail($transformedEmail)
            ->setTempEmail($tempEmail)
            ->setUpdatedAt(new DateTime());

        $this->userRepository->add($user, true);

        $this->twoFactorAuthService->updateAlternativeEmail($user, $transformedEmail);
    }

    public function updateLocale(UserInterface|User $user, string $locale): User
    {
        $availabeLocale = array_keys(Locale::AVAILABLE, true);

        if (!in_array($locale, $availabeLocale, true)) {
            $locale = Locale::DEFAULT;
        }

        $user
            ->setLocale($locale)
            ->setUpdatedAt(new DateTime());

        $this->userRepository->add($user, true);

        return $user;
    }

    public function updateEmail(UserInterface|User $user, string $email): User
    {
        $user
            ->setEmail($email)
            ->setUpdatedAt(new DateTime());

        $this->userRepository->add($user, true);

        return $user;
    }

    public function updatePassword(UserInterface|User $user, string $hashedPassword): User
    {
        if (!$user->isVerified()) {
            $this->updateIsVerified($user);
        }

        $user
            ->setPassword($hashedPassword)
            ->setLastPasswordUpdated(new DateTime())
            ->setUpdatedAt(new DateTime());

        $this->userRepository->add($user, true);

        return $user;
    }

    public function updateTokenAndIsVerified(User $user, ?string $token = null, bool $isVerified = true): User
    {
        $user
            ->setToken($token)
            ->setIsVerified($isVerified)
            ->setUpdatedAt(new DateTime());

        $this->userRepository->add($user, true);

        return $user;
    }

    public function updateIsVerified(UserInterface|User $user, bool $isVerified = true): User
    {
        $user
            ->setIsVerified($isVerified)
            ->setUpdatedAt(new DateTime());

        $this->userRepository->add($user, true);

        return $user;
    }

    public function updateLastLoggedInAndLoginCounts(UserInterface|User $user): void
    {
        $counter = $user->getLoginCounts() + 1;
        $user
            ->setLastLoggedIn(new DateTime())
            ->setLoginCounts($counter)
            ->setUpdatedAt(new DateTime());

        $this->userRepository->add($user, true);
    }

    public function disableAccount(UserInterface|User $user, bool $isDisabled = true): User
    {
        $user
            ->setIsDisabled($isDisabled)
            ->setUpdatedAt(new DateTime());

        $this->userRepository->add($user, true);

        return $user;
    }

    public function deleteAccount(UserInterface|User $user, bool $isDeleted = true): User
    {
        $user
            ->setIsDeleted($isDeleted)
            ->setUpdatedAt(new DateTime());

        $this->userRepository->add($user, true);

        return $user;
    }

    public function isPasswordValid(User|UserInterface $user, string $plainPassword): bool
    {
        return $this->userPasswordHash->isPasswordValid($user, $plainPassword);
    }

    public function hashPassword(User|UserInterface $user, string $plainPassword): string
    {
        return $this->userPasswordHash->hashPassword($user, $plainPassword);
    }

    /**
     * @return User[]
     */
    public function getTransformableUserDeletedAccounts(): array
    {
        $modifier = (new DateTime())->modify(Job::DELETED_ACCOUNT_TRANSFORM_MODIFIER)->format('Y-m-d H:i:s');

        return $this->userRepository->findDeletedAccountsToBeTransformed($modifier);
    }

    /**
     * @return User[]
     */
    public function search(string $keyword): array
    {
        return $this->userRepository->search($keyword);
    }
}