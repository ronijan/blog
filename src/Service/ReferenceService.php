<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Reference;
use App\Helper\ReferenceHelper;
use App\Repository\ReferenceRepository;
use DateTime;

final class ReferenceService
{
    public function __construct(private readonly ReferenceRepository $referenceRepository)
    {
    }

    public function getById(int $id): ?Reference
    {
        return $this->referenceRepository->find($id);
    }

    /**
     * @return Reference[]
     */
    public function getPublished(): array
    {
        return $this->referenceRepository->findBy(['isPublished' => 1]);
    }

    /**
     * @return Reference[]
     */
    public function getSomeOfRecentlyPublished(): array
    {
        return $this->referenceRepository->findSomeOfRecentlyPublished(
            ReferenceHelper::COUNT_REFERENCE_SHOWN_IN_START_PAGE
        );
    }

    /**
     * @return Reference[]
     */
    public function getAll(): array
    {
        return $this->referenceRepository->findBy([], ['createdAt' => 'DESC']);
    }

    public function updateIsPublished(Reference $team, bool $isPublished): void
    {
        $team
            ->setIsPublished($isPublished)
            ->setUpdatedAt(new DateTime());

        $this->referenceRepository->createOrUpdate($team, true);
    }

    public function updateRefImage(Reference $team, string $refImage): Reference
    {
        $team
            ->setRefImage($refImage)
            ->setUpdatedAt(new DateTime());

        $this->referenceRepository->createOrUpdate($team, true);

        return $team;
    }

    public function createOrUpdate(
        ?Reference $entity,
        string $refImage,
        ?string $title = null,
        ?string $url = null,
        bool $isPublished = false,
    ): Reference {

        if (empty($entity)) {
            $entity = new Reference();
        }

        $entity
            ->setRefImage($refImage)
            ->setIsPublished($isPublished)
            ->setTitle($title)
            ->setUrl($url)
            ->setUpdatedAt(new DateTime());

        $this->referenceRepository->createOrUpdate($entity, true);

        return $entity;
    }

    public function delete(Reference $team): void
    {
        $this->referenceRepository->remove($team, true);
    }
}