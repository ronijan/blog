<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Faker;
use App\Helper\DataTransformer;
use App\Repository\FakerRepository;
use Faker\Factory;
use RuntimeException;

final class FakerService
{
    public function __construct(private readonly FakerRepository $fakerRepository)
    {
    }

    public function getName(): string
    {
        return DataTransformer::TRANSFORMED_USERNAME;
    }

    public function getOne(): Faker
    {
        return $this->getRandomOne();
    }

    public function getRandomEmail(int $dataId): string
    {
        $faker = $this->getRandomOne();

        return DataTransformer::TRANSFORMED_EMAIL_PREFIX . $dataId . '_' . $faker->getEmail();
    }

    public function create(): void
    {
        $fakerCreater = Factory::create();

        $faker = new Faker();
        $faker
            ->setName($fakerCreater->lastName())
            ->setEmail($fakerCreater->email());

        $this->fakerRepository->save($faker, true);
    }

    private function getRandomOne(): ?Faker
    {
        $faker = $this->fakerRepository->find(random_int(1, DataTransformer::FAKER_TABLE_ENTRIES_COUNT));

        if (!$faker) {
            throw new RuntimeException('Please seed Faker table with data.');
        }

        return $faker;
    }
}