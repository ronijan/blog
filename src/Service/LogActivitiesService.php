<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\LogActivities;
use App\Entity\User;
use App\Exception\InvalidConfigArgumentException;
use App\Mails\Account\UserLogActivityMail;
use App\Repository\LogActivitiesRepository;
use DateTime;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Security\Core\User\UserInterface;

final class LogActivitiesService
{
    public function __construct(
        private readonly LogActivitiesRepository $logActivitiesRepository,
        private readonly UserLogActivityMail $userLogActivityMail
    ) {
    }

    public function getById(int $id): ?LogActivities
    {
        return $this->logActivitiesRepository->find($id);
    }

    /**
     * @return LogActivities[]
     */
    public function getAllByUser(UserInterface $user): array
    {
        return $this->logActivitiesRepository->findBy(['user' => $user]);
    }

    /**
     * @return LogActivities[]
     */
    public function getAllByUserAndLogsLimit(UserInterface $user, int $logActivitiesLimit): array
    {
        return $this->logActivitiesRepository->findAllByUserAndLimit($user, $logActivitiesLimit);
    }

    public function add(UserInterface|User $user, string $action, string $message): LogActivities
    {
        $entity = new LogActivities();
        $entity
            ->setUser($user)
            ->setAction($action)
            ->setDescription($message)
            ->setUpdatedAt(new DateTime());

        $this->logActivitiesRepository->add($entity, true);

        return $entity;
    }

    public function delete(LogActivities $logActivities): void
    {
        $this->logActivitiesRepository->remove($logActivities, true);
    }

    public function cleanUpByUserAndLimit(UserInterface $user, int $logActivitiesLimit): int
    {
        $totalLogs = $this->logActivitiesRepository->count(['user' => $user]);

        if ($totalLogs <= $logActivitiesLimit) {
            return 0;
        }

        $limit = $totalLogs - $logActivitiesLimit;

        $logActivities = $this->logActivitiesRepository->findResetActivitiesByUserAndLimit($user, $limit);

        $countDeletedLogs = 0;

        if ($logActivities) {
            $countDeletedLogs = count($logActivities);

            foreach ($logActivities as $logActivity) {
                $this->delete($logActivity);
            }
        }

        return $countDeletedLogs;
    }

    /**
     * @throws InvalidConfigArgumentException
     * @throws TransportExceptionInterface
     */
    public function sendLogToUserIfEnabled(UserInterface|User $user, string $logActivity): void
    {
        if ($this->isSendingEmailEnabledByUser($user)) {
            $this->userLogActivityMail->send($user->getUsername(), $user->getUserIdentifier(), $logActivity);
        }
    }

    public function isSendingEmailEnabledByUser(UserInterface|User $user): bool
    {
        $userSetting = $user->getUserSetting();

        return $userSetting && $userSetting->isLogSending();
    }
}