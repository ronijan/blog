<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Testimonial;
use App\Helper\TestimonialHelper;
use App\Repository\TestimonialRepository;
use DateTime;

final class TestimonialService
{
    public function __construct(private readonly TestimonialRepository $testimonialRepository)
    {
    }

    public function getById(int $id): ?Testimonial
    {
        return $this->testimonialRepository->find($id);
    }

    public function getByToken(?string $token): ?Testimonial
    {
        if (empty($token)) {
            return null;
        }

        return $this->testimonialRepository->findOneByToken($token);
    }

    /**
     * @return Testimonial[]
     */
    public function getPublished(): array
    {
        return $this->testimonialRepository->findPublished();
    }

    /**
     * @return Testimonial[]
     */
    public function getSomeOfRecentlyPublished(): array
    {
        return $this->testimonialRepository->findSomeOfRecentlyPublished(
            TestimonialHelper::COUNT_TESTIMONIALS_SHOWN_IN_SECTION
        );
    }

    /**
     * @return Testimonial[]
     */
    public function getDeletedOrNotPublished(): array
    {
        return $this->testimonialRepository->findDeletedOrNotPublished();
    }

    /**
     * @return Testimonial[]
     */
    public function getAll(): array
    {
        return $this->testimonialRepository->findTestimonials();
    }

    public function create(
        string $firstName,
        string $lastName,
        string $position,
        string $description,
        string $avatar = TestimonialHelper::DEFAULT_AVATAR,
        ?string $email = null,
        ?string $token = null,
        bool $isPublished = false
    ): Testimonial {
        $entity = new Testimonial();
        $entity
            ->setFirstName($firstName)
            ->setLastName($lastName)
            ->setEmail($email)
            ->setPosition($position)
            ->setDescription($description)
            ->setAvatar($avatar)
            ->setIsPublished($isPublished)
            ->setToken($token)
            ->setUpdatedAt(new DateTime());

        $this->testimonialRepository->createOrUpdate($entity, true);

        return $entity;
    }

    public function updateIsPublished(int $id, bool $isPublished): void
    {
        $testimonial = $this->getById($id);

        if ($testimonial) {
            $testimonial
                ->setIsPublished($isPublished)
                ->setUpdatedAt(new DateTime());

            $this->testimonialRepository->createOrUpdate($testimonial, true);
        }
    }

    public function updateIsDeleted(int $id, bool $isDeleted): void
    {
        $testimonial = $this->getById($id);

        if ($testimonial) {
            $testimonial
                ->setIsDeleted($isDeleted)
                ->setUpdatedAt(new DateTime());

            $this->testimonialRepository->createOrUpdate($testimonial, true);
        }
    }

    public function updateImage(Testimonial $testimonial, string $image): Testimonial
    {
        $testimonial
            ->setAvatar($image)
            ->setUpdatedAt(new DateTime());

        $this->testimonialRepository->createOrUpdate($testimonial, true);

        return $testimonial;
    }

    public function update(
        Testimonial $testimonial,
        string $firstName,
        string $lastName,
        string $position,
        string $description,
        string $avatar,
        bool $isPublished,
        bool $isDeleted,
        ?string $email = null,
        ?string $token = null
    ): Testimonial {
        $testimonial
            ->setFirstName($firstName)
            ->setLastName($lastName)
            ->setEmail($email)
            ->setPosition($position)
            ->setDescription($description)
            ->setAvatar($avatar)
            ->setIsPublished($isPublished)
            ->setToken($token)
            ->setIsDeleted($isDeleted)
            ->setUpdatedAt(new DateTime());

        $this->testimonialRepository->createOrUpdate($testimonial, true);

        return $testimonial;
    }

    public function delete(Testimonial $testimonial): void
    {
        $this->testimonialRepository->remove($testimonial, true);
    }
}