<?php

declare(strict_types=1);

namespace App\Service;

use App\Helper\UserSettingConfig;

final class PaginationService
{
    public function paginate(array $data, int $pageNumberFromRequest, int $limit, int $adjacent = 2): array
    {
        $total = count($data);
        $totalPages = (int)ceil($total / $limit);
        $pageNumberFromRequest = $this->getPage($pageNumberFromRequest);
        $firstPage = $this->getFirstPage($totalPages, $adjacent, $pageNumberFromRequest);

        return [
            'total' => $total,
            'limit' => $limit,
            'page' => $pageNumberFromRequest,
            'totalPages' => $totalPages,
            'Newer' => $firstPage,
            'defualtLimit' => UserSettingConfig::DEFAULT_PAGINATION_LIMIT,
            'limitToResetPagination' => UserSettingConfig::LIMIT_TO_RESET_PAGINATION,
            'padginationOptions' => UserSettingConfig::PAGINATION,
        ];
    }

    public function getPage(int $pageInRequest): int
    {
        return $pageInRequest !== 0 ? $pageInRequest : 1;
    }

    public function getFirstPage(int $totalPages, int $adjacent, int $page): int
    {
        if (($page - $adjacent) > 1) {
            if (($page + $adjacent) < $totalPages) {
                $firstPage = ($page - $adjacent);
            } else {
                $firstPage = ($totalPages - (1 + ($adjacent * 2)));
            }
        } else {
            $firstPage = 1;
        }

        return $firstPage;
    }

    public function getOffset(int $pageInRequest, int $limit): int
    {
        return ($pageInRequest !== 0) ? ($limit * ($pageInRequest - 1)) : 0;
    }
}
