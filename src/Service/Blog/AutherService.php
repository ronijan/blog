<?php

declare(strict_types=1);

namespace App\Service\Blog;

use App\Entity\Blog\Auther;
use App\Entity\User;
use App\Helper\BlogHelper;
use App\Repository\Blog\AutherRepository;
use DateTime;
use Symfony\Component\Security\Core\User\UserInterface;

final class AutherService
{
    public function __construct(private readonly AutherRepository $autherRepository)
    {
    }
    public function getOneById(int $id): ?Auther
    {
        return $this->autherRepository->find($id);
    }

    public function getByUserAndId(User $user, int $id): ?Auther
    {
        return $this->autherRepository->findOneBy([
            'user' => $user,
            'id' => $id
        ]);
    }

    /**
     * @return Auther[]
     */
    public function getAuthers(): array
    {
        return $this->autherRepository->findAuthers();
    }

    public function getOneByUser(UserInterface|User $user): ?Auther
    {
        return $this->autherRepository->findOneBy(['user' => $user]);
    }

    /**
     * @return Auther[]
     */
    public function getOneByUserAsArray(UserInterface|User $user): array
    {
        return $this->autherRepository->findBy(['user' => $user]);
    }

    /**
     * @return Auther[]
     */
    public function getByOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->autherRepository->findByOffsetAndLimit($offset, $limit);
    }

    public function updateArticleCounts(Auther $auther, int $count): void
    {
        $counter = $count > 0 ? $count : 0;

        $auther
            ->setArticlesCount($counter)
            ->setUpdatedAt(new DateTime());

        $this->autherRepository->save($auther, true);
    }

    public function update(Auther $auther, string $username, string $aboutAuther, int $count, bool $isProfilePublic): void
    {
        $counter = $count > 0 ? $count : 0;

        $auther
            ->setUsername($username)
            ->setAbout($aboutAuther)
            ->setIsProfilePublic($isProfilePublic)
            ->setArticlesCount($counter)
            ->setUpdatedAt(new DateTime());

        $this->autherRepository->save($auther, true);
    }

    public function increaseArticleCounts(Auther $auther): void
    {
        $this->updateArticleCounts($auther, $auther->getArticlesCount() + 1);
    }

    public function decreaseArticleCounts(Auther $auther): void
    {
        $this->updateArticleCounts($auther, $auther->getArticlesCount() - 1);
    }

    /**
     * @return Auther[]
     */
    public function search(string $auther): array
    {
        return $this->autherRepository->search($auther);
    }

    public function add(UserInterface|User $user): Auther
    {
        $entity = new Auther();
        $entity
            ->setUsername($user->getUsername())
            ->setAbout(BlogHelper::ABOUT_AUTHER_ON_PUBLIC_PROFILE)
            ->setUser($user)
            ->setUpdatedAt(new DateTime());

        $this->autherRepository->save($entity, true);

        return $entity;
    }

    public function delete(?Auther $auther): void
    {
        if (!$auther) {
            return;
        }

        $this->autherRepository->remove($auther, true);
    }
}