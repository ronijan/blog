<?php

declare(strict_types=1);

namespace App\Service\Blog;

use App\Entity\Blog\Article;
use App\Entity\Blog\Auther;
use App\Entity\Blog\Category;
use App\Helper\BlogHelper;
use App\Repository\Blog\ArticleRepository;
use DateTime;

final class ArticleService
{
    public function __construct(
        private readonly ArticleRepository $articleRepository,
        private readonly CommentService $commentService
    ) {
    }

    public function getById(int $id): ?Article
    {
        return $this->articleRepository->findOneBy(['id' => $id]);
    }

    public function getByTitle(string $title): ?Article
    {
        $articles = $this->articleRepository->findByTitle($title);
        return $articles ? $articles[0] : null;
    }

    public function getOneByAutherAndId(Auther $auther, int $id): ?Article
    {
        return $this->articleRepository->findOneBy([
            'auther' => $auther,
            'id' => $id
        ]);
    }

    public function getHeadArticle(): ?Article
    {
        return $this->articleRepository->findOneBy([
            'isPublished' => 1,
            'isHead' => 1,
            'isDeleted' => 0
        ], ['createdAt' => 'DESC']);
    }

    /**
     * @return Article[]
     */
    public function getByCategory(Category $category): array
    {
        return $this->articleRepository->findBy([
            'category' => $category,
            'isPublished' => 1,
            'isDeleted' => 0
        ]);
    }

    /**
     * @return Article[]
     */
    public function getRelatedByCategoryAndNotId(Category $category, int $id): array
    {
        return $this->articleRepository->findRelatedByCategoryAndNotId($category, $id);
    }

    public function markLastOnePublishedAsHead(): void
    {
        $article = $this->articleRepository->findOneBy([
            'isPublished' => 1,
            'isDeleted' => 0,
        ], ['createdAt' => 'DESC']);

        if ($article) {
            $this->updateIsHead($article, true);
        }
    }

    /**
     * @return Article[]
     */
    public function getPublishedArticles(): array
    {
        return $this->articleRepository->findBy([
            'isPublished' => 1,
            'isHead' => 0,
            'isDeleted' => 0,
        ], ['id' => 'DESC']);
    }

    /**
     * @return Article[]
     */
    public function getPublishedArticlesByOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->articleRepository->findPublishedArticlesByOffsetAndLimit($offset, $limit);
    }

    /**
     * @return Article[]
     */
    public function getPublishedArticlesByTag(string $tag): array
    {
        return $this->articleRepository->findPublishedArticlesByTag($tag);
    }

    /**
     * @return Article[]
     */
    public function getRecentArticles(): array
    {
        return $this->articleRepository->findBy([
            'isPublished' => 1,
            'isHead' => 0,
            'isDeleted' => 0
        ], ['id' => 'DESC'], BlogHelper::DEFAULT_RECENT_ARTICLES_LINKS_ON_SIDEBAR);
    }

    /**
     * @return Article[]
     */
    public function getAllByLimitAndOffset(int $limit, int $offset): array
    {
        return $this->articleRepository->findBy([
            'isDeleted' => 0
        ], ['createdAt' => 'DESC'], $limit, $offset);
    }

    /**
     * @return Article[]
     */
    public function getAllByAuther(?Auther $auther): array
    {
        if (!$auther) {
            return [];
        }

        return $this->articleRepository->findBy([
            'auther' => $auther,
            // 'isPublished' => 0,
            // 'isDeleted' => 0
        ], ['createdAt' => 'DESC']);
    }

    /**
     * @return Article[]
     */
    public function getAllByAutherAndLimitAndOffset(?Auther $auther, int $limit, int $offset): array
    {
        if (!$auther) {
            return [];
        }

        return $this->articleRepository->findBy([
            'auther' => $auther,
            // 'isPublished' => 0,
            'isDeleted' => 0
        ], ['createdAt' => 'DESC'], $limit, $offset);
    }

    /**
     * @return Article[]
     */
    public function getArticles(): array
    {
        return $this->articleRepository->findBy(['isDeleted' => 0], ['createdAt' => 'DESC']);
    }

    public function getOneByAutherAndCategory(Auther $auther, Category $category): ?Article
    {
        return $this->articleRepository->findOneBy(['auther' => $auther, 'category' => $category]);
    }

    public function update(
        Article $article,
        Category $category,
        string $title,
        string $content,
        string $readTime,
        string $tags,
        bool $isPublished,
        bool $isHead,
        bool $isCommantable,
        bool $isDeleted
    ): Article {
        $article
            ->setCategory($category)
            ->setTitle($title)
            ->setContent($content)
            ->setReadTime($readTime)
            ->setTags($tags)
            ->setIsPublished($isPublished)
            ->setIsHead($isHead)
            ->setIsCommentsAllowed($isCommantable)
            ->setIsDeleted($isDeleted)
            ->setUpdatedAt(new DateTime());

        $this->articleRepository->save($article, true);

        return $article;
    }

    public function updateIsHead(Article $article, bool $isHead = false): Article
    {
        $article
            ->setIsHead($isHead)
            ->setUpdatedAt(new DateTime());

        $this->articleRepository->save($article, true);

        return $article;
    }

    public function updateImage(Article $article, string $image): Article
    {
        $article
            ->setHeadImage($image)
            ->setUpdatedAt(new DateTime());

        $this->articleRepository->save($article, true);

        return $article;
    }

    public function updateAuther(Article $article, Auther $auther): Article
    {
        $article
            ->setAuther($auther)
            ->setUpdatedAt(new DateTime());

        $this->articleRepository->save($article, true);

        return $article;
    }

    public function markOtherHeadArticleToBasic(): void
    {
        foreach ($this->articleRepository->findBy(['isHead' => 1]) as $article) {
            $this->updateIsHead($article);
        }
    }

    public function add(
        Auther $auther,
        Category $category,
        string $title,
        string $content,
        string $readTime,
        string $tags,
        string $haedImage
    ): Article {

        $entity = new Article();
        $entity
            ->setAuther($auther)
            ->setCategory($category)
            ->setTitle($title)
            ->setContent($content)
            ->setReadTime($readTime)
            ->setTags($tags)
            ->setHeadImage($haedImage)
            ->setUpdatedAt(new DateTime());

        $this->articleRepository->save($entity, true);

        return $entity;
    }

    public function deleteByCategory(Category $category): int
    {
        $count = 0;
        $articles = $this->getByCategory($category);

        if (count($articles) > 0) {
            foreach ($articles as $article) {
                $count++;
                $this->delete($article);
            }
        }

        return $count;
    }

    /**
     * @return Article[]
     */
    public function getRemovedArticles(): array
    {
        return $this->articleRepository->findBy(['isDeleted' => 1]);
    }

    public function emptyBin(): int
    {
        $count = 0;

        foreach ($this->getRemovedArticles() as $article) {

            $count++;

            if ($article->isCommentsAllowed() || count($article->getComments()) > 0) {

                foreach ($article->getComments() as $comment) {
                    $this->commentService->delete($comment);
                }
            }

            $this->delete($article);
        }

        return $count;
    }

    /**
     * @return Article[]
     */
    public function search(string $title): array
    {
        return $this->articleRepository->search($title);
    }

    public function delete(Article $article): void
    {
        $this->articleRepository->remove($article, true);
    }
}