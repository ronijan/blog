<?php

declare(strict_types=1);

namespace App\Service\Blog;

use App\Entity\Blog\Tag;
use App\Repository\Blog\TagRepository;
use DateTime;

final class TagService
{
    public function __construct(private readonly TagRepository $tagRepository) {}

    public function getById(int $id): ?Tag
    {
        return $this->tagRepository->find($id);
    }

    /**
     * @return Tag[]
     */
    public function getTags(): array
    {
        return $this->tagRepository->findBy([], ['id' => 'DESC']);
    }

    /**
     * @return Tag[]
     */
    public function getByOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->tagRepository->findBy([], ['id' => 'DESC'], $limit, $offset);
    }

    public function add(string $tagName): Tag
    {
        $tagModel = new Tag();

        $tagModel
            ->setName($tagName)
            ->setUpdatedAt(new DateTime());

        $this->tagRepository->save($tagModel, true);

        return $tagModel;
    }

    public function delete(Tag $tag): void
    {
        $this->tagRepository->remove($tag, true);
    }
}
