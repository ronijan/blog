<?php

declare(strict_types=1);

namespace App\Service\Blog\Export;

abstract class ExporterAbstruct
{
    protected function prepareJsonHeader(string $fileName): void
    {
        $file = ucfirst($fileName) . '_' . date('Ymd_His') . '.json';

        header("Content-Type: application/json");
        header("Content-Disposition: attachment; filename=" . htmlspecialchars($file));
        header("Pragma: no-cache");
        header("Expires: 0");
    }
}