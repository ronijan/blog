<?php

declare(strict_types=1);

namespace App\Service\Blog\Export;

use App\Entity\Blog\Category;
use App\Service\Blog\CategoryService;

final class ExportCategoriesService extends ExporterAbstruct implements ExporterInterface
{
    public function __construct(
        private readonly CategoryService $categoryService,
    ) {
    }

    public function asJson(): bool|string
    {
        $this->prepareJsonHeader('Exported_Blog_Categories');

        return json_encode($this->export(), JSON_PRETTY_PRINT);
    }

    private function export(): array
    {
        return array_map(static function (Category $entity) {
            return [
                'title' => $entity->getTitle(),
                'articles count' => $entity->getArticles()->count(),
                'modified' => ($entity->getUpdatedAt())->format('Y-m-d H:i:s'),
                'created' => ($entity->getCreatedAt())->format('Y-m-d H:i:s')
            ];
        }, $this->categoryService->getCategories());
    }
}