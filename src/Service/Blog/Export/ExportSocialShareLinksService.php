<?php

declare(strict_types=1);

namespace App\Service\Blog\Export;

use App\Entity\Blog\SocialShare;
use App\Service\Blog\SocialShareService;

final class ExportSocialShareLinksService extends ExporterAbstruct implements ExporterInterface
{
    public function __construct(
        private readonly SocialShareService $socialShareService,
    ) {
    }

    public function asJson(): string|bool
    {
        $this->prepareJsonHeader('Exported_Social_Share_Links');

        return json_encode($this->export(), JSON_PRETTY_PRINT);
    }

    private function export(): array
    {
        return array_map(static function (SocialShare $entity) {
            return [
                'name' => $entity->getName(),
                'icon' => $entity->getIcon(),
                'color' => $entity->getColor(),
                'url' => $entity->getUrl(),
                'modified' => ($entity->getUpdatedAt())->format('Y-m-d H:i:s'),
                'created' => ($entity->getCreatedAt())->format('Y-m-d H:i:s')
            ];
        }, $this->socialShareService->getAll());
    }
}