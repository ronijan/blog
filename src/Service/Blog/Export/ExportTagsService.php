<?php

declare(strict_types=1);

namespace App\Service\Blog\Export;

use App\Entity\Blog\Tag;
use App\Service\Blog\TagService;

final class ExportTagsService extends ExporterAbstruct implements ExporterInterface
{
    public function __construct(
        private readonly TagService $tagService,
    ) {
    }

    public function asJson(): string|bool
    {
        $this->prepareJsonHeader('Exported_Blog_Tags');

        return json_encode($this->export(), JSON_PRETTY_PRINT);
    }

    private function export(): array
    {
        return array_map(static function (Tag $entity) {
            return [
                'name' => $entity->getName(),
                'modified' => ($entity->getUpdatedAt())->format('Y-m-d H:i:s'),
                'created' => ($entity->getCreatedAt())->format('Y-m-d H:i:s')
            ];
        }, $this->tagService->getTags());
    }
}