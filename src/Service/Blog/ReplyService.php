<?php

declare(strict_types=1);

namespace App\Service\Blog;

use App\Entity\Blog\Comment;
use App\Entity\Blog\ReplyComment;
use App\Entity\User;
use App\Repository\Blog\ReplyCommentRepository;
use DateTime;

final class ReplyService
{
    public function __construct(private readonly ReplyCommentRepository $replyCommentRepository) {}

    public function getById(int $id): ?ReplyComment
    {
        return $this->replyCommentRepository->find($id);
    }

    /**
     * @return ReplyComment[]
     */
    public function getAllByComment(Comment $comment): array
    {
        return $this->replyCommentRepository->findBy([
            'comment' => $comment
        ], ['createdAt' => 'DESC']);
    }

    public function add(Comment $comment, User $user, string $replyText): ReplyComment
    {
        $model = new ReplyComment();

        $model
            ->setUser($user)
            ->setComment($comment)
            ->setReply($replyText)
            ->setUpdatedAt(new DateTime());

        $this->replyCommentRepository->save($model, true);

        return $model;
    }

    public function delete(ReplyComment $replyComment): void
    {
        $this->replyCommentRepository->remove($replyComment, true);
    }
}
