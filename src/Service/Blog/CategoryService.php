<?php

declare(strict_types=1);

namespace App\Service\Blog;

use App\Entity\Blog\Category;
use App\Repository\Blog\CategoryRepository;
use DateTime;

final class CategoryService
{
    public function __construct(private readonly CategoryRepository $categoryRepository) {}

    public function getById(int $id): ?Category
    {
        return $this->categoryRepository->find($id);
    }

    public function getOneByTitle(string $title): ?Category
    {
        return $this->categoryRepository->findOneBy(['title' => $title]);
    }

    /**
     * @return Category[]
     */
    public function getCategories(): array
    {
        return $this->categoryRepository->findBy([], ['id' => 'DESC']);

        // $categories = $this->categoryRepository->findAll();

        // $results = [];

        // foreach ($categories as $category) {
        //     foreach ($category->getArticles() as $article) {
        //         if (!$article->isDeleted()) {
        //             $results [] = $category;
        //         }
        //     }
        // }

        // return $results;
    }

    /**
     * @return Category[]
     */
    public function getByOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->categoryRepository->findByOffsetAndLimit($offset, $limit);
    }

    public function updateTitle(Category $category, string $title): Category
    {
        $category
            ->setTitle($title)
            ->setUpdatedAt(new DateTime());

        $this->categoryRepository->save($category, true);

        return $category;
    }

    public function add(string $title): Category
    {
        $entity = new Category();
        $entity
            ->setTitle($title)
            ->setUpdatedAt(new DateTime());

        $this->categoryRepository->save($entity, true);

        return $entity;
    }

    /**
     * @return Category[]
     */
    public function search(string $title): array
    {
        return $this->categoryRepository->search($title);
    }

    public function delete(Category $category): void
    {
        $this->categoryRepository->remove($category, true);
    }
}
