<?php

declare(strict_types=1);

namespace App\Service\Blog;

use App\Entity\Blog\SocialShare;
use App\Repository\Blog\SocialShareRepository;
use DateTime;

final class SocialShareService
{
    public function __construct(private readonly SocialShareRepository $socialShareRepository) {}

    public function getById(int $id): ?SocialShare
    {
        return $this->socialShareRepository->find($id);
    }

    /**
     * @return SocialShare[]
     */
    public function getAll(): array
    {
        return $this->socialShareRepository->findAll();
    }

    public function getPreparedSocialLinks(string $url, string $text): array
    {
        $results = [];

        foreach ($this->getAll() as $row) {
            $results[] = [
                'name' => $row->getName(),
                'icon' => $row->getIcon(),
                'color' => $row->getColor(),
                'url' => str_replace(['URL', 'TEXT'], [$url, $text], $row->getUrl()),
            ];
        }

        return $results;
    }

    public function add(string $name, string $icon, string $color, string $url): SocialShare
    {
        $model = new SocialShare();

        $model
            ->setName($name)
            ->setIcon($icon)
            ->setColor($color)
            ->setUrl($url)
            ->setUpdatedAt(new DateTime());

        $this->socialShareRepository->save($model, true);

        return $model;
    }

    public function update(SocialShare $socialLink, string $name, string $icon, string $color, string $url): void
    {
        $socialLink
            ->setName($name)
            ->setIcon($icon)
            ->setColor($color)
            ->setUrl($url)
            ->setUpdatedAt(new DateTime());

        $this->socialShareRepository->save($socialLink, true);
    }

    public function delete(SocialShare $SocialShare): void
    {
        $this->socialShareRepository->remove($SocialShare, true);
    }
}
