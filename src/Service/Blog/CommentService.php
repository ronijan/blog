<?php

declare(strict_types=1);

namespace App\Service\Blog;

use App\Entity\Blog\Article;
use App\Entity\Blog\Comment;
use App\Entity\User;
use App\Repository\Blog\CommentRepository;
use DateTime;
use Symfony\Component\Security\Core\User\UserInterface;

final class CommentService
{
    public function __construct(
        private readonly CommentRepository $commentRepository,
        private readonly ReplyService $replyService
    ) {
    }

    public function getById(int $id): ?Comment
    {
        return $this->commentRepository->find($id);
    }

    public function getOneByUserAndId(User $user, int $id): ?Comment
    {
        return $this->commentRepository->findOneBy([
            'user' => $user,
            'id' => $id
        ]);
    }

    /**
     * @return Comment[]
     */
    public function getAllByArticle(Article $article): array
    {
        return $this->commentRepository->findBy([
            'article' => $article
        ], ['createdAt' => 'DESC']);
    }

    /**
     * @return Comment[]
     */
    public function getComments(): array
    {
        return $this->commentRepository->findAll();
    }

    public function add(Article $article, UserInterface $user, string $comment): Comment
    {
        $model = new Comment();

        $model
            ->setArticle($article)
            ->setUser($user)
            ->setComment($comment)
            ->setUpdatedAt(new DateTime());

        $this->commentRepository->save($model, true);

        return $model;
    }

    public function delete(Comment $comment): void
    {
        foreach ($this->replyService->getAllByComment($comment) as $reply) {
            $this->replyService->delete($reply);
        }
        
        $this->commentRepository->remove($comment, true);
    }
}