<?php

declare(strict_types=1);

namespace App\Service\Account;

use App\Entity\Account\Profile;
use App\Entity\Account\ResetPassword;
use App\Entity\Account\TwoFactorAuth;
use App\Entity\Account\TwoFactorAuthDevices;
use App\Entity\Blog\Auther;
use App\Entity\LogActivities;
use App\Entity\Notification;
use App\Entity\User;
use App\Entity\UserSetting;
use App\Service\SystemLogsService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Persistence\ManagerRegistry;

final class DeleteUserPermanentlyService
{
    public function __construct(
        private readonly SystemLogsService $systemLogsService,
        private readonly EntityManagerInterface $manager,
        private readonly ManagerRegistry $registry,
    ) {
    }

    // Delete Notification, Auth, Auther, Profile, Auth Devices, Rest Password, Log Activities, User Setting and User
    public function cleanUp(UserInterface|User $user): bool
    {
        $notifications = $this->manager->getRepository(Notification::class)->findBy(['user' => $user]);

        if (!empty($notifications)) {
            foreach ($notifications as $notification) {
                $this->manager->remove($notification);
            }
        }

        $logActivities = $this->manager->getRepository(LogActivities::class)->findBy(['user' => $user]);

        if (!empty($logActivities)) {
            foreach ($logActivities as $logActivity) {
                $this->manager->remove($logActivity);
            }
        }

        $devices = $this->manager->getRepository(TwoFactorAuthDevices::class)->findBy(['user' => $user]);

        if (!empty($devices)) {
            foreach ($devices as $device) {
                $this->manager->remove($device);
            }
        }

        $authenticator = $this->manager->getRepository(TwoFactorAuth::class)->findOneBy(['user' => $user]);

        if ($authenticator) {
            $this->manager->remove($authenticator);
        }

        // No need to check Articles! The Articles will be transfered via job
        $auther = $this->manager->getRepository(Auther::class)->findOneBy(['user' => $user]);

        if ($auther) {
            $this->manager->remove($auther);
        }

        $profile = $this->manager->getRepository(Profile::class)->findOneBy(['user' => $user]);

        if ($profile) {
            $this->manager->remove($profile);
        }

        $resetPassword = $this->manager->getRepository(ResetPassword::class)->findOneBy(['user' => $user]);

        if (!empty($resetPassword)) {
            $this->manager->remove($resetPassword);
        }

        $userSetting = $this->manager->getRepository(UserSetting::class)->findBy(['user' => $user]);

        if (!empty($userSetting)) {
            $this->manager->remove($userSetting[0]);
        }

        try {
            $this->manager->remove($user);
            $this->manager->flush();
            $isDone = true;
        } catch (\Exception $e) {

            if (!$this->manager->isOpen()) {
                $this->registry->resetManager();

                $this->systemLogsService->add(
                    'Deleting User Permanentelly',
                    sprintf('%s', $e->getMessage())
                );
            }

            $isDone = false;
        }

        return $isDone;
    }
}