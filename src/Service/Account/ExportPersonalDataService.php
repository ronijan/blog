<?php

declare(strict_types=1);

namespace App\Service\Account;

use App\Entity\Account\TwoFactorAuthDevices;
use App\Entity\User;
use App\Service\LogActivitiesService;
use App\Service\NotificationService;
use App\Service\UserService;
use Symfony\Component\Security\Core\User\UserInterface;

final class ExportPersonalDataService
{
    public function __construct(
        private readonly UserService $userService,
        private readonly TwoFactorAuthService $twoFactorAuthService,
        private readonly NotificationService $notificationService,
        private readonly TwoFactorAuthDevicesService $twoFactorAuthDevicesService,
        private readonly LogActivitiesService $logActivitiesService
    ) {
    }

    public function exportDataAsJsonByUser(UserInterface|User $user): bool|string
    {
        $this->prepareJsonHeader($user);

        return json_encode($this->exportByUser($user), JSON_PRETTY_PRINT);
    }

    private function exportByUser(UserInterface|User $user): array
    {
        return [
            'User' => $this->getUserTableData($user),
            'Profile' => $this->getProfileTableData($user),
            'Setting' => $this->getUserSettingTableData($user),
            'Authentication' => $this->getTwoFactorAuthTableData($user),
            'Authenticated Devices' => $this->getTrustedDevicesTableData($user),
            'Notifications' => $this->getNotificationTableData($user),
            'Log Activities' => $this->getLogActivitiesTableData($user),
        ];
    }

    private function getUserTableData(UserInterface $user): array
    {
        $row = $this->userService->getById($user->getId());
        $result = [];

        if (null !== $row) {
            $result = [
                '01_User_Name' => $row->getUsername(),
                '02_Email_Address' => $row->getEmail(),
                '03_User_Role' => strtolower(str_replace('_', '-', $row->getRoles()[0])),
                '04_Is_Account_Verified' => $row->isVerified(),
                '05_Is_Account_Disabled' => $row->isDisabled(),
                '06_Is_Account_Deleted' => $row->isDeleted(),
                '07_Login_Counts' => $row->getLoginCounts(),
                '08_User_Locale' => $row->getLocale(),
                '09_Last_Logged_In' => ($row->getLastLoggedIn())->format('Y-m-d H:i:s'),
                '10_Last_Password_Updated' => ($row->getLastPasswordUpdated())->format('Y-m-d H:i:s'),
                '11_Account_Created_At' => ($row->getLastPasswordUpdated())->format('Y-m-d H:i:s'),
            ];
        }

        return $result;
    }

    private function getProfileTableData(UserInterface $user): array
    {
        $profile = $user->getProfile();
        $result = [];

        if (null !== $profile) {
            $result = [
                '01_Image_Name' => $profile->getName(),
                '02_Image_Size' => $profile->getSize(),
                '03_Image_Ext' => $profile->getExtension(),
                '04_Is_Image_Uploaded' => $profile->isUploaded(),
                '05_Datetime' => $profile->getCreatedAt()->format('Y-m-d H:i:s'),
            ];
        }

        return $result;
    }

    private function getUserSettingTableData(UserInterface $user): array
    {
        $userSetting = $user->getUserSetting();
        $result = [];

        if (null !== $userSetting) {
            $result = [
                '01_Log_Activities_Limit_Control' => $userSetting->getLogsLimit(),
                '02_Log_Activities_Sending_Log_Via_Email' => $userSetting->isLogSending(),
                '03_Pagination_Limit_Control' => $userSetting->getPaginationLimit(),
                '04_Delete_Notifications_Auto' => $userSetting->shouldAutoDeleteNotifications(),
                '05_Send_Email_By_New_Notification' => $userSetting->isNotificationSending(),
                '06_Datetime' => ($userSetting->getCreatedAt())->format('Y-m-d H:i:s'),
            ];
        }

        return $result;
    }

    private function getTwoFactorAuthTableData(UserInterface $user): array
    {
        $twoFactorAuth = $this->twoFactorAuthService->getByUser($user);
        $result = [];

        if (null !== $twoFactorAuth) {
            $result = [
                '01_Alternative_Email_For_OTP' => $twoFactorAuth->getAlternativeEmail(),
                '02_Is_2FA_Enabled' => $twoFactorAuth->isEnabled(),
                '03_Datetime' => ($twoFactorAuth->getCreatedAt())->format('Y-m-d H:i:s'),
            ];
        }

        return $result;
    }

    private function getTrustedDevicesTableData(UserInterface $user): array
    {
        $devices = $this->twoFactorAuthDevicesService->getAllByUser($user);

        if (empty($devices)) {
            return [];
        }

        return array_map(static function (TwoFactorAuthDevices $device) {
            return [
                '01_Data_Encrypted' => true,
                '02_Browser_Infos' => $device->getBrowser(),
                '03_OS_Info' => $device->getOperatingSystem(),
                '04_Datetime' => ($device->getCreatedAt())->format('Y-m-d H:i:s')
            ];
        }, $devices);
    }

    private function getNotificationTableData(UserInterface $user): array
    {
        $notifications = $this->notificationService->getAllByUser($user);
        $results = [];

        if (!empty($notifications)) {
            foreach ($notifications as $notification) {
                $results[] = [
                    '01_Notification_Subject' => $notification->getSubject(),
                    '02_Notification_Message' => $notification->getMessage(),
                    '03_Is_Notification_Seen' => $notification->isSeen(),
                    '04_Datetime' => ($notification->getCreatedAt())->format('Y-m-d H:i:s'),
                ];
            }
        }

        return $results;
    }

    private function getLogActivitiesTableData(UserInterface $user): array
    {
        $logActivities = $this->logActivitiesService->getAllByUser($user);
        $results = [];

        if (!empty($logActivities)) {
            foreach ($logActivities as $logActivity) {
                $results[] = [
                    '01_Log_Subject' => $logActivity->getAction(),
                    '02_Log_Message' => $logActivity->getDescription(),
                    '04_Datetime' => ($logActivity->getCreatedAt())->format('Y-m-d H:i:s'),
                ];
            }
        }

        return $results;
    }

    private function prepareJsonHeader(UserInterface|User $user): void
    {
        $file = ucfirst($user->getUsername()) . '_Personal_Data_' . date('Ymd_His') . '.json';

        header("Content-Type: application/json");
        header("Content-Disposition: attachment; filename=" . htmlspecialchars($file));
        header("Pragma: no-cache");
        header("Expires: 0");
    }
}