<?php

declare(strict_types=1);

namespace App\Service\Account;

use App\Entity\Account\TwoFactorAuthDevices;
use App\Repository\Account\TwoFactorAuthDevicesRepository;
use Symfony\Component\Security\Core\User\UserInterface;

final class TwoFactorAuthDevicesService
{
    public function __construct(
        private readonly TwoFactorAuthDevicesRepository $twoFactorAuthDevicesRepository
    ){}

    /**
     * @return TwoFactorAuthDevices[]
     */
    public function getAllByUser(UserInterface $user): array
    {
        return $this->twoFactorAuthDevicesRepository->findBy(['user' => $user]);;
    }

    public function add(UserInterface $user, string $browser, string $op): TwoFactorAuthDevices
    {
        $model = new TwoFactorAuthDevices();

        $model
            ->setUser($user)
            ->setBrowser($browser)
            ->setOperatingSystem($op);

        $this->twoFactorAuthDevicesRepository->add($model, true);

        return $model;
    }

    public function delete(UserInterface $user): bool
    {
        $devices = $this->getAllByUser($user);

        if ($devices) {
            foreach ($devices as $device) {
                $this->twoFactorAuthDevicesRepository->remove($device, true);
            }

            return true;
        }

        return false;
    }
}
