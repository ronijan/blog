<?php

declare(strict_types=1);

namespace App\Service\Account;

use App\Entity\Account\ResetPassword;
use App\Repository\Account\ResetPasswordRepository;
use DateTime;
use Symfony\Component\Security\Core\User\UserInterface;

final class ResetPasswordService
{
    public function __construct(private readonly ResetPasswordRepository $resetPasswordRepository) {}

    public function getByUser(UserInterface $user): ?ResetPassword
    {
        return $this->resetPasswordRepository->findOneBy(['user' => $user]);
    }

    public function getByToken(string $token): ?ResetPassword
    {
        return $this->resetPasswordRepository->findOneBy(['token' => $token]);
    }

    public function createOrUpdate(UserInterface $user, string $token): ?ResetPassword
    {
        if ($this->update($user, $token)) {
            return null;
        }

        $model = new ResetPassword();
        $model
            ->setUser($user)
            ->setToken($token)
            ->setRequestedAt(new DateTime())
            ->setExpiresAt(new DateTime());

        $this->resetPasswordRepository->add($model, true);

        return $model;
    }

    public function update(UserInterface $user, string $token): ?ResetPassword
    {
        $resetPassword = $this->getByUser($user);

        if ($resetPassword === null) {
            return null;
        }

        $resetPassword
            ->setToken($token)
            ->setRequestedAt(new DateTime())
            ->setExpiresAt(new DateTime());

        $this->resetPasswordRepository->add($resetPassword, true);

        return $resetPassword;
    }

    public function delete(ResetPassword $resetPassword): bool
    {
        $this->resetPasswordRepository->remove($resetPassword, true);

        return true;
    }
}
