<?php

declare(strict_types=1);

namespace App\Service\Account;

use App\Entity\User;
use App\Service\UserService;
use App\Service\UserSettingService;
use App\Traits\RandomTokenGeneratorTrait;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;

final class RegisterNewUserService
{
    use RandomTokenGeneratorTrait;

    public function __construct(
        private readonly UserPasswordHasher $userPasswordHash,
        private readonly UserService $userService,
        private readonly TwoFactorAuthService $twoFactorAuthService,
        private readonly ProfileService $profileService,
        private readonly UserSettingService $userSettingService
    ) {}

    public function setUp(
        string $username,
        string $email,
        string $plainPassword,
        ?string $token = null
    ): User {
        $user = new User();

        $hashPassword = $this->userPasswordHash->hashPassword($user, $plainPassword);

        $this->userService->create($user, $username, $email, $hashPassword, $token);
        $this->twoFactorAuthService->add($user);
        $this->profileService->add($user);
        $this->userSettingService->add($user);

        return $user;
    }
}