<?php

declare(strict_types=1);

namespace App\Service\Account;

use App\Entity\Account\TwoFactorAuth;
use App\Repository\Account\TwoFactorAuthRepository;
use DateTime;
use Symfony\Component\Security\Core\User\UserInterface;

final class TwoFactorAuthService
{
    public function __construct(private readonly TwoFactorAuthRepository $twoFactorAuthRepository)
    {
    }

    public function getByUser(UserInterface $user): ?TwoFactorAuth
    {
        return $this->twoFactorAuthRepository->findOneBy(['user' => $user]);
    }

    public function getByEmail(string $email): ?TwoFactorAuth
    {
        $twoFactorAuth = $this->twoFactorAuthRepository->findByEmail($email);

        return $twoFactorAuth ? $twoFactorAuth[0] : null;
    }

    public function getByUserAndIsEnabled(UserInterface $user, bool $isEnabled = true): ?TwoFactorAuth
    {
        return $this->twoFactorAuthRepository->findOneBy([
            'user' => $user,
            'isEnabled' => $isEnabled
        ]);
    }

    public function getByUserAndOtp(UserInterface $user, int $otp): ?TwoFactorAuth
    {
        return $this->twoFactorAuthRepository->findOneBy(['user' => $user, 'otp' => $otp]);
    }

    public function add(UserInterface $user): TwoFactorAuth
    {
        $model = new TwoFactorAuth();
        $model
            ->setUser($user)
            ->setUpdatedAt(new DateTime());

        $this->twoFactorAuthRepository->add($model, true);

        return $model;
    }

    public function updateAlternativeEmail(UserInterface $user, string $aEmail): ?TwoFactorAuth
    {
        $twoFactorAuth = $this->getByUser($user);

        if (null === $twoFactorAuth) {
            return null;
        }

        $twoFactorAuth
            ->setAlternativeEmail($aEmail)
            ->setUpdatedAt(new DateTime());

        $this->twoFactorAuthRepository->add($twoFactorAuth, true);

        return $twoFactorAuth;
    }

    public function updateAlternativeEmailAndIsEnabled(
        UserInterface $user,
        ?string $aEmail = null,
        bool $isEnabled = false
    ): ?TwoFactorAuth {
        $twoFactorAuth = $this->getByUser($user);

        if (null === $twoFactorAuth) {
            return null;
        }

        $twoFactorAuth
            ->setAlternativeEmail($aEmail)
            ->setIsEnabled($isEnabled)
            ->setUpdatedAt(new DateTime());

        $this->twoFactorAuthRepository->add($twoFactorAuth, true);

        return $twoFactorAuth;
    }

    public function updateOtp(TwoFactorAuth $auth, ?string $opt = null): TwoFactorAuth
    {
        $auth
            ->setOtp($opt)
            ->setUpdatedAt(new DateTime());

        $this->twoFactorAuthRepository->add($auth, true);

        return $auth;
    }

    public function delete(TwoFactorAuth $auth): void
    {
        $this->twoFactorAuthRepository->remove($auth, true);
    }
}