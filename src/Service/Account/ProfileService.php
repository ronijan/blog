<?php

declare(strict_types=1);

namespace App\Service\Account;

use App\Entity\Account\Profile;
use App\Helper\UserHelper;
use App\Repository\Account\ProfileRepository;
use DateTime;
use Symfony\Component\Security\Core\User\UserInterface;

final class ProfileService
{
    public function __construct(private readonly ProfileRepository $profileRepository) {}

    public function getByUser(UserInterface $user): ?Profile
    {
        return $this->profileRepository->findOneBy(['user' => $user]);
    }

    public function add(UserInterface $user): Profile
    {
        $entity = new Profile();
        $entity
            ->setUser($user)
            ->setName(UserHelper::DEFAULT_AVATAR)
            ->setUpdatedAt(new DateTime());

        $this->profileRepository->add($entity, true);

        return $entity;
    }

    public function update(Profile $entity, string $name, int $size, string $ext, bool $isUploaded = true): Profile
    {
        $entity
            ->setName($name)
            ->setSize($size)
            ->setExtension($ext)
            ->setIsUploaded($isUploaded)
            ->setUpdatedAt(new DateTime());

        $this->profileRepository->add($entity, true);

        return $entity;
    }

    public function updateName(Profile $entity, string $name): Profile
    {
        $entity
            ->setName($name)
            ->setUpdatedAt(new DateTime());

        $this->profileRepository->add($entity, true);

        return $entity;
    }
}
