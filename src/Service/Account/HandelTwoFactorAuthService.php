<?php

declare(strict_types=1);

namespace App\Service\Account;

use App\Entity\Account\TwoFactorAuth;
use App\Entity\Account\TwoFactorAuthDevices;
use App\Entity\User;
use App\Mails\Account\HandelTwoFactorAuthMail;
use App\Traits\RandomTokenGeneratorTrait;
use App\Traits\UserAgentTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class HandelTwoFactorAuthService
{
    use RandomTokenGeneratorTrait;
    use UserAgentTrait;

    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly EntityManagerInterface $entityManager,
        private readonly TwoFactorAuthService $twoFactorAuthService,
        private readonly TwoFactorAuthDevicesService $twoFactorAuthDevicesService,
        private readonly ContainerInterface $container,
        private readonly HandelTwoFactorAuthMail $handelTwoFactorAuthMail
    ) {}

    public function checkTwoStepVerification($user): bool
    {
        $auth = $this->getTwoFactorAuth($user);

        return $auth !== null && $auth->isEnabled() && !$this->isTrustedDeviceExist($user);
    }

    public function isUserExistByProvidedEmail(string $email): ?User
    {
        $auth = $this->entityManager->getRepository(TwoFactorAuth::class)->findByEmail($email);

        if (empty($auth) || ($auth[0]->getAlternativeEmail() !== $email && ($auth[0]->getUser())->getEmail() !== $email)) {
            return null;
        }

        return $auth[0]->getUser();
    }

    public function sendOtp(User|UserInterface $user): void
    {
        $auth = $this->getTwoFactorAuth($user);

        if ($auth !== null) {
            $otp = (string)$this->getOtp();
            $userEmail = $auth->getAlternativeEmail() ?? $user->getEmail();
            $this->twoFactorAuthService->updateOtp($auth, $otp);

            $this->handelTwoFactorAuthMail->send($user->getUsername(), $userEmail, $otp);
        }
    }

    public function handleTwoStepVerification(User|UserInterface $user): bool
    {
        if ($this->isTrustedDeviceExist($user)) {
            return false;
        }

        $auth = $this->getTwoFactorAuth($user);

        return ($auth !== null && $auth->isEnabled() && $auth->getOtp() !== null);
    }

    public function markDeviceAsTrustedOne(User $user): void
    {
        if (!$this->isTrustedDeviceExist($user)) {
            $this->twoFactorAuthDevicesService->add($user, $this->getBrowser(), $this->getOperatingSystem());
        }
    }

    private function isTrustedDeviceExist(User $user): bool
    {
        $devices = $this->entityManager->getRepository(TwoFactorAuthDevices::class)->findBy(['user' => $user]);

        foreach ($devices as $device) {
            if ($device->getBrowser() === $this->getBrowser() &&
                $device->getOperatingSystem() === $this->getOperatingSystem()) {
                return true;
            }
        }

        return false;
    }

    private function getTwoFactorAuth(User|UserInterface $user, $isEnabled = true): ?TwoFactorAuth
    {
        $auth = $this->entityManager->getRepository(TwoFactorAuth::class)->findBy([
            'user' => $user,
            'isEnabled' => $isEnabled,
        ]);

        return !empty($auth) ? $auth[0] : null;
    }
}