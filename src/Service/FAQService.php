<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\FrequentlyAskedQuestions;
use App\Repository\FrequentlyAskedQuestionsRepository;
use DateTime;

final class FAQService
{
    public function __construct(
        private readonly FrequentlyAskedQuestionsRepository $frequentlyAskedQuestionsRepository,
        private readonly ConfigService $configService,
    ) {
    }

    public function getById(int $id): ?FrequentlyAskedQuestions
    {
        return $this->frequentlyAskedQuestionsRepository->find($id);
    }

    /**
     * @return FrequentlyAskedQuestions[]
     */
    public function getAll(): array
    {
        $frequentlyAskedQuestions = [];

        if ($this->configService->getParameter('ALLOW_RENDER_FAQ') === 'on') {
            $frequentlyAskedQuestions = $this->frequentlyAskedQuestionsRepository->findAll();
        }

        return $frequentlyAskedQuestions;
    }

    public function create(string $title, string $description): FrequentlyAskedQuestions
    {
        $faq = new FrequentlyAskedQuestions();
        $faq
            ->setTitle($title)
            ->setDescription($description)
            ->setUpdatedAt(new DateTime());

        $this->frequentlyAskedQuestionsRepository->add($faq, true);

        return $faq;
    }

    public function update(FrequentlyAskedQuestions $faq, string $title, string $description): FrequentlyAskedQuestions
    {
        $faq
            ->setTitle($title)
            ->setDescription($description)
            ->setUpdatedAt(new DateTime());

        $this->frequentlyAskedQuestionsRepository->add($faq, true);

        return $faq;
    }

    public function delete(FrequentlyAskedQuestions $faq): void
    {
        $this->frequentlyAskedQuestionsRepository->remove($faq, true);
    }
}
