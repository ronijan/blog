<?php

declare(strict_types=1);

namespace App\Service\Admin\Job;

use App\Service\ContactFormService;
use App\Service\NotificationService;

final class DeleteContactsMarkedAsDeletedPermanentlyJob extends JobBaseAbstract implements JobInterface
{
    public function __construct(
        private readonly ContactFormService $contactFormService,
        private readonly NotificationService $notificationService,
    ) {
    }

    public function start(): ?int
    {
        $count = 0;

        $contacts = $this->contactFormService->getDeletedContacts();

        if ($contacts) {
            foreach ($contacts as $contact) {
                if ($contact->isDeleted()) {
                    $count++;
                    $this->contactFormService->delete($contact);
                }
            }

            if ($count > 0) {
                $this->notificationService->notifyAdmin(
                    'Delete Contacts Permanently Job',
                    sprintf(
                        'Job has been executed and [%s] contacts (marked as deleted) has been deleted permanently.',
                        $count
                    )
                );
            }
        }

        return $count;
    }
}