<?php

declare(strict_types=1);

namespace App\Service\Admin\Job;

use App\Service\ContactFormService;
use App\Service\NotificationService;

final class DeleteOldContactsBasedOnModifierJob extends JobBaseAbstract implements JobInterface
{
    public function __construct(
        private readonly ContactFormService $contactFormService,
        private readonly NotificationService $notificationService,
    ) {}

    public function start(): ?int
    {
        $count = 0;

        $contacts = $this->contactFormService->getDeleteableContacts();

        if ($contacts) {
            $count = count($contacts);

            foreach ($contacts as $contact) {
                $this->contactFormService->delete($contact);
            }

            if ($count > 0) {
                $this->notificationService->notifyAdmin(
                    'Delete Contacts Job',
                    sprintf(
                        'Job has been executed and [%s] contacts (marked as deleted) based on modifier has been deleted.', 
                        $count
                    )
                );
            }
        }

        return $count;
    }
}