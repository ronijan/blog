<?php

declare(strict_types=1);

namespace App\Service\Admin\Job;

use App\Service\ContactFormService;
use App\Service\NotificationService;

final class RemoveIpAddressFromMessagesJob extends JobBaseAbstract implements JobInterface
{
    public function __construct(
        private readonly ContactFormService $contactFormService,
        private readonly NotificationService $notificationService
    ) {
    }

    public function start(): ?int
    {
        $contacts = $this->contactFormService->getContacts();

        $count = 0;

        if ($contacts) {
            foreach ($contacts as $contact) {
                if ($contact->getIp()) {
                    $this->contactFormService->updateIpAddress($contact, '');
                    $count++;
                }
            }

            if ($count > 0) {
                $this->notificationService->notifyAdmin(
                    'IP From Messages Job',
                    sprintf(
                        'Job has been executed and [%s] IP addresses from contact form messages has been removed.',
                        $count
                    )
                );
            }
        }

        return $count;
    }
}