<?php

declare(strict_types=1);

namespace App\Service\Admin\Job;

use App\Service\NewsletterService;
use App\Service\NotificationService;

final class DeleteOldSubscribersBasedOnModifierJob extends JobBaseAbstract implements JobInterface
{
    public function __construct(
        private readonly NewsletterService $newsletterService,
        private readonly NotificationService $notificationService,
    ) {}

    public function start(): ?int
    {
        $count = 0;

        $subscribers = $this->newsletterService->getInactiveSubscribersBasedOnModifier();

        if ($subscribers) {
            $count = count($subscribers);

            foreach ($subscribers as $subscriber) {
                $this->newsletterService->delete($subscriber);
            }

            if ($count > 0) {
                $this->notificationService->notifyAdmin(
                    'Delete Subscripers Job',
                    sprintf(
                        'Job has been executed and [%s] subscripers from Newsletter has been deleted.', 
                        $count
                    )
                );
            }
        }

        return $count;
    }
}