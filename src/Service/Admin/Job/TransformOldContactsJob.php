<?php

declare(strict_types=1);

namespace App\Service\Admin\Job;

use App\Helper\DataTransformer;
use App\Service\ContactFormService;
use App\Service\FakerService;
use App\Service\NotificationService;

final class TransformOldContactsJob extends JobBaseAbstract implements JobInterface
{
    public function __construct(
        private readonly ContactFormService $contactFormService,
        private readonly NotificationService $notificationService,
        private readonly FakerService $fakerService
    ) {
    }

    public function start(): ?int
    {
        $countTransformedContacts = 0;

        $contacts = $this->contactFormService->getTransformableDeletedContacts();

        if ($contacts) {
            foreach ($contacts as $contact) {
                if ($contact->getName() !== DataTransformer::TRANSFORMED_USERNAME) {
                    $countTransformedContacts++;
                    $this->contactFormService->transformGDRRData($contact);
                }
            }

            if ($countTransformedContacts > 0) {
                $this->notificationService->notifyAdmin(
                    'Transform Contacts Job',
                    sprintf(
                        'Job has been executed and [%s] contacts (which marked as deleted) has been transformed.',
                        $countTransformedContacts
                    )
                );
            }
        }

        return $countTransformedContacts;
    }
}