<?php

declare(strict_types=1);

namespace App\Service\Admin\Job;

interface JobInterface
{
    public function start(): ?int;
}