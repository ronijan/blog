<?php

declare(strict_types=1);

namespace App\Service\Admin\Job;

use Ronijan\BackupDatabase\Backup;

final class FullBackupJob extends JobBaseAbstract implements JobInterface
{
    public function start(): ?int
    {
        // DATABASE_URL="mysql://username:password@host:port/db_name?serverVersion=13&charset=utf8mb4"

        if (isset($_ENV['DATABASE_URL'])) {
            $conn = mb_split(':', $_ENV['DATABASE_URL']);
            $temp = explode('@', $conn[2]);

            $host = $temp[1];
            $username = str_replace('//', '', $conn[1]);
            $password = $temp[0];

            $temp = explode('/', strtok($conn[3], '?'));
            $port = $temp[0];
            $dbName = $temp[1];

            $backup = new Backup($host, $username, $password, $dbName);
            $path = __DIR__ . "/../../../../storage/sql/";
            $backup->start($path);
        }

        return null;
    }
}