<?php

declare(strict_types=1);

namespace App\Service\Admin\Job;

use App\Helper\DataTransformer;
use App\Helper\UserHelper;
use App\Service\Account\ProfileService;
use App\Service\ConfigService;
use App\Service\FileHandlerService;
use App\Service\NotificationService;
use App\Service\UserService;

final class TransformDeletedAccountsJob extends JobBaseAbstract implements JobInterface
{
    public function __construct(
        private readonly UserService $userService,
        private readonly ProfileService $profileService,
        private readonly ConfigService $configService,
        private readonly FileHandlerService $fileHandler,
        private readonly NotificationService $notificationService
    ) {
    }

    public function start(): ?int
    {
        $countTransformedAccounts = 0;

        $users = $this->userService->getTransformableUserDeletedAccounts();

        if ($users) {
            $path = $this->configService->getParameter('avatar_dir');
            $fakeAvatar = DataTransformer::TRANSFORMED_AVATAR_NAME;

            foreach ($users as $user) {
                $defaultAvatar = UserHelper::DEFAULT_AVATAR;

                if ($user->getUsername() !== DataTransformer::TRANSFORMED_USERNAME) {
                    $countTransformedAccounts++;
                    $this->userService->transformUserDataGDRR($user);
                }

                $profile = $this->profileService->getByUser($user);

                if (
                    $profile &&
                    $profile->isUploaded() &&
                    $profile->getName() !== $fakeAvatar &&
                    $profile->getName() !== $defaultAvatar
                ) {
                    $this->fileHandler->unlinkFile($path, $profile->getName());
                }

                $this->profileService->updateName($profile, $fakeAvatar);
            }

            if ($countTransformedAccounts > 0) {
                $this->notificationService->notifyAdmin(
                    'Transform Accounts Job',
                    sprintf(
                        'Job has been executed and [%s] user accounts (marked as deleted) has been transformed.',
                        $countTransformedAccounts
                    )
                );
            }
        }

        return $countTransformedAccounts;
    }
}