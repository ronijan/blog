<?php

declare(strict_types=1);

namespace App\Service\Admin\Job;

use App\Service\NotificationService;
use App\Service\SystemLogsService;

final class RemoveIpAddressesFromArchivedLogsJob extends JobBaseAbstract implements JobInterface
{
    public function __construct(
        private readonly SystemLogsService $systemLogsService,
        private readonly NotificationService $notificationService,
    ) {
    }

    public function start(): ?int
    {
        $systemLogs = $this->systemLogsService->getAllArchived();

        $count = 0;

        if ($systemLogs) {
            foreach ($systemLogs as $systemLog) {
                if (null !== $systemLog->getIp()) {
                    $this->systemLogsService->updateIpAddress($systemLog);
                    $count++;
                }
            }

            if ($count > 0) {
                $this->notificationService->notifyAdmin(
                    'IP From Archived Logs Job',
                    sprintf(
                        'Job has been executed and [%s] IP addresses from archived logs has been removed.',
                        $count
                    )
                );
            }
        }

        return $count;
    }
}