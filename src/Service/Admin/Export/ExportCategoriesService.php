<?php

declare(strict_types=1);

namespace App\Service\Admin\Export;

use App\Entity\Blog\Category;
use App\Helper\ExportHelper;
use App\Service\Blog\CategoryService;

final class ExportCategoriesService extends ExporterAbstruct implements ExporterInterface
{
    public function __construct(
        private readonly CategoryService $categoryService,
    ) {
    }

    public function asJson(): bool|string
    {
        return $this->jsonEncode(ExportHelper::DEFAULT_BLOG_CATEGORIES_FILE_NAME, $this->data());
    }

    private function data(): array
    {
        return array_map(static function (Category $entity) {
            return [
                'title' => $entity->getTitle(),
                'articles_count' => $entity->getArticles()->count(),
                'modified' => ($entity->getUpdatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT),
                'created' => ($entity->getCreatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT)
            ];
        }, $this->categoryService->getCategories());
    }
}