<?php

declare(strict_types=1);

namespace App\Service\Admin\Export;

use App\Entity\Testimonial;
use App\Helper\ExportHelper;
use App\Service\TestimonialService;

final class ExportTestimonialService extends ExporterAbstruct implements ExporterInterface
{
    public function __construct(
        private readonly TestimonialService $testimonialService,
    ) {
    }

    public function asJson(): bool|string
    {
        return $this->jsonEncode(ExportHelper::DEFAULT_TESTIMONIALS_FILE_NAME, $this->data());
    }

    private function data(): array
    {
        return array_map(static function (Testimonial $entity) {
            return [
                'name' => $entity->getFirstName(),
                'last_name' => $entity->getLastName(),
                'email' => $entity->getEmail(),
                'position' => $entity->getPosition(),
                'avatar' => $entity->getAvatar(),
                'description' => $entity->getDescription(),
                'token' => $entity->getToken(),
                'is_published' => $entity->isPublished(),
                'is_deleted' => $entity->isDeleted(),
                'modified' => ($entity->getUpdatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT),
                'created' => ($entity->getCreatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT)
            ];
        }, $this->testimonialService->getAll());
    }
}