<?php

declare(strict_types=1);

namespace App\Service\Admin\Export;

use App\Entity\User;
use App\Helper\ExportHelper;
use App\Service\UserService;

final class ExportUserService extends ExporterAbstruct implements ExporterInterface
{
    public function __construct(
        private readonly UserService $userService,
    ) {
    }

    public function asJson(): bool|string
    {
        return $this->jsonEncode(ExportHelper::DEFAULT_USERS_FILE_NAME, $this->data());
    }

    // public function asCsv()

    private function data(): array
    {
        return array_map(static function (User $entity) {
            return [
                'username' => $entity->getUsername(),
                'email' => $entity->getEmail(),
                'role' => $entity->getRoles()[0],
                'login_count' => $entity->getLoginCounts(),
                'temp_email' => $entity->getTempEmail() ?? '',
                'token' => $entity->getToken() ?? '',
                'is_verified' => $entity->isVerified(),
                'is_writer' => $entity->isWriter(),
                'is_disabled' => $entity->isDisabled(),
                'is_deleted' => $entity->isDeleted(),
                'modified' => ($entity->getUpdatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT),
                'created' => ($entity->getCreatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT)
            ];
        }, $this->userService->getAll());
    }
}