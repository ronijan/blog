<?php

declare(strict_types=1);

namespace App\Service\Admin\Export;

use App\Entity\Reference;
use App\Helper\ExportHelper;
use App\Service\ReferenceService;

final class ExportReferencesService extends ExporterAbstruct implements ExporterInterface
{
    public function __construct(
        private readonly ReferenceService $referenceService,
    ) {
    }

    public function asJson(): bool|string
    {
        return $this->jsonEncode(ExportHelper::DEFAULT_REFERENCES_FILE_NAME, $this->data());
    }

    private function data(): array
    {
        return array_map(static function (Reference $entity) {
            return [
                'title' => $entity->getTitle() ?? '',
                'url' => $entity->getUrl() ?? '',
                'ref_image' => $entity->getRefImage(),
                'is_published' => $entity->isPublished(),
                'modified' => ($entity->getUpdatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT),
                'created' => ($entity->getCreatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT)
            ];
        }, $this->referenceService->getAll());
    }
}