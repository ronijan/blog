<?php

declare(strict_types=1);

namespace App\Service\Admin\Export;

use App\Entity\Newsletter;
use App\Helper\ExportHelper;
use App\Service\NewsletterService;

final class ExportSubscribersDataService extends ExporterAbstruct implements ExporterInterface
{
    public function __construct(
        private readonly NewsletterService $newsletterService,
    ) {
    }

    public function asJson(): bool|string
    {
        return $this->jsonEncode(ExportHelper::DEFAULT_NEWSLETTER_SUBSCRIBERS_FILE_NAME, $this->data());
    }

    public function emailsAsJson(): bool|string
    {
        return $this->jsonEncode(ExportHelper::DEFAULT_NEWSLETTER_SUBSCRIBERS_EMAILS_FILE_NAME, $this->emailsData());
    }

    private function data(): array
    {
        return array_map(static function (Newsletter $entity) {
            return [
                'name' => $entity->getName(),
                'email' => $entity->getEmail(),
                'token' => $entity->getToken(),
                'is_subscribed' => $entity->isSubscribed(),
                'modified' => ($entity->getUpdatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT),
                'created' => ($entity->getCreatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT)
            ];
        }, $this->newsletterService->getAll());
    }

    private function emailsData(): array
    {
        return array_map(static function (Newsletter $entity) {
            return [
                'email' => $entity->getEmail(),
            ];
        }, $this->newsletterService->getActiveSubscribers());
    }
}