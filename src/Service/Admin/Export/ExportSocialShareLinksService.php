<?php

declare(strict_types=1);

namespace App\Service\Admin\Export;

use App\Entity\Blog\SocialShare;
use App\Helper\ExportHelper;
use App\Service\Blog\SocialShareService;

final class ExportSocialShareLinksService extends ExporterAbstruct implements ExporterInterface
{
    public function __construct(
        private readonly SocialShareService $socialShareService,
    ) {
    }

    public function asJson(): string|bool
    {
        return $this->jsonEncode(ExportHelper::DEFAULT_BLOG_SOCIAL_SHARE_LINKS_FILE_NAME, $this->data());
    }

    private function data(): array
    {
        return array_map(static function (SocialShare $entity) {
            return [
                'name' => $entity->getName(),
                'icon' => $entity->getIcon(),
                'color' => $entity->getColor(),
                'url' => $entity->getUrl(),
                'modified' => ($entity->getUpdatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT),
                'created' => ($entity->getCreatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT)
            ];
        }, $this->socialShareService->getAll());
    }
}