<?php

declare(strict_types=1);

namespace App\Service\Admin\Export;

use App\Entity\ContactForm;
use App\Helper\ExportHelper;
use App\Service\ContactFormService;

final class ExportContactFormService extends ExporterAbstruct implements ExporterInterface
{
    public function __construct(
        private readonly ContactFormService $contactFormService,
    ) {
    }

    public function asJson(): bool|string
    {
        return $this->jsonEncode(ExportHelper::DEFAULT_CONTACT_FORM_FILE_NAME, $this->data());
    }

    public function onlyArchivedAsJson(): bool|string
    {
        return $this->jsonEncode(ExportHelper::DEFAULT_ARCHIVED_CONTACT_FORM_FILE_NAME, $this->archivedData());
    }

    private function data(): array
    {
        return array_map(function (ContactForm $entity) {
            return $this->row($entity);
        }, $this->contactFormService->getContacts());
    }

    private function archivedData(): array
    {
        return array_map(function (ContactForm $entity) {
            return $this->row($entity);
        }, $this->contactFormService->getArchivedContacts());
    }

    private function row(ContactForm $entity): array
    {
        return [
            'name' => $entity->getName(),
            'email' => $entity->getEmail(),
            'subject' => $entity->getSubject(),
            'message' => $entity->getMessage(),
            'ip_adress' => $entity->getIp(),
            'is_copy_sent' => $entity->isCopySent(),
            'is_archived' => $entity->isArchived(),
            'is_deleted' => $entity->isDeleted(),
            'modified' => ($entity->getUpdatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT),
            'created' => ($entity->getCreatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT)
        ];
    }
}