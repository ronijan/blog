<?php

declare(strict_types=1);

namespace App\Service\Admin\Export;

use App\Entity\Team;
use App\Helper\ExportHelper;
use App\Service\TeamService;

final class ExportTeamMemberService extends ExporterAbstruct implements ExporterInterface
{
    public function __construct(
        private readonly TeamService $teamService,
    ) {
    }

    public function asJson(): bool|string
    {
        return $this->jsonEncode(ExportHelper::DEFAULT_TEAMS_MEMBERS_FILE_NAME, $this->data());
    }

    private function data(): array
    {
        return array_map(static function (Team $entity) {
            return [
                'name' => $entity->getFirstName(),
                'last_name' => $entity->getLastName(),
                'email' => $entity->getEmail() ?? '',
                'Phone' => $entity->getPhone() ?? '',
                'position' => $entity->getPosition(),
                'avatar' => $entity->getImage(),
                'is_active' => $entity->isIsActive(),
                'modified' => ($entity->getUpdatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT),
                'created' => ($entity->getCreatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT)
            ];
        }, $this->teamService->getAll());
    }
}