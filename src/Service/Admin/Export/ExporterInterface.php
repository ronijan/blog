<?php

declare(strict_types=1);

namespace App\Service\Admin\Export;

interface ExporterInterface
{
    public function asJson(): string|bool;
}