<?php

declare(strict_types=1);

namespace App\Service\Admin\Export;

use App\Entity\SystemLogs;
use App\Helper\ExportHelper;
use App\Service\SystemLogsService;

final class ExportSystemLogsService extends ExporterAbstruct implements ExporterInterface
{
    public function __construct(
        private readonly SystemLogsService $systemLogsService,
    ) {
    }

    public function asJson(): bool|string
    {
        return $this->jsonEncode(ExportHelper::DEFAULT_ARCHIVED_SYSTEM_LOGS_FILE_NAME, $this->data());
    }

    private function data(): array
    {
        return array_map(static function (SystemLogs $log) {
            return [
                'event' => $log->getEvent(),
                'message' => $log->getMessage(),
                'is_archived' => $log->isArchived(),
                'ip_adress' => $log->getIp(),
                'modified' => ($log->getUpdatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT),
                'created' => ($log->getCreatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT)
            ];
        }, $this->systemLogsService->getAllArchived());
    }
}