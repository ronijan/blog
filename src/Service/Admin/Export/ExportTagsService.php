<?php

declare(strict_types=1);

namespace App\Service\Admin\Export;

use App\Entity\Blog\Tag;
use App\Helper\ExportHelper;
use App\Service\Blog\TagService;

final class ExportTagsService extends ExporterAbstruct implements ExporterInterface
{
    public function __construct(
        private readonly TagService $tagService,
    ) {
    }

    public function asJson(): string|bool
    {
        return $this->jsonEncode(ExportHelper::DEFAULT_BLOG_TAGS_FILE_NAME, $this->data());
    }

    private function data(): array
    {
        return array_map(static function (Tag $entity) {
            return [
                'name' => $entity->getName(),
                'modified' => ($entity->getUpdatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT),
                'created' => ($entity->getCreatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT)
            ];
        }, $this->tagService->getTags());
    }
}