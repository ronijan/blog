<?php

declare(strict_types=1);

namespace App\Service\Admin\Export;

use App\Entity\FrequentlyAskedQuestions;
use App\Helper\ExportHelper;
use App\Service\FAQService;

final class ExportFAQService extends ExporterAbstruct implements ExporterInterface
{
    public function __construct(
        private readonly FAQService $faqService,
    ) {
    }

    public function asJson(): bool|string
    {
        return $this->jsonEncode(ExportHelper::DEFAULT_FAQ_FILE_NAME, $this->data());
    }

    private function data(): array
    {
        return array_map(static function (FrequentlyAskedQuestions $entity) {
            return [
                'title' => $entity->getTitle(),
                'description' => $entity->getDescription(),
                'modified' => ($entity->getUpdatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT),
                'created' => ($entity->getCreatedAt())->format(ExportHelper::DEFAULT_DATETIME_FORMAT)
            ];
        }, $this->faqService->getAll());
    }
}