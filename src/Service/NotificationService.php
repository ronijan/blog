<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Notification;
use App\Entity\User;
use App\Helper\Job;
use App\Helper\UserHelper;
use App\Repository\NotificationRepository;
use DateTime;
use Symfony\Component\Security\Core\User\UserInterface;

final class NotificationService
{
    public function __construct(
        private readonly NotificationRepository $notificationRepository,
        private readonly UserService $userService,
    ) {
    }

    public function getById(int $id): ?Notification
    {
        return $this->notificationRepository->find($id);
    }

    /**
     * @return Notification[]
     */
    public function getByUser(UserInterface $user): array
    {
        return $this->notificationRepository->findBy(['user' => $user]);
    }

    public function getByUserAndId(UserInterface $user, int $id): ?Notification
    {
        return $this->notificationRepository->findOneBy(['user' => $user, 'id' => $id]);
    }

    /**
     * @return Notification[]
     */
    public function getAllByUser(UserInterface $user): array
    {
        return $this->notificationRepository->findAllByUser($user);
    }

    /**
     * @return Notification[]
     */
    public function getAllUnseenByUser(UserInterface $user): array
    {
        return $this->notificationRepository->findAllUnseenByUser($user);
    }

    /**
     * @return Notification[]
     */
    public function getUnseenNotifications(): array
    {
        return $this->notificationRepository->findUnseenNotifications();
    }

    /**
     * @return Notification[]
     */
    public function getNotifications(): array
    {
        return $this->notificationRepository->findNotifications();
    }

    /**
     * @return Notification[]
     */
    public function getNotificationsWithOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->notificationRepository->findNotificationsWithOffsetAndLimit($offset, $limit);
    }

    /**
     * @return Notification[]
     */
    public function getAllByUserWithOffsetAndLimit(UserInterface $user, int $offset, int $limit): array
    {
        return $this->notificationRepository->findAllByUserWithOffsetAndLimit($user, $offset, $limit);
    }

    /**
     * @return Notification[]
     */
    public function getAllByMessage(string $message): array
    {
        return $this->notificationRepository->findAllByMessage($message);
    }

    public function create(UserInterface|User $user, string $subject, string $message): Notification
    {
        $entity = new Notification();
        $entity
            ->setUser($user)
            ->setSubject($subject)
            ->setMessage($message)
            ->setUpdatedAt(new DateTime());

        $this->notificationRepository->add($entity, true);

        return $entity;
    }

    /**
     * @param Notification[] $notifications
     */
    public function updateIsSeen(array $notifications, bool $isSeen = true): void
    {
        if (count($notifications) > 0) {
            foreach ($notifications as $notification) {
                $notification
                    ->setIsSeen($isSeen)
                    ->setUpdatedAt(new DateTime());

                $this->notificationRepository->add($notification, true);
            }
        }
    }

    public function notifyAdmin(string $subject, string $message): void
    {
        $users = $this->userService->getAll(); // @todo getByRole maybe?!

        foreach ($users as $user) {
            if (in_array(UserHelper::ROLE_ADMIN, $user->getRoles(), true)) {
                $this->create($user, $subject, $message);
                break; // not all Admins; just the first one
            }
        }
    }

    public function deleteAllOlderThanSixMonthsByUser(UserInterface $user): void
    {
        $modifier = (new DateTime())->modify(Job::DELETE_OLD_NOTIFICATIONS_MODIFIER)->format('Y-m-d H:i:s');

        $notifications = $this->notificationRepository->deleteAllOlderThanSixMonthsByUserAndModifier($user, $modifier);

        if ($notifications) {
            foreach ($notifications as $notification) {
                $this->delete($notification);
            }
        }
    }

    public function delete(Notification $notification): void
    {
        $this->notificationRepository->remove($notification, true);
    }

    /**
     * @return Notification[]
     */
    public function search(string $keyword): array
    {
        return $this->notificationRepository->search($keyword);
    }
}
