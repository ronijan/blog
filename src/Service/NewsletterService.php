<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Newsletter;
use App\Helper\Job;
use App\Repository\NewsletterRepository;
use App\Traits\NameFromEmailAddressTrait;
use App\Traits\RandomTokenGeneratorTrait;
use DateTime;

final class NewsletterService
{
    use RandomTokenGeneratorTrait;
    use NameFromEmailAddressTrait;

    public function __construct(private readonly NewsletterRepository $newsletterRepository)
    {
    }

    public function getById(int $id): ?Newsletter
    {
        return $this->newsletterRepository->find($id);
    }

    public function getByEmail(string $email): ?Newsletter
    {
        return $this->newsletterRepository->findOneBy(['email' => $email]);
    }

    public function getByToken(string $token): ?Newsletter
    {
        return $this->newsletterRepository->findOneBy(['token' => $token]);
    }

    /**
     * @return Newsletter[]
     */
    public function getAll(): array
    {
        return $this->newsletterRepository->findAll();
    }

    /**
     * @return Newsletter[]
     */
    public function getActiveSubscribers(): array
    {
        return $this->newsletterRepository->findBy(['isSubscribed' => 1]);
    }

    /**
     * @return Newsletter[]
     */
    public function getUnActiveSubscribers(): array
    {
        return $this->newsletterRepository->findBy(['isSubscribed' => 0]);
    }

    /**
     * @return Newsletter[]
     */
    public function getAllWithOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->newsletterRepository->findAllWithOffsetAndLimit($offset, $limit);
    }

    public function addNewSubscriber(string $email): ?Newsletter
    {
        if ($this->getByEmail($email)) {
            return null;
        }

        $entity = new Newsletter();
        $entity
            ->setName($this->getNameFromEmailAddress($email))
            ->setEmail($email)
            ->setToken($this->getRandomToken(32))
            ->setUpdatedAt(new DateTime());

        $this->newsletterRepository->add($entity, true);

        return $entity;
    }

    public function updateUserSubscription(string $email): bool
    {
        $subscriber = $this->getByEmail($email);

        if ($subscriber) {
            $token = $this->getRandomToken(32);
            $this->updateToken($subscriber, $token);

            return $this->updateIsDeleted($subscriber, true) !== null;
        }

        return false;
    }

    public function unSubscribeUser(Newsletter $subscriber): void
    {
        $this->updateIsDeleted($subscriber, false);
        $this->updateToken($subscriber, '');
    }

    public function updateIsDeleted(Newsletter $subscriber, bool $isSubscribed): Newsletter
    {
        $subscriber
            ->setIsSubscribed($isSubscribed)
            ->setUpdatedAt(new DateTime());

        $this->newsletterRepository->add($subscriber, true);

        return $subscriber;
    }

    public function updateToken(Newsletter $subscriber, string $token): Newsletter
    {
        $subscriber
            ->setToken($token)
            ->setUpdatedAt(new DateTime());

        $this->newsletterRepository->add($subscriber, true);

        return $subscriber;
    }

    public function update(Newsletter $subscriber, string $name, string $email, string $token, bool $isSubscribed): Newsletter
    {
        $subscriber
            ->setName($name)
            ->setEmail($email)
            ->setToken($token)
            ->setIsSubscribed($isSubscribed)
            ->setUpdatedAt(new DateTime());

        $this->newsletterRepository->add($subscriber, true);

        return $subscriber;
    }

    public function delete(Newsletter $subscriber): void
    {
        $this->newsletterRepository->remove($subscriber, true);
    }

    /**
     * @return Newsletter[]
     */
    public function getInactiveSubscribersBasedOnModifier(): array
    {
        $modifier = (new DateTime())->modify(Job::DELETE_INACTIVE_SUBSCRIBER_MODIFIER)->format('Y-m-d H:i:s');

        return $this->newsletterRepository->findInactiveSubscribersBasedOnModifier($modifier);
    }

    /**
     * @return Newsletter[]
     */
    public function search(string $keyword): array
    {
        return $this->newsletterRepository->search($keyword);
    }
}