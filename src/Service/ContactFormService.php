<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\ContactForm;
use App\Helper\Job;
use App\Repository\ContactFormRepository;
use App\Traits\RemoteTrait;
use DateTime;

final class ContactFormService
{
    use RemoteTrait;

    public function __construct(
        private readonly ContactFormRepository $contactFormRepository,
        private readonly SystemLogsService $systemLogsService,
        private readonly FakerService $fakerService
    ) {
    }

    public function getById(int $id): ?ContactForm
    {
        return $this->contactFormRepository->find($id);
    }

    /**
     * @return ContactForm[]
     */
    public function getContacts(): array
    {
        return $this->contactFormRepository->findContacts();
    }

    /**
     * @return ContactForm[]
     */
    public function getContactsWithOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->contactFormRepository->findContactsWithOffsetAndLimit($offset, $limit);
    }

    /**
     * @return ContactForm[]
     */
    public function getArchivedContacts(): array
    {
        return $this->contactFormRepository->findArchivedContacts();
    }

    /**
     * @return ContactForm[]
     */
    public function getUnArchivedContacts(): array
    {
        return $this->contactFormRepository->findBy(['isArchived' => 0, 'isDeleted' => 0]);
    }


    /**
     * @return ContactForm[]
     */
    public function getArchivedContactsWithOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->contactFormRepository->findArchivedContactsWithOffsetAndLimit($offset, $limit);
    }

    /**
     * @return ContactForm[]
     */
    public function getDeletedContacts(): array
    {
        return $this->contactFormRepository->findDeletedContacts();
    }

    /**
     * @return ContactForm[]
     */
    public function getDeletedContactsWithOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->contactFormRepository->findDeletedContactsWithOffsetAndLimit($offset, $limit);
    }

    public function create(...$contents): ContactForm
    {
        [$name, $email, $subject, $message, $isSent] = $contents;

        $entity = new ContactForm();
        $entity
            ->setName($name)
            ->setEmail($email)
            ->setSubject($subject)
            ->setMessage($message)
            ->setIsCopySent($isSent)
            ->setIp($this->getIP(true))
            ->setUpdatedAt(new DateTime());

        $this->contactFormRepository->add($entity, true);

        return $entity;
    }

    public function updateIsArchived(ContactForm $contactForm, bool $isArchived = true): ContactForm
    {
        $contactForm
            ->setIsArchived($isArchived)
            ->setUpdatedAt(new DateTime());

        $this->contactFormRepository->add($contactForm, true);

        return $contactForm;
    }

    public function updateIsDeleted(ContactForm $contactForm, bool $isDeleted = true): ContactForm
    {
        $contactForm
            ->setIsDeleted($isDeleted)
            ->setUpdatedAt(new DateTime());

        $this->contactFormRepository->add($contactForm, true);

        return $contactForm;
    }

    public function transformGDRRData(ContactForm $contact): ContactForm
    {
        $transformedName = $this->fakerService->getName();

        $transformedEmail = $this->fakerService->getRandomEmail($contact->getId());

        $this->systemLogsService->add(
            'Contact Data Transformed',
            sprintf('%s (%s) to (%s)', $contact->getName(), $contact->getEmail(), $transformedEmail)
        );

        $contact
            ->setName($transformedName)
            ->setEmail($transformedEmail)
            ->setUpdatedAt(new DateTime());

        $this->contactFormRepository->add($contact, true);

        return $contact;
    }

    public function updateIpAddress(ContactForm $entity, string $ip): void
    {
        $entity
            ->setIp($ip)
            ->setUpdatedAt(new DateTime());

        $this->contactFormRepository->add($entity, true);
    }

    public function update(ContactForm $entity, ...$contents): ContactForm
    {
        [$name, $email, $subject, $message, $ip, $isSent, $isArchived, $isDeleted, $updated, $created] = $contents;

        $entity
            ->setName($name)
            ->setEmail($email)
            ->setSubject($subject)
            ->setMessage($message)
            ->setIp($ip)
            ->setIsCopySent($isSent)
            ->setIsArchived($isArchived)
            ->setIsDeleted($isDeleted)
            ->setUpdatedAt(new DateTime($updated))
            ->setCreatedAt(new DateTime($created));

        $this->contactFormRepository->add($entity, true);

        return $entity;
    }

    /**
     * @return ContactForm[]
     */
    public function getDeleteableContacts(): array
    {
        $modifier = (new DateTime())->modify(Job::DELETE_OLD_CONTACT_FORM_MESSAGES_MODIFIER)->format('Y-m-d H:i:s');

        return $this->contactFormRepository->findAllDeletedByModifier($modifier);
    }

    /**
     * @return ContactForm[]
     */
    public function getTransformableDeletedContacts(): array
    {
        $modifier = (new DateTime())->modify(Job::CONTACT_FORM_MESSAGES_TRANSFORM_MODIFIER)->format('Y-m-d H:i:s');

        return $this->contactFormRepository->findAllDeletedByModifier($modifier);
    }

    public function delete(ContactForm $entity): void
    {
        $this->contactFormRepository->remove($entity, true);
    }

    /**
     * @return ContactForm[]
     */
    public function search(string $keyword): array
    {
        return $this->contactFormRepository->search($keyword);
    }
}