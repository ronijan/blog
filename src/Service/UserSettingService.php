<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Entity\UserSetting;
use App\Helper\UserSettingConfig;
use App\Repository\UserSettingRepository;
use DateTime;
use Symfony\Component\Security\Core\User\UserInterface;

final class UserSettingService
{
    public function __construct(private readonly UserSettingRepository $userSettingRepository) {}

    public function getById(int $id): ?UserSetting
    {
        return $this->userSettingRepository->find($id);
    }

    public function getByUser(UserInterface $user): ?UserSetting
    {
        return $this->userSettingRepository->findOneBy(['user' => $user]);
    }

    /**
     * @return UserSetting[]
     */
    public function getAll(): array
    {
        return $this->userSettingRepository->findAll();
    }

    /**
     * @return UserSetting[]
     */
    public function getAllWithOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->userSettingRepository->findAllWithOffsetAndLimit($offset, $limit);
    }

    public function add(UserInterface|User $user): UserSetting
    {
        $entity = new UserSetting();
        $entity
            ->setUser($user)
            ->setPaginationLimit(UserSettingConfig::DEFAULT_PAGINATION_LIMIT)
            ->setUpdatedAt(new DateTime());

        $this->userSettingRepository->add($entity, true);

        return $entity;
    }

    public function getPaginationLimit(UserInterface $user): int
    {
        $defaultPaginationLimit = UserSettingConfig::DEFAULT_PAGINATION_LIMIT;
        $userSetting = $this->getByUser($user);

        return null !== $userSetting ? $userSetting->getPaginationLimit() : $defaultPaginationLimit;
    }

    public function getLogActivitiesLimitByUser(UserInterface $user): int
    {
        $defaultLogLimit = UserSettingConfig::DEFAULT_LOGS_LIMIT;
        $userSetting = $this->getByUser($user);

        return null !== $userSetting ? $userSetting->getLogsLimit() : $defaultLogLimit;
    }

    public function updatePaginationLimit(UserInterface $user, int $limit): void
    {
        $userSetting = $this->getByUser($user);

        if (!in_array($limit, UserSettingConfig::PAGINATION, true)) {
            $limit = UserSettingConfig::DEFAULT_PAGINATION_LIMIT;
        }

        if ($userSetting) {
            $userSetting
                ->setPaginationLimit($limit)
                ->setUpdatedAt(new DateTime());

            $this->userSettingRepository->add($userSetting, true);
        }
    }

    public function updateLogsLimit(UserInterface $user, int $limit): void
    {
        $userSetting = $this->getByUser($user);

        if (!in_array($limit, UserSettingConfig::LOGS, true)) {
            $limit = UserSettingConfig::DEFAULT_LOGS_LIMIT;
        }

        if ($userSetting) {
            $userSetting
                ->setLogsLimit($limit)
                ->setUpdatedAt(new DateTime());

            $this->userSettingRepository->add($userSetting, true);
        }
    }

    public function updateLogSending(UserInterface $user, bool $shouldLogBeSent): void
    {
        $userSetting = $this->getByUser($user);

        if ($userSetting) {
            $userSetting
                ->setLogSending($shouldLogBeSent)
                ->setUpdatedAt(new DateTime());

            $this->userSettingRepository->add($userSetting, true);
        }
    }

    public function updateAutoDeleteNotifications(UserInterface $user, bool $autoDeleteNotifications): void
    {
        $userSetting = $this->getByUser($user);

        if ($userSetting) {
            $userSetting
                ->setAutoDeleteNotifications($autoDeleteNotifications)
                ->setUpdatedAt(new DateTime());

            $this->userSettingRepository->add($userSetting, true);
        }
    }

    public function updateNotificationSending(UserInterface $user, bool $notifyViaEmail): void
    {
        $userSetting = $this->getByUser($user);

        if ($userSetting) {
            $userSetting
                ->setNotificationSending($notifyViaEmail)
                ->setUpdatedAt(new DateTime());
                
            $this->userSettingRepository->add($userSetting, true);
        }
    }

    public function isAutoDeleteNotificationsEnabled(UserInterface $user): bool
    {
        $userSetting = $this->getByUser($user);

        return $userSetting && $userSetting->shouldAutoDeleteNotifications();
    }

    public function isActiveSendNotification(UserInterface $user): bool
    {
        $userSetting = $this->getByUser($user);

        return $userSetting && $userSetting->isNotificationSending();
    }

    public function update(
        UserInterface $user,
        int $paginationLimit,
        int $logsLimit,
        bool $sendLog,
        bool $autoDeleteNotifications,
        bool $informUserNewNotificationArrived
    ): void {
        if (!in_array($paginationLimit, UserSettingConfig::PAGINATION, true)) {
            $paginationLimit = UserSettingConfig::DEFAULT_PAGINATION_LIMIT;
        }

        if (!in_array($logsLimit, UserSettingConfig::LOGS, true)) {
            $logsLimit = UserSettingConfig::DEFAULT_LOGS_LIMIT;
        }

        $userSetting = $this->getByUser($user);

        if ($userSetting) {
            $userSetting
                ->setPaginationLimit($paginationLimit)
                ->setLogsLimit($logsLimit)
                ->setLogSending($sendLog)
                ->setAutoDeleteNotifications($autoDeleteNotifications)
                ->setNotificationSending($informUserNewNotificationArrived)
                ->setUpdatedAt(new DateTime());

            $this->userSettingRepository->add($userSetting, true);
        }
    }

    /**
     * @return UserSetting[]
     */
    public function search(string $keyword): array
    {
        return $this->userSettingRepository->search($keyword);
    }
}
