<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\SystemLogs;
use App\Repository\SystemLogsRepository;
use App\Traits\RemoteTrait;
use DateTime;

final class SystemLogsService
{
    use RemoteTrait;

    public function __construct(private readonly SystemLogsRepository $systemLogsRepository) {}

    public function getById(int $id): ?SystemLogs
    {
        return $this->systemLogsRepository->find($id);
    }

    /**
     * @return SystemLogs[]
     */
    public function getAllButNotArchived(): array
    {
        return $this->systemLogsRepository->findAllButNotArchived();
    }

    /**
     * @return SystemLogs[]
     */
    public function getAllArchived(): array
    {
        return $this->systemLogsRepository->findAllArchived();
    }

    /**
     * @return SystemLogs[]
     */
    public function getAllArchivedByOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->systemLogsRepository->findAllArchivedByOffsetAndLimit($offset, $limit);
    }

    /**
     * @return SystemLogs[]
     */
    public function getAllButNotArchivedByOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->systemLogsRepository->findAllButNotArchivedByOffsetAndLimit($offset, $limit);
    }

    public function add(string $event, string $message): SystemLogs
    {
        $entity = new SystemLogs();
        $entity
            ->setEvent($event)
            ->setMessage($message)
            ->setIp($this->getIP(true))
            ->setUpdatedAt(new DateTime());

        $this->systemLogsRepository->add($entity, true);

        return $entity;
    }

    public function updateIsArchived(int $id, bool $isArchived): void
    {
        $systemLog = $this->getById($id);

        if ($systemLog) {
            $systemLog
                ->setIsArchived($isArchived)
                ->setUpdatedAt(new DateTime());

            $this->systemLogsRepository->add($systemLog, true);
        }
    }

    public function updateIpAddress(SystemLogs $systemLog, ?string $ip = null): SystemLogs
    {
        $systemLog
            ->setIp($ip)
            ->setUpdatedAt(new DateTime());

        $this->systemLogsRepository->add($systemLog, true);

        return $systemLog;
    }

    public function delete(SystemLogs $systemLog): void
    {
        $this->systemLogsRepository->remove($systemLog, true);
    }
}
