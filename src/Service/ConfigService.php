<?php

declare(strict_types=1);

namespace App\Service;

use App\Exception\InvalidConfigArgumentException;
use App\Helper\Logger;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class ConfigService
{
    public function __construct(
        private readonly ContainerInterface $container,
        private readonly SystemLogsService $systemLogsService
    ) {
    }

    /**
     * @throws InvalidConfigArgumentException
     */
    public function getParameter(string $key): string
    {
        try {
            return $this->container->getParameter($key);
        } catch (Exception $e) {
            Logger::addLog($e->getTraceAsString(), 'critical');
            $this->systemLogsService->add('Critical', $e->getMessage());
            throw new InvalidConfigArgumentException(sprintf('Key "%s" was not defined in Config.', $key));
        }
    }
}
