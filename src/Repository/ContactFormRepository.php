<?php

namespace App\Repository;

use App\Entity\ContactForm;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ContactForm>
 *
 * @method ContactForm|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactForm|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactForm[]    findAll()
 * @method ContactForm[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactFormRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContactForm::class);
    }

    public function add(ContactForm $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ContactForm $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return ContactForm[]
     */
    public function findContacts(): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isDeleted = 0')
            ->andWhere('t1.isArchived = 0')
            ->orderBy('t1.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return ContactForm[]
     */
    public function findContactsWithOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isDeleted = 0')
            ->andWhere('t1.isArchived = 0')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('t1.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return ContactForm[]
     */
    public function findDeletedContacts(): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isDeleted = 1')
            ->andWhere('t1.isArchived = 0')
            ->orderBy('t1.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return ContactForm[]
     */
    public function findDeletedContactsWithOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isDeleted = 1')
            ->andWhere('t1.isArchived = 0')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('t1.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return ContactForm[]
     */
    public function findArchivedContacts(): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isArchived = 1')
            ->orderBy('t1.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return ContactForm[]
     */
    public function findArchivedContactsWithOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isArchived = 1')
            ->orderBy('t1.createdAt', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return ContactForm[]
     */
    public function findAllDeletedByModifier(string $modifier): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isDeleted = 1')
            ->andWhere('t1.updatedAt <= :modifier')
            ->setParameter('modifier', $modifier)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return ContactForm[]
     */
    public function search(string $keyword): array
    {
        $text = '%' . $keyword . '%';
        $qb = $this->createQueryBuilder('t1');
        $qb
            ->where($qb->expr()->like('t1.email', ':email'))
            ->setParameter('email', $text);

        return $qb->orWhere($qb->expr()->like('t1.name', ':name'))
            ->setParameter('name', $text)
            ->getQuery()
            ->getResult();
    }
}
