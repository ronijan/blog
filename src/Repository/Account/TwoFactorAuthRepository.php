<?php

namespace App\Repository\Account;

use App\Entity\Account\TwoFactorAuth;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TwoFactorAuth>
 *
 * @method TwoFactorAuth|null find($id, $lockMode = null, $lockVersion = null)
 * @method TwoFactorAuth|null findOneBy(array $criteria, array $orderBy = null)
 * @method TwoFactorAuth[]    findAll()
 * @method TwoFactorAuth[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TwoFactorAuthRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TwoFactorAuth::class);
    }

    public function add(TwoFactorAuth $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(TwoFactorAuth $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return TwoFactorAuth[]
     */
    public function findByEmail($email): array
    {
        return $this->createQueryBuilder('t1')
            ->Join('t1.user', 't2', 't1.user_id = t2.id')
            ->where('t1.alternativeEmail = :alternativeEmail')
            ->setParameter('alternativeEmail', $email)
            ->orWhere('t2.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getResult();
    }
}