<?php

namespace App\Repository\Account;

use App\Entity\Account\TwoFactorAuthDevices;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TwoFactorAuthDevices>
 *
 * @method TwoFactorAuthDevices|null find($id, $lockMode = null, $lockVersion = null)
 * @method TwoFactorAuthDevices|null findOneBy(array $criteria, array $orderBy = null)
 * @method TwoFactorAuthDevices[]    findAll()
 * @method TwoFactorAuthDevices[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TwoFactorAuthDevicesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TwoFactorAuthDevices::class);
    }

    public function add(TwoFactorAuthDevices $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(TwoFactorAuthDevices $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
