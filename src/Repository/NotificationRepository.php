<?php

namespace App\Repository;

use App\Entity\Notification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @extends ServiceEntityRepository<Notification>
 *
 * @method Notification|null find($id, $lockMode = null, $lockVersion = null)
 * @method Notification|null findOneBy(array $criteria, array $orderBy = null)
 * @method Notification[]    findAll()
 * @method Notification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Notification::class);
    }

    public function add(Notification $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Notification $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return Notification[]
     */
    public function findAllByUserWithOffsetAndLimit(UserInterface $user, int $offset, int $limit): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.user = :userId')
            ->setParameter('userId', $user)
            ->orderBy('p.id', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Notification[]
     */
    public function findAllByUser(UserInterface $user): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.user = :user')
            ->setParameter('user', $user)
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Notification[]
     */
    public function findAllUnseenByUser(UserInterface $user): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.user = :user')
            ->setParameter('user', $user)
            ->andWhere('t1.isSeen = 0')
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Notification[]
     */
    public function deleteAllOlderThanSixMonthsByUserAndModifier(UserInterface $user, string $modifier): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.user = :user')
            ->setParameter('user', $user)
            ->andWhere('t1.updatedAt <= :modifier')
            ->setParameter('modifier', $modifier)
            ->setMaxResults(50)
            ->getQuery()
            ->getResult();
    }

    // for admin

    /**
     * @return Notification[]
     */
    public function findUnseenNotifications(): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isSeen = 0')
            ->orderBy('t1.id', 'DESC')
            ->setMaxResults(7)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Notification[]
     */
    public function findNotifications(): array
    {
        return $this->createQueryBuilder('t1')
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Notification[]
     */
    public function findNotificationsWithOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->createQueryBuilder('t1')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Notification[]
     */
    public function findAllByMessage(string $message): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.message LIKE :message')
            ->setParameter('message', $message)
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Notification[]
     */
    public function search(string $keyword): array
    {
        $text = '%' . $keyword . '%';
        $qb = $this->createQueryBuilder('t1');

        return $qb->orWhere($qb->expr()->like('t1.subject', ':subject'))
            ->setParameter('subject', $text)
            ->getQuery()
            ->getResult();
    }
}
