<?php

namespace App\Repository;

use App\Entity\FrequentlyAskedQuestions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FrequentlyAskedQuestions>
 *
 * @method FrequentlyAskedQuestions|null find($id, $lockMode = null, $lockVersion = null)
 * @method FrequentlyAskedQuestions|null findOneBy(array $criteria, array $orderBy = null)
 * @method FrequentlyAskedQuestions[]    findAll()
 * @method FrequentlyAskedQuestions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FrequentlyAskedQuestionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FrequentlyAskedQuestions::class);
    }

    public function add(FrequentlyAskedQuestions $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(FrequentlyAskedQuestions $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}