<?php

namespace App\Repository;

use App\Entity\Testimonial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Testimonial>
 *
 * @method Testimonial|null find($id, $lockMode = null, $lockVersion = null)
 * @method Testimonial|null findOneBy(array $criteria, array $orderBy = null)
 * @method Testimonial[]    findAll()
 * @method Testimonial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TestimonialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Testimonial::class);
    }

    public function createOrUpdate(Testimonial $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Testimonial $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findOneByToken(string $token): ?Testimonial
    {
        return $this->findOneBy(['token' => $token]);
    }

    /**
     * @return Testimonial[]
     */
    public function findTestimonials(): array
    {
        return $this->createQueryBuilder('t1')
            ->orderBy('t1.updatedAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Testimonial[]
     */
    public function findPublished(): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isPublished = 1')
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Testimonial[]
     */
    public function findSomeOfRecentlyPublished(int $limit): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isPublished = 1')
            ->setMaxResults($limit)
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Testimonial[]
     */
    public function findDeletedOrNotPublished(): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isPublished = 0')
            ->orWhere('t1.isDeleted = 1')
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }
}