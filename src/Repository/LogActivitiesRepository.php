<?php

namespace App\Repository;

use App\Entity\LogActivities;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @extends ServiceEntityRepository<LogActivities>
 *
 * @method LogActivities|null find($id, $lockMode = null, $lockVersion = null)
 * @method LogActivities|null findOneBy(array $criteria, array $orderBy = null)
 * @method LogActivities[]    findAll()
 * @method LogActivities[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogActivitiesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LogActivities::class);
    }

    public function add(LogActivities $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(LogActivities $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return LogActivities[]
     */
    public function findAllByUserAndLimit(UserInterface $user, int $logActivitiesLimit): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.user = :user')
            ->setParameter('user', $user)
            ->setMaxResults($logActivitiesLimit)
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return LogActivities[]
     */
    public function findResetActivitiesByUserAndLimit(UserInterface $user, int $logActivitiesLimit): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.user = :user')
            ->setParameter('user', $user)
            ->setMaxResults($logActivitiesLimit)
            ->getQuery()
            ->getResult();
    }
}
