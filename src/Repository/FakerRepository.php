<?php

namespace App\Repository;

use App\Entity\Faker;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Faker>
 *
 * @method Faker|null find($id, $lockMode = null, $lockVersion = null)
 * @method Faker|null findOneBy(array $criteria, array $orderBy = null)
 * @method Faker[]    findAll()
 * @method Faker[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FakerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Faker::class);
    }

    public function save(Faker $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Faker $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
