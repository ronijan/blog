<?php

namespace App\Repository\Blog;

use App\Entity\Blog\Auther;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Auther>
 *
 * @method Auther|null find($id, $lockMode = null, $lockVersion = null)
 * @method Auther|null findOneBy(array $criteria, array $orderBy = null)
 * @method Auther[]    findAll()
 * @method Auther[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AutherRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Auther::class);
    }

    public function save(Auther $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Auther $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return Auther[]
     */
    public function findByOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.id', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Auther[]
     */
    public function findAuthers(): array
    {
        return $this->createQueryBuilder('t1')
            ->innerJoin('t1.user', 't2', 't1.id = t2.id')
            ->where('t2.isWriter = TRUE')
            ->orderBy('t2.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Auther[]
     */
    public function search(string $keyword): array
    {
        $username = '%' . $keyword . '%';
        
        $qb = $this->createQueryBuilder('t1');

        return $qb
            ->where($qb->expr()->like('t1.username', ':username'))
            ->setParameter('username', $username)
            ->getQuery()
            ->getResult();
    }
}