<?php

namespace App\Repository\Blog;

use App\Entity\Blog\Article;
use App\Entity\Blog\Category;
use App\Helper\BlogHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Article>
 *
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function save(Article $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Article $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return Article[]
     */
    public function findRelatedByCategoryAndNotId(Category $category, int $id): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.id <> :id')
            ->setParameter('id', $id)
            ->andWhere('p.category = :category')
            ->setParameter('category', $category)
            ->andWhere('p.isPublished = 1')
            ->orderBy('p.createdAt', 'DESC')
            ->setMaxResults(BlogHelper::DEFAULT_ARTICLE_RELATED_POSTS_LIMIT)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Article[]
     */
    public function findPublishedArticlesByTag(string $tag): array
    {
        $tag = '%' . $tag . '%';
        $qb = $this->createQueryBuilder('t1');

        return $qb
            ->where($qb->expr()->like('t1.tags', ':tag'))
            ->setParameter('tag', $tag)
            ->andWhere('t1.isPublished = 1')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Article[]
     */
    public function findPublishedArticlesByOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isPublished = 1')
            ->andWhere('t1.isHead = 0')
            ->andWhere('t1.isDeleted = 0')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Article[]
     */
    public function search(string $keyword): array
    {
        $title = '%' . $keyword . '%';
        $qb = $this->createQueryBuilder('t1');

        return $qb
            ->where($qb->expr()->like('t1.title', ':title'))
            ->setParameter('title', $title)
            ->andWhere('t1.isPublished = 1')
            ->getQuery()
            ->getResult();
    }
}
