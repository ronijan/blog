<?php

namespace App\Repository\Blog;

use App\Entity\Blog\ReplyComment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ReplyComment>
 *
 * @method ReplyComment|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReplyComment|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReplyComment[]    findAll()
 * @method ReplyComment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReplyCommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReplyComment::class);
    }

    public function save(ReplyComment $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ReplyComment $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
