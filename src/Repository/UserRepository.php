<?php

namespace App\Repository;

use App\Entity\User;
use App\Helper\Job;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

use function get_class;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function add(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $user->setPassword($newHashedPassword);

        $this->add($user, true);
    }

    /**
     * @return User[]
     */
    public function findActiveUsers(): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isDeleted = 0')
            ->andWhere('t1.isDisabled = 0')
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return User[]
     */
    public function findActiveUsersWithOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isDeleted = 0')
            ->andWhere('t1.isDisabled = 0')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return User[]
     */
    public function findDisabledAccounts(): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isDisabled = 1')
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return User[]
     */
    public function findDisabledAccountsWithOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isDisabled = 1')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult();
    }


    /**
     * @return User[]
     */
    public function findDeletedAccounts(): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isDeleted = 1')
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return User[]
     */
    public function findDeletedAccountsWithOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isDeleted = 1')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return User[]
     */
    public function findDeletedAccountsToBeTransformed(string $modifier): array
    {
        return $this->createQueryBuilder('t1')
        ->where('t1.isDeleted = 1')
        ->andWhere('t1.updatedAt <= :modifier')
        ->setParameter('modifier', $modifier)
        ->setMaxResults(Job::DELETED_ACCOUNT_TRANSFORM_MAX_RESULTS)
        ->getQuery()
        ->getResult()
        ;
    }

    /**
     * @return User[]
     */
    public function search(string $keyword): array
    {
        $text = '%' . $keyword . '%';
        $qb = $this->createQueryBuilder('t1');
        $qb
            ->where($qb->expr()->like('t1.email', ':email'))
            ->setParameter('email', $text);

        return $qb->orWhere($qb->expr()->like('t1.username', ':username'))
            ->setParameter('username', $text)
            ->getQuery()
            ->getResult();
    }
}
