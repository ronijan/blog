<?php

namespace App\Repository;

use App\Entity\SystemLogs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SystemLogs>
 *
 * @method SystemLogs|null find($id, $lockMode = null, $lockVersion = null)
 * @method SystemLogs|null findOneBy(array $criteria, array $orderBy = null)
 * @method SystemLogs[]    findAll()
 * @method SystemLogs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SystemLogsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SystemLogs::class);
    }

    public function add(SystemLogs $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(SystemLogs $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return SystemLogs[]
     */
    public function findAllButNotArchived(): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isArchived = 0')
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return SystemLogs[]
     */
    public function findAllButNotArchivedByOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isArchived = 0')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return SystemLogs[]
     */
    public function findAllArchived(): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isArchived = 1')
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return SystemLogs[]
     */
    public function findAllArchivedByOffsetAndLimit(int $offset, int $limit): array
    {
        return $this->createQueryBuilder('t1')
            ->where('t1.isArchived = 1')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('t1.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }
}