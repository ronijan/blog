<?php

namespace App\Exception\Account;

use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;

class ExportPersonalDataException extends CustomUserMessageAuthenticationException
{
    //
}