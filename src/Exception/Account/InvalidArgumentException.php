<?php

namespace App\Exception\Account;

use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;

class InvalidArgumentException extends CustomUserMessageAuthenticationException
{
    //
}