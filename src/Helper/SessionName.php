<?php

declare(strict_types=1);

namespace App\Helper;

final class SessionName
{
    public const REST_PASSWORD = 'resetPasswordSession';
    public const TWO_F_A = '2fa';
    public const TWO_F_A_USER_IDENTIFIER = '2faUserIdentifier';
    public const CONTACT_FORM_SUBMISSION = 'contactFormSubmit';
    public const COUNT_FORM_SUBMISSION = 'countSubmits';
}