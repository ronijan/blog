<?php

declare(strict_types=1);

namespace App\Helper;

final class DataTransformer
{
    public const FAKER_TABLE_ENTRIES_COUNT = 100;
    
    public const TRANSFORMED_USERNAME = 'Xyz_xxxx';

    public const TRANSFORMED_EMAIL_PREFIX = 'Xyz.';

    public const TRANSFORMED_AVATAR_NAME = 'fake-avatar.svg';
}