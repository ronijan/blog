<?php

declare(strict_types=1);

namespace App\Helper;

interface MailerInterface
{
    public static function catch(mixed $content): void;
}