<?php

declare(strict_types=1);

namespace App\Helper;

final class SearchableRoutes
{
    public const ADMIN_USERS = [
        'app_admin_user_index',
        'app_admin_user_disabled_accounts_index',
        'app_admin_user_deleted_accounts_index',
    ];

    public const ADMIN_CONTACTS = [
        'app_admin_contacts_index',
        'app_admin_contacts_archived_index',
        'app_admin_contacts_deleted_contacts_index',
    ];

    public const ADMIN_CATEGORIES = [
        'app_admin_blog_categories_index'
    ];

    public const ADMIN_AUTHERS = [
        'app_admin_blog_authers_index'
    ];
}