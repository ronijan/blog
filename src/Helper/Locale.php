<?php

declare(strict_types=1);

namespace App\Helper;

final class Locale
{
    public const DEFAULT = 'en';

    public const AVAILABLE = [
        'en' => 'English', 
        'de' => 'German'
        // Define new Translation here
    ];
}