<?php

declare(strict_types=1);

namespace App\Helper;

final class BlogHelper
{
    public const DEFAULT_ARTICLES_PAGINATION_LIMIT = 6;

    public const DEFAULT_RECENT_ARTICLES_LINKS_ON_SIDEBAR = 8;

    public const SHOULD_ARTICLE_HAVE_COMMENTS = false;

    public const SHOULD_ARTICLE_BE_PUBLISHED_DIRECTLY = false;

    public const ARTICLE_READ_TIME = [
        '2 Min.',
        '3 Min.',
        '4 Min.',
        '5 Min.',
        '6 Min.',
        '7 Min.',
        '8 Min.',
    ];

    public const IS_WRITER_PROFILE_PUBLIC = false;

    public const DEFAULT_ARTICLE_RELATED_POSTS_LIMIT = 4;

    public const ABOUT_AUTHER_ON_PUBLIC_PROFILE = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.';
}