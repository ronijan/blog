<?php

declare(strict_types=1);

namespace App\Helper;

use Exception;
use RuntimeException;

final class Mailer implements MailerInterface
{
    public static function catch(mixed $content): void
    {
        try {
            if (isset($_ENV['ACTIVATE_MAILER_LOG']) && $_ENV['ACTIVATE_MAILER_LOG'] === '1') {
                file_put_contents(self::getFileName(), $content);
            }
        } catch (Exception $e) {
            Logger::addLog($e->getMessage(), 'debug');
        }
    }

    private static function getFileName(): string
    {
        $path = __DIR__ . "/../../storage/mailer/";

        if (!file_exists($path) && !mkdir($path, 0777, true) && !is_dir($path)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $path));
        }

        return $path . 'mailer.html';
    }
}