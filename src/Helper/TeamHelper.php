<?php

declare(strict_types=1);

namespace App\Helper;

final class TeamHelper
{
    public const DEFAULT_AVATAR = 'avatar378x378.png';

    public const TEAMS_DIR = '/assets/img/team/';
}