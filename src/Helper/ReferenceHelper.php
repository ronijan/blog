<?php

declare(strict_types=1);

namespace App\Helper;

final class ReferenceHelper
{
    public const COUNT_REFERENCE_SHOWN_IN_START_PAGE = 6;
}