<?php

declare(strict_types=1);

namespace App\Helper;

use RuntimeException;

final class Logger implements LoggerInterface
{
    public static function addLog(string $message, string $type = 'info'): void
    {
        $path = __DIR__ . "/../../storage/logs/";

        if (!file_exists($path) && !mkdir($path, 0777, true) && !is_dir($path)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $path));
        }

        $filename = $path . '/' . strtolower($type) . '_' . date('Ym') . '.log';
        $data = '# ' . date('Y-m-d H:i:s') . "\t" . $message;

        file_put_contents($filename, $data . "\n", FILE_APPEND);
    }
}