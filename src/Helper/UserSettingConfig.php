<?php

declare(strict_types=1);

namespace App\Helper;

final class UserSettingConfig
{
    public const DEFAULT_PAGINATION_LIMIT = 8;
    public const LIMIT_TO_RESET_PAGINATION = 32;
    public const PAGINATION = [
        8,
        16, 
        24, 
        32,
        1000
    ];

    public const DEFAULT_LOGS_LIMIT = 6;
    public const SENDING_LOG_VIA_EMAIL = false;
    public const LOGS = [
        4, 
        6, 
        8,
        1000
    ];

    public const AUTO_DELETE_OLD_NOTIFICATIONS = true;
    public const INFORM_USER_NEW_NOTIFICATION_ARRIVED = false;
}