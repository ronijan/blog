<?php

declare(strict_types=1);

namespace App\Helper;

interface LoggerInterface
{
    public static function addLog(string $message, string $type = 'info'): void;
}