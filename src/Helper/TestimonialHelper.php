<?php

declare(strict_types=1);

namespace App\Helper;

final class TestimonialHelper
{
    public const TOKEN_MUST_BE_RESETED_AFTER_CUSTOMER_SUBMIT = '';

    public const DEFAULT_AVATAR = 'anomyized75x75.png';

    public const TESTIMONIAL_DIR = '/assets/img/testimonial/';

    public const COUNT_TESTIMONIALS_SHOWN_IN_SECTION = 3;
}