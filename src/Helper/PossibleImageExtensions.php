<?php

declare(strict_types=1);

namespace App\Helper;

final class PossibleImageExtensions
{
    public const AVAILABLE = [
        'png', 
        'jpg', 
        'jpeg',
    ];
}