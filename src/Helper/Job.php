<?php

declare(strict_types=1);

namespace App\Helper;

final class Job
{
    /**
     * Delete old notifications for users that less than last 6 months
     */
    public const DELETE_OLD_NOTIFICATIONS_MODIFIER = '-6 months';

    /**
     * Delete inactive subscribers from newsletter that less than last 6 months
     */
    public const DELETE_INACTIVE_SUBSCRIBER_MODIFIER = '-6 months';

    /**
     * Delete old messages from contact form that less than last 6 months
     */
    public const DELETE_OLD_CONTACT_FORM_MESSAGES_MODIFIER = '-6 months';

     /**
     * Transform old user accounts that less than last 6 months
     */
    public const DELETED_ACCOUNT_TRANSFORM_MODIFIER = '-6 months';
    public const DELETED_ACCOUNT_TRANSFORM_MAX_RESULTS = 15;

    /**
     * Transform old contacts form messages that less than last 6 months
     */
    public const CONTACT_FORM_MESSAGES_TRANSFORM_MODIFIER = '-6 months';
}