<?php

declare(strict_types=1);

namespace App\Helper;

final class FakerHelper
{
    public const USERS_LIMIT = 7;

    public const CONTACT_MESSAGES_LIMIT = 4;

    public const TESTIMONIALS_LIMIT = 3;

    public const FAQ_LIMIT = 6;

    public const TEAM_MEMBERS_LIMIT = 4;

    public const SUBSCRIPERS_LIMIT = 12;

    public const BLOG_CATEGORIES_LIMIT = 4;

    public const BLOG_TAGS_LIMIT = 4;

    private const CHANGE_ME = false;

    public const ALLOW_BLOG_CATEGORIES = self::CHANGE_ME;

    public const ALLOW_BLOG_TAGS = self::CHANGE_ME;

    public const ALLOW_BLOG_SOCIAL_SHARE_LINKS = self::CHANGE_ME;

    public const ALLOW_TEAMS_MEMBERS = self::CHANGE_ME;

    public const ALLOW_TESTIMONIALS = self::CHANGE_ME;

    public const ALLOW_FAQ = self::CHANGE_ME;

    public const ALLOW_SUBSCRIPERS = self::CHANGE_ME;

    public const ALLOW_CONTACT_FORM_MESSAGES = self::CHANGE_ME;
}