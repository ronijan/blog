# Blog System with Symfony 6

_Personal, advanced and user-friendly blog system with commenting system - the author writes an article and the admin can publish it with just one click._


### Development

* change `DATABASE_URL` in `.env`
* $ `composer install`
* $ `php bin/console make:migration` and `php bin/console doctrine:migrations:migrate` or
  run manually `migrations/migrations.sql`
* $ `php bin/console app:seeds 5`
* $ `php -S localhost:8080 -t public`
