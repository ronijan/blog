
USE blog;

--
-- See storage/sql/Seed.sql
--
-- 
USE blog;
--
-- Drop all tabales at once
-- 
SET FOREIGN_KEY_CHECKS = 0; 
SET @tables = NULL;
SELECT GROUP_CONCAT('`', table_schema, '`.`', table_name, '`') INTO @tables
  FROM information_schema.tables 
    WHERE table_schema = 'blog'; -- !!!_SEPECIFY_DB_NAME_HERE_!!!

SET @tables = CONCAT('DROP TABLE ', @tables);
PREPARE stmt FROM @tables;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET FOREIGN_KEY_CHECKS = 1;
--
--
-- or
--
--  $ php bin/console doctrine:schema:drop --force
--  $ php bin/console doctrine:migrations:migrate
--  $ php bin/console app:seeds 5
--  $ php bin/console faker:load -- 100 entries by default

