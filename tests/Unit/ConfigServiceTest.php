<?php

declare(strict_types=1);

namespace App\Tests;

use App\Service\ConfigService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ConfigServiceTest extends KernelTestCase
{
    public function testAppParametersAppNameExist(): void
    {
        $configService = $this->getConfigService();

        $appName = $configService->getParameter('app_name');

        $this->assertNotEmpty($appName);
        $this->assertIsString($appName);
    }

    public function testAppParametersAppNoReplyEmailExist(): void
    {
        $configService = $this->getConfigService();

        $noReplyEmail = $configService->getParameter('no_reply');


        $this->assertNotEmpty($noReplyEmail);
        $this->assertIsString($noReplyEmail);
    }

    public function testAppParametersAppSupportEmailExist(): void
    {
        $configService = $this->getConfigService();


        $supportEmail = $configService->getParameter('supporter_email');

        $this->assertNotEmpty($supportEmail);
        $this->assertIsString($supportEmail);
    }

    public function testAppParametersAppInfoEmailExist(): void
    {
        $configService = $this->getConfigService();

        $infoEmail = $configService->getParameter('info_email');

        $this->assertNotEmpty($infoEmail);
        $this->assertIsString($infoEmail);
    }

    public function testAppParametersPhoneNumberExist(): void
    {
        $configService = $this->getConfigService();

        $phoneNumber = $configService->getParameter('phone_number');

        $this->assertNotEmpty($phoneNumber);
        $this->assertIsString($phoneNumber);
    }

    public function testAppParametersAddressExist(): void
    {
        $configService = $this->getConfigService();

        $address = $configService->getParameter('address');

        $this->assertNotEmpty($address);
        $this->assertIsString($address);
    }

    public function testAppParametersProfileAvatarPathExist(): void
    {
        $configService = $this->getConfigService();

        $avatarDir = $configService->getParameter('avatar_dir');

        $this->assertNotEmpty($avatarDir);
        $this->assertIsString($avatarDir);
    }

    public function testAppParametersReferencImagesPathExist(): void
    {
        $configService = $this->getConfigService();
        $config = $configService->getParameter('reference_dir');

        $this->assertNotEmpty($config);
        $this->assertIsString($config);
    }

    public function testAppParametersAdminLtePathExist(): void
    {
        $configService = $this->getConfigService();

        $adminLteDri = $configService->getParameter('admin_lte_dir');

        $this->assertNotEmpty($adminLteDri);
        $this->assertIsString($adminLteDri);
    }

    public function testAppParametersAppAutherExist(): void
    {
        $configService = $this->getConfigService();

        $auther = $configService->getParameter('app_author');

        $this->assertNotEmpty($auther);
        $this->assertIsString($auther);
    }

    public function testAppParametersAppCreatedExist(): void
    {
        $configService = $this->getConfigService();

        $appCreatedAt = $configService->getParameter('app_created');

        $this->assertNotEmpty($appCreatedAt);
        $this->assertIsString($appCreatedAt);
    }

    public function testAppParametersAppVersionExist(): void
    {
        $configService = $this->getConfigService();

        $appVersion = $configService->getParameter('app_version');

        $this->assertNotEmpty($appVersion);
        $this->assertIsString($appVersion);
    }

    public function testAppParametersAppMadeByExist(): void
    {
        $configService = $this->getConfigService();

        $appVersion = $configService->getParameter('app_made_by');

        $this->assertNotEmpty($appVersion);
        $this->assertIsString($appVersion);
    }

    public function testAppParametersAppMadeByUrlExist(): void
    {
        $configService = $this->getConfigService();

        $appVersion = $configService->getParameter('app_made_by_url');

        $this->assertNotEmpty($appVersion);
        $this->assertIsString($appVersion);
    }

    public function testAppParametersAllowLoadFixturesExistAndOff(): void
    {
        $configService = $this->getConfigService();

        $allowLoadFixtures = $configService->getParameter('ALLOW_COMMAND');

        $this->assertNotEmpty($allowLoadFixtures);
        $this->assertIsString($allowLoadFixtures);
        $this->assertSame('off', $allowLoadFixtures);
    }

    public function testAppParametersAllowRenderFaqExistAndOff(): void
    {
        $configService = $this->getConfigService();

        $allowRenderFaq = $configService->getParameter('ALLOW_RENDER_FAQ');

        $this->assertNotEmpty($allowRenderFaq);
        $this->assertIsString($allowRenderFaq);
        $this->assertSame('off', $allowRenderFaq);
    }

    public function testAppParametersShowDummyDataDuringDevExistAndOff(): void
    {
        $configService = $this->getConfigService();

        $allowRenderFaq = $configService->getParameter('SHOW_DUMMY_DATA_ON_BLOG');

        $this->assertNotEmpty($allowRenderFaq);
        $this->assertIsString($allowRenderFaq);
        $this->assertSame('off', $allowRenderFaq);
    }

    public function testAppParametersAllowModifyAdminDetailsDuringDevExistsAddOff(): void
    {
        $configService = $this->getConfigService();

        $confid = $configService->getParameter('ALLOW_MODIFY_ADMIN_DETAILS');

        $this->assertNotEmpty($confid);
        $this->assertIsString($confid);
        $this->assertSame('off', $confid);
    }

    private function getConfigService(): ConfigService
    {
        self::bootKernel();

        $container = static::getContainer();

        return $container->get(ConfigService::class);
    }
}
