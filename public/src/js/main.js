$(document).ready(function () {
    // close alert
    setTimeout(function () {
        $(".alert").alert('close').fadeOut();
    }, 6000);

    // add autofocus on modal
    let modal = $('.modal');
    modal.on('shown.bs.modal', function () {
        $(this).find('[autofocus]').focus();
    });

    // pass id to modal
    $('.uId, .contactId, .nId, .newsId, .fId').on('click', function () {
        $(this).closest('tr').addClass('bg-warning');
        $('#uId, #contactId, #nId, #newsId, #fId').val($(this).attr('data-id'));
        $('#uri').val($(this).attr('data-description')); // delete user btn
    });

    // remove bg from tr table
    modal.on('hidden.bs.modal', function () {
        $('tr').removeClass('bg-warning');
    });

    $('.cancel').on('click', function () {
        $('tr').removeClass('bg-warning');
    });

    // remove modal after click
    $('.rmModal').on('click', function () {
        setTimeout(function () {
            $('.removeModalAfterClick').modal('hide');
            $('input').val('');
        }, 3000);
    });

    // data transformer checkbox
    $("#transform").on('click', function () {
        const $box = $(this);
        let aDEmail = $('#aDEmail');

        if ($box.is(":checked")) {
            aDEmail.prop('disabled', true);
            aDEmail.prop('checked', true);
        } else {
            aDEmail.prop('disabled', false);
        }
    });

    // shortcuts
    document.addEventListener('keyup', function (event) {
        event.stopPropagation();
        if (event.ctrlKey && event.key === '?') {
            window.location.href = '/'
        }
    });
});