
var toolbarOptions = [
    [{ "font": [] }, { "size": ["small", false, "large", "huge"] }],
    ["bold", "italic", "underline", "strike"],
    [{ "color": [] }, { "background": [] }],
    [{ "script": "sub" }, { "script": "super" }],
    [{ "header": 1 }, { "header": 2 }, "blockquote", "code-block"],
    [{ "list": "ordered" }, { "list": "bullet" }, { "indent": "-1" }, { "indent": "+1" }],
    [{ "direction": "rtl" }, { "align": [] }],
    ["link", "image", "video", "formula"],
    ["clean"]
];

let quill = new Quill('#editor', {
    modules: {
        syntax: true,
        toolbar: toolbarOptions,
    },
    theme: 'snow',
});

let = formId = document.getElementById('formSave');

if (formId) {
    formId.addEventListener('click', function (e) {
        let blogHtml = quill.container.firstChild.innerHTML;
        let input = document.createElement('input');
        input.setAttribute('name', 'content');
        input.setAttribute('value', blogHtml);
        input.setAttribute('type', 'hidden')
        formId.appendChild(input);
    });
}